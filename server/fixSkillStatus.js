/*
Originally SkillStatus did not have a courseId field.
This means that we cannot easily limit SkillStatus publications
to only SkillStatus documents in a given course.
We had just published ALL SkillStatus objects instead.
This lead to a huge CoursesCount
(there are a lot of SkillStatus documents,
and they exist now (Summer 2016) for two courses).
*/

Meteor.startup(function() {
  // Not needed anymore (this ran at least once on the deployed machine)
  /*
  console.log("Fixing SkillStatus documents (adding courseId fields)");
  var skills = Skills.find({});
  skills.forEach(function(skill) {
    console.log("  updating SkillStatuses for Skill "+skill._id);
    var result = SkillStatus.update(
      {skillId: skill._id, courseId: {$exists: false}},
      {$set: {
        courseId: skill.courseId
      }},
      {multi: true}
    );
    console.log("  -> "+result);
  });
  */
});
