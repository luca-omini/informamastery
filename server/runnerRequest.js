// eslint-disable-next-line import/no-unresolved
import { HTTP } from 'meteor/http';

Meteor.methods({
  HTTPRequestServer(method, url, data) {
    return new Promise((resolve, reject) => {
      try {
        HTTP.call(method, url, { data }, (err, res) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(res);
        });
      } catch (exception) {
        reject(exception);
      }
    });
  },
});
