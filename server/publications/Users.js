// Publish all user and registration info
// this is only for admins (who e.g., need to register users as instructors of new courses)
Meteor.publish("all-user-contents", function() {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "all-user-contents", arguments);
  if (isAdmin(userId)) {
    return [
      Meteor.users.find({}, {fields: {profile: true, status: true, roles: true, emails: true, createdAt: true}}),
      StudentRegistrations.find({}),
      AssistantRegistrations.find({}),
      InstructorRegistrations.find({}),
    ];
  } else {
    return [];
  }
});

// Publish all my own stuff that is independent from a specific course
Meteor.publish("my-user-contents", function() {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "my-user-contents", arguments);
  return [
    Meteor.users.find({_id: userId}, {fields: {profile: true, status: true, roles: true, emails: true}}),
    StudentRegistrations.find({studentId: userId}),
    AssistantRegistrations.find({assistantId: userId}),
    InstructorRegistrations.find({instructorId: userId}),
  ];
});
