Meteor.publish('labSubmissionFiles', function (clientUserId, courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "labSubmissionFiles", arguments);

  // This prevents a race condition on the client between Meteor.userId() and subscriptions to this publish
  // See: https://stackoverflow.com/questions/24445404/how-to-prevent-a-client-reactive-race-between-meteor-userid-and-a-subscription/24460877#24460877
  if (clientUserId === userId) {
    // Only publish files co-owned by this userId, and ignore
    // file chunks being used by Resumable.js for current uploads
    var cursor;
    if (courseId) {
      if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
        cursor = LabSubmissionFiles.find({
          'metadata._Resumable': { $exists: false },
          'metadata.courseId': courseId,
        });
        console.log("  on teaching team or admin: cursor.count()=", cursor.count());
      } else {
        cursor = LabSubmissionFiles.find({
          'metadata._Resumable': { $exists: false },
          'metadata.courseId': courseId,
          'metadata.userIds': userId,
        });
        console.log("  neither on teaching team nor admin: cursor.count()=", cursor.count());
      }
    } else {
      cursor = null;
    }
    return cursor;
  } else {
    // This is triggered when publish is rerun with a new
    // userId before client has resubscribed with that userId
    return [];
  }
});


Meteor.publish('studentPhotoFiles', function (clientUserId, courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "studentPhotoFiles", arguments);

  // This prevents a race condition on the client between Meteor.userId() and subscriptions to this publish
  // See: https://stackoverflow.com/questions/24445404/how-to-prevent-a-client-reactive-race-between-meteor-userid-and-a-subscription/24460877#24460877
  if (clientUserId === userId) {
    // Only publish files to teaching team/admin, and ignore
    // file chunks being used by Resumable.js for current uploads
    var cursor;
    if (courseId) {
      if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
        cursor = StudentPhotoFiles.find({
          'metadata._Resumable': { $exists: false },
          'metadata.courseId': courseId,
        });
        console.log("  on teaching team or admin: cursor.count()=", cursor.count());
      } else {
        cursor = [];
      }
    } else {
      cursor = [];
    }
    return cursor;
  } else {
    // This is triggered when publish is rerun with a new
    // userId before client has resubscribed with that userId
    return [];
  }
});


console.log("#### ABOUT TO PUBLISH courseFiles...");
Meteor.publish('courseFiles', function (clientUserId, courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "courseFiles", arguments);

  // This prevents a race condition on the client between Meteor.userId() and subscriptions to this publish
  // See: https://stackoverflow.com/questions/24445404/how-to-prevent-a-client-reactive-race-between-meteor-userid-and-a-subscription/24460877#24460877
  if (clientUserId === userId) {
    // Ignore file chunks being used by Resumable.js for current uploads
    var cursor;
    if (courseId) {
      cursor = CourseFiles.find({
        'metadata._Resumable': { $exists: false },
        'metadata.courseId': courseId,
      });
    } else {
      cursor = [];
    }
    return cursor;
  } else {
    // This is triggered when publish is rerun with a new
    // userId before client has resubscribed with that userId
    return [];
  }
});
