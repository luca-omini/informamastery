Meteor.publish('course-feed', function(courseId, limit) {
	var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "course-feed", arguments);
	if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
		if (limit > FeedEvents.find({courseId: courseId, active: true}).count()) {
			limit = 0;
		}
		return FeedEvents.find({courseId: courseId, active: true}, {sort: {date: -1}, limit: limit});
	}
	return null;
});

Meteor.publish('user-feed', function(courseId, feedUserId, limit) {
	var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "user-feed", arguments);
	if (isOnTeachingTeam(userId, courseId) || isAdmin(userId) || userId===feedUserId) {
		if (limit > FeedEvents.find({courseId: courseId, userId: feedUserId}).count()) {
			limit = 0;
		}
		return FeedEvents.find({courseId: courseId, userId: feedUserId}, {sort: {date: -1}, limit: limit});
	}
	return null;
});
