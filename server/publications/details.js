Meteor.publish("details-for-section", function(sectionId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "details-for-section", arguments);
  var section = Sections.findOne({_id: sectionId});
  var courseId = section.courseId;
  if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    return SectionStatus.find({courseId: courseId, sectionId: sectionId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        recallText: 1,
        modificationDate: 1,
        startedReading: 1,
        startedReadingDate: 1,
        finishedReading: 1,
        finishedReadingDate: 1,
      }
    });
  } else if (isRegisteredAsAnything(userId, courseId)) {
    return SectionStatus.find({courseId: courseId, sectionId: sectionId, userId: userId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        recallText: 1,
        modificationDate: 1,
        startedReading: 1,
        startedReadingDate: 1,
        finishedReading: 1,
        finishedReadingDate: 1,
      }
    });
  } else {
    return [];
  }
});

Meteor.publish("details-for-chapter", function(chapterId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "details-for-chapter", arguments);
  var chapter = Chapters.findOne({_id: chapterId});
  var courseId = chapter.courseId;
  if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    return SectionStatus.find({courseId: courseId, chapterId: chapterId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        modificationDate: 1,
        // we don't provide recallText, because that's big and not needed here
      }
    });
  } else if (isRegisteredAsAnything(userId, courseId)) {
    return SectionStatus.find({courseId: courseId, chapterId: chapterId, userId: userId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        modificationDate: 1,
        // we don't provide recallText, because that's big and not needed here
      }
    });
  } else {
    return [];
  }
});

Meteor.publish("details-for-book", function(bookId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "details-for-book", arguments);
  var book = Books.findOne({_id: bookId});
  var courseId = book.courseId;
  if (isRegisteredAsAnything(userId, courseId)) {
    return SectionStatus.find({courseId: courseId, bookId: bookId, userId: userId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        // we don't provide recallText, because that's big and not needed here
      }
    });
  } else {
    return [];
  }
});

Meteor.publish("details-for-user-recalls", function(ofUserId, courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "details-for-user-recalls", arguments);
  if (userId===ofUserId || isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    return SectionStatus.find({courseId: courseId, userId: ofUserId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        recallText: 1,
        modificationDate: 1,
      }
    });
  } else {
    return [];
  }
});

Meteor.publish("details-for-topic", function(topicId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "details-for-topic", arguments);
  var topic = Topics.findOne({_id: topicId});
  var courseId = topic.courseId;
  // Find list of section ids used in study tasks in this course
  // so we can publish the SectionStatus of their sections.
  // Similarly, find list of stydyTask ids
  // so we can publish their StudyTaskStatus.
  var studyTasks = StudyTasks.find({topicId: topic._id});
  var sectionIds = [];
  var studyTaskIds = [];
  studyTasks.forEach(function(studyTask) {
    //console.log(studyTask);
    if (studyTask.kind=="Textbook") {
      if (studyTask.detail && studyTask.detail.sectionIds) {
        //console.log("^^^ Textbook studyTask, with detail, with sectionIds ", studyTask.detail.sectionIds);
        sectionIds = sectionIds.concat(studyTask.detail.sectionIds);
      }
    } else {
      //console.log("^^^ Non-Textbook studyTask");
      studyTaskIds.push(studyTask._id);
    }
  });
  console.log("sectionIds: ", sectionIds);
  console.log("studyTaskIds: ", studyTaskIds);
  if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    return [
      SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          bookId: 1,
          chapterId: 1,
          sectionId: 1,
          modificationDate: 1,
          recallText: 1,
        }
      }),
      StudyTaskStatus.find({courseId: courseId, studyTaskId: {$in: studyTaskIds}, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else if (isRegisteredAsAnything(userId, courseId)) {
    return [
      SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          bookId: 1,
          chapterId: 1,
          sectionId: 1,
          modificationDate: 1,
          recallText: 1,
        }
      }),
      StudyTaskStatus.find({courseId: courseId, studyTaskId: {$in: studyTaskIds}, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else {
    return [];
  }
});


Meteor.publish("details-for-study-task-list", function(courseId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "details-for-study-task-list", arguments);
  var course = Courses.findOne({_id: courseId});
  // Find list of section ids used in study tasks in this course
  // so we can publish the SectionStatus of their sections.
  // Similarly, find list of stydyTask ids
  // so we can publish their StudyTaskStatus.
  var studyTasks = StudyTasks.find({courseId: course._id});
  var sectionIds = [];
  var studyTaskIds = [];
  studyTasks.forEach(function(studyTask) {
    //console.log(studyTask);
    if (studyTask.kind=="Textbook") {
      if (studyTask.detail && studyTask.detail.sectionIds) {
        //console.log("^^^ Textbook studyTask, with detail, with sectionIds ", studyTask.detail.sectionIds);
        sectionIds = sectionIds.concat(studyTask.detail.sectionIds);
      }
    } else {
      //console.log("^^^ Non-Textbook studyTask");
      studyTaskIds.push(studyTask._id);
    }
  });
  console.log("sectionIds: ", sectionIds);
  console.log("studyTaskIds: ", studyTaskIds);
  if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    return [
      SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          bookId: 1,
          chapterId: 1,
          sectionId: 1,
          modificationDate: 1,
          recallText: 1,
        }
      }),
      StudyTaskStatus.find({courseId: courseId, studyTaskId: {$in: studyTaskIds}, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else if (isRegisteredAsAnything(userId, courseId)) {
    return [
      SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          bookId: 1,
          chapterId: 1,
          sectionId: 1,
          modificationDate: 1,
          recallText: 1,
        }
      }),
      StudyTaskStatus.find({courseId: courseId, studyTaskId: {$in: studyTaskIds}, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else {
    return [];
  }
});

Meteor.publish("details-for-user-study-task-list", function(courseId, userId) {
  var now = new Date();
  var thisUserId = this.userId;
  logPublication(now, userId, "details-for-user-study-task-list", arguments);
  var course = Courses.findOne({_id: courseId});
  // Find list of section ids used in study tasks in this course
  // so we can publish the SectionStatus of their sections.
  // Similarly, find list of studyTask ids
  // so we can publish their StudyTaskStatus.
  var studyTasks = StudyTasks.find({courseId: course._id});
  var sectionIds = [];
  var studyTaskIds = [];
  studyTasks.forEach(function(studyTask) {
    //console.log(studyTask);
    if (studyTask.kind=="Textbook") {
      if (studyTask.detail && studyTask.detail.sectionIds) {
        //console.log("^^^ Textbook studyTask, with detail, with sectionIds ", studyTask.detail.sectionIds);
        sectionIds = sectionIds.concat(studyTask.detail.sectionIds);
      }
    } else {
      //console.log("^^^ Non-Textbook studyTask");
      studyTaskIds.push(studyTask._id);
    }
  });
  console.log("sectionIds: ", sectionIds);
  console.log("studyTaskIds: ", studyTaskIds);
  if (isOnTeachingTeam(thisUserId, courseId) || isAdmin(thisUserId) || thisUserId==userId) {
    return [
      SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          bookId: 1,
          chapterId: 1,
          sectionId: 1,
          modificationDate: 1,
          recallText: 1,
        }
      }),
      StudyTaskStatus.find({courseId: courseId, studyTaskId: {$in: studyTaskIds}, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else {
    return [];
  }
});
