Meteor.publish("study-task-status", function(studyTaskId) {
  var now = new Date();
  var userId = this.userId;
  logPublication(now, userId, "study-task-status", arguments);
  console.log("for userId "+userId);
  var studyTask = StudyTasks.findOne({_id: studyTaskId});
  var courseId = studyTask.courseId;

  // Find list of section ids used in study tasks in this course
  // so we can publish the SectionStatus of their sections.
  var sectionIds = [];
  if (studyTask.kind=="Textbook" && studyTask.detail && studyTask.detail.sectionIds) {
    console.log("^^^ Textbook studyTask, with detail, with sectionIds ", studyTask.detail.sectionIds);
    sectionIds = studyTask.detail.sectionIds;
  }
  console.log("sectionIds: ", sectionIds);

  if (isOnTeachingTeam(userId, courseId) || isAdmin(userId)) {
    console.log("teaching team member");
    return [
      SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          bookId: 1,
          chapterId: 1,
          sectionId: 1,
          modificationDate: 1,
          recallText: 1,
        }
      }),
      StudyTaskStatus.find({courseId: courseId, studyTaskId: studyTaskId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else if (isRegisteredAsAnything(userId, courseId)) {
    console.log("normal user");
    var sectionStatusCursor = SectionStatus.find({courseId: courseId, sectionId: {$in: sectionIds}, userId: userId, deleted: {$exists: false}}, {
      fields: {
        userId: 1,
        courseId: 1,
        bookId: 1,
        chapterId: 1,
        sectionId: 1,
        modificationDate: 1,
        recallText: 1,
      }
    });
    console.log("sectionStatusCursor.count()="+sectionStatusCursor.count());
    return [
      sectionStatusCursor,
      StudyTaskStatus.find({courseId: courseId, studyTaskId: studyTaskId, userId: userId, deleted: {$exists: false}}, {
        fields: {
          userId: 1,
          courseId: 1,
          themeId: 1,
          topicId: 1,
          studyTaskId: 1,
          recallText: 1,
          modificationDate: 1,
          started: 1,
          startedDate: 1,
          finished: 1,
          finishedDate: 1,
        }
      }),
    ];
  } else {
    return [];
  }
});
