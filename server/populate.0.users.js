Meteor.startup(function () {
	console.log("POPULATING DATABASE: populate.0.users.js");

	// Should we do that here, or should/can we do it in lib/at_config.js? Do these two settings interfere?
	//Accounts.config({restrictCreationByEmailDomain: "usi.ch"});

	// TODO: comment this out at some point!
	// always start from scratch
	//console.log("Deleting users!");
	//Meteor.users.remove({});

	if (Meteor.users.find().count()===0) {

		// Create the root user
		// Accounts.createUser is part of the accounts-password package.
		var adminId = Accounts.createUser({
			username: "hauswirm",
			email: "matthias.hauswirth@usi.ch", //IMPORTANT: must be all lower-case (otherwise account won't be found!)
			password: "password",
			profile: {
				name: "Matthias Hauswirth",
			}
		});
		console.log("Created administrator user: "+adminId);
		console.log(JSON.stringify(Meteor.users.findOne({_id: adminId})));
		
		// Note: email is not verified (which may prevent this user from logging in)
		
		// Should we forcefully verify it (MongoDB query)?
		// e.g., manually, in the mongod console --- db.users.update({"_id": "nGPiQFpucp6MEqkLm"}, {$set: {"emails.0.verified": true}})
		Meteor.users.update({"_id": adminId}, {$set: {"emails.0.verified": true}});

		// Or should we call Accounts.sendVerificationEmail(adminId)?
		//Accounts.sendVerificationEmail(adminId);
		// this indeed sends the mail, and the verification page works and directly logs us in,
		// but normal login via the login form is still not possible... (why???)


		// We use the alanning:roles package.
		// Documentation: http://alanning.github.io/meteor-roles/classes/Roles.html
		// Do we also need to add roles to Meteor.roles (or do we do all of that lazily, via Roles.addUsersToRoles())?

		// We use four kinds of roles:
		// * admin - administrator who can modify stuff (e.g., create a course)
		// * instructor - an instructor (of a given course, the "group" is the courseId) who can edit the course
		// * assistant - a teaching assistant (of a given course, the "group" is the courseId) who can do mastery checks and see students' progress
		// * student - a student (of a given course, the "group" is the courseId) who can see their own progress
		// Note that a given user may play different roles in different courses ("groups").
		Roles.addUsersToRoles(adminId, ['admin', 'instructor', 'assistant', 'student'], Roles.GLOBAL_GROUP);
		console.log("Added roles to administrator user: "+adminId);
		console.log(JSON.stringify(Meteor.users.findOne({_id: adminId})));
	}
});
