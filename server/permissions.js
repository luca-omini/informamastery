LabSubmissionFiles.allow({
  // The creator of a file owns it. UserId may be null.
  insert: function (userId, file) {
    console.log("LabSubmissionFiles.allow.insert("+userId+",file) where file=", file);
    // Assign the proper owner when a file is created
    file.metadata = file.metadata || {};
    file.metadata.ownerId = userId;
    // Only allow inserts if lab is open
    var lab = Labs.findOne({_id: file.metadata.labId});
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId) ||
      (_.contains(file.metadata.userIds, userId) && lab.status=="open");
    console.log("-> "+permitted);
    return permitted;
  },
  // Only teaching team members, admins, or co-owners can remove a file
  remove: function (userId, file) {
    console.log("LabSubmissionFiles.allow.remove("+userId+",file) where file=", file);
    var lab = Labs.findOne({_id: file.metadata.labId});
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId) ||
      (_.contains(file.metadata.userIds, userId) && lab.status=="open");
    console.log("-> "+permitted);
    return permitted;
  },
  // Only teaching team members, admins, or co-owners can retrieve a file via HTTP GET
  read: function (userId, file) {
    console.log("LabSubmissionFiles.allow.read("+userId+",file) where file=", file);
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId) ||
      _.contains(file.metadata.userIds, userId);
    console.log("-> "+permitted);
    return permitted;
  },
  // This rule secures the HTTP REST interfaces' PUT/POST
  // Necessary to support Resumable.js
  write: function (userId, file, fields) {
    console.log("LabSubmissionFiles.allow.write("+userId+",file) where file=", file);
    // Only owners can upload file data
    var permitted = (userId === file.metadata.ownerId);
    console.log("-> "+permitted);
    return permitted;
  }
});


StudentPhotoFiles.allow({
  // The creator of a file owns it. UserId may be null.
  insert: function (userId, file) {
    console.log("StudentPhotoFiles.allow.insert("+userId+",file) where file=", file);
    // Assign the proper owner when a file is created
    file.metadata = file.metadata || {};
    file.metadata.ownerId = userId;
    // Only allow inserts if owner is on teaching team
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId);
    console.log("-> "+permitted);
    return permitted;
  },
  // Only teaching team members and admins can remove a file
  remove: function (userId, file) {
    console.log("StudentPhotoFiles.allow.remove("+userId+",file) where file=", file);
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId);
    console.log("-> "+permitted);
    return permitted;
  },
  // Only teaching team members and admins can retrieve a file via HTTP GET
  read: function (userId, file) {
    console.log("StudentPhotoFiles.allow.read("+userId+",file) where file=", file);
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId);
    console.log("-> "+permitted);
    return permitted;
  },
  // This rule secures the HTTP REST interfaces' PUT/POST
  // Necessary to support Resumable.js
  write: function (userId, file, fields) {
    console.log("StudentPhotoFiles.allow.write("+userId+",file) where file=", file);
    // Only owners can upload file data
    var permitted = (userId === file.metadata.ownerId);
    console.log("-> "+permitted);
    return permitted;
  }
});


CourseFiles.allow({
  // The creator of a file owns it. UserId may be null.
  insert: function (userId, file) {
    console.log("CourseFiles.allow.insert("+userId+",file) where file=", file);
    // Assign the proper owner when a file is created
    file.metadata = file.metadata || {};
    file.metadata.ownerId = userId;
    // Only allow inserts if owner is on teaching team
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId);
    console.log("-> "+permitted);
    return permitted;
  },
  // Only teaching team members and admins can remove a file
  remove: function (userId, file) {
    console.log("CourseFiles.allow.remove("+userId+",file) where file=", file);
    var permitted =
      isOnTeachingTeam(userId, file.metadata.courseId) ||
      isAdmin(userId);
    console.log("-> "+permitted);
    return permitted;
  },
  // Everybody, even if not logged in, can retrieve a file via HTTP GET
  read: function (userId, file) {
    console.log("CourseFiles.allow.read("+userId+",file) where file=", file);
    var permitted = true;
    console.log("-> "+permitted);
    return permitted;
  },
  // This rule secures the HTTP REST interfaces' PUT/POST
  // Necessary to support Resumable.js
  write: function (userId, file, fields) {
    console.log("CourseFiles.allow.write("+userId+",file) where file=", file);
    // Only owners can upload file data
    var permitted = (userId === file.metadata.ownerId);
    console.log("-> "+permitted);
    return permitted;
  }
});
