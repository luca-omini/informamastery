// facts configuration
Meteor.startup(function() {
  console.log("Configuring meteor:facts");
  Facts.setUserIdFilter(function(userId) {
    return isAdmin(userId);
  });
});
