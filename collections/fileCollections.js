// This FileCollection holds all submission files uploaded by students for labs.
LabSubmissionFiles = new FileCollection('labSubmissionFiles', {
  resumable: true,    // Enable built-in resumable.js chunked upload support
  // explicitly define the index name (chose a short name to avoid trouble):
  // see: https://github.com/vsivsi/meteor-file-collection/issues/55
  resumableIndexName: 'labSubmissionFilesResumableIndex',
  http: [             // Define HTTP route
    {
      method: 'get',  // Enable a GET endpoint
      path: '/md5/:md5',  // this will be at route "/gridfs/labSubmissionFiles/md5/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to Files
      }
    }
  ]
});


// This FileCollection holds all portrait photos a teacher uploads and assigns to students.
StudentPhotoFiles = new FileCollection('studentPhotoFiles', {
  resumable: true,    // Enable built-in resumable.js chunked upload support
  // explicitly define the index name (chose a short name to avoid trouble):
  // see: https://github.com/vsivsi/meteor-file-collection/issues/55
  resumableIndexName: 'studentPhotoFilesResumableIndex',
  http: [             // Define HTTP route
    {
      method: 'get',  // Enable a GET endpoint
      path: '/md5/:md5',  // this will be at route "/gridfs/studentPhotoFiles/md5/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to Files
      }
    }
  ]
});


// This FileCollection holds all files the teaching team uploads for a course.
CourseFiles = new FileCollection('courseFiles', {
  resumable: true,    // Enable built-in resumable.js chunked upload support
  // explicitly define the index name (chose a short name to avoid trouble):
  // see: https://github.com/vsivsi/meteor-file-collection/issues/55
  resumableIndexName: 'courseFilesResumableIndex',
  http: [             // Define HTTP route
    {
      method: 'get',  // Enable a GET endpoint
      path: '/md5/:md5',  // this will be at route "/gridfs/courseFiles/md5/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to Files
      }
    }
  ]
});
