Meteor.methods({
	startPomodoro: function(goal, courseId, topicId, labId) {
		console.log("Meteor.methods.startPomodoro("+goal+", "+courseId+", "+topicId+", "+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, course._id) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to start a pomodoro.");
		}
		var now = new Date();
		var userId = this.userId;
		Meteor.users.update({_id: userId}, {
			$set: {
				"status.pomodoroOngoing": true,
				"status.pomodoroStartTime": now,
			},
		});
		var pomodoroId = Pomodoros.insert({
			userId: userId,
			goal: goal,
			courseId: courseId,
			topicId: topicId,
			labId: labId,
			startTime: now,
			ongoing: true,
		});
		FeedEvents.insert({userId: this.userId, courseId: courseId, date: now, active: true, event: "IStartedAPomodoro", pomodoroId: pomodoroId});
		return pomodoroId;
	},
	abortPomodoro: function(pomodoroId, reason) {
		console.log("Meteor.methods.abortPomodoro("+pomodoroId+", "+reason+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var pomodoro = Pomodoros.findOne({_id: pomodoroId});
		if (!pomodoro) {
			throw new Meteor.Error("pomodoro-does-not-exist", "The specified pomodoro does not exist.");
		}
		if (this.userId!==pomodoro.userId) {
			throw new Meteor.Error("no-permission", "You don't have permissions to abort this pomodoro.");
		}
		var now = new Date();
		var userId = this.userId;
		Meteor.users.update({_id: userId}, {
			$set: {
				"status.pomodoroOngoing": false,
				"status.pomodoroStartTime": undefined,
			},
		});
		FeedEvents.insert({userId: this.userId, courseId: pomodoro.courseId, date: now, active: true, event: "IAbortedAPomodoro", pomodoroId: pomodoroId});
		return Pomodoros.update({_id: pomodoroId}, {
			$set: {
				ongoing: false,
				aborted: true,
				reason: reason,
				finishTime: now,
			}
		});
	},
	completePomodoro: function(pomodoroId, goalAccomplished, comment) {
		console.log("Meteor.methods.completePomodoro("+pomodoroId+", "+goalAccomplished+", "+comment+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var pomodoro = Pomodoros.findOne({_id: pomodoroId});
		if (!pomodoro) {
			throw new Meteor.Error("pomodoro-does-not-exist", "The specified pomodoro does not exist.");
		}
		if (this.userId!==pomodoro.userId) {
			throw new Meteor.Error("no-permission", "You don't have permissions to complete this pomodoro.");
		}
		var now = new Date();
		var userId = this.userId;
		Meteor.users.update({_id: userId}, {
			$set: {
				"status.pomodoroOngoing": false,
				"status.pomodoroStartTime": undefined,
			},
		});
		FeedEvents.insert({userId: this.userId, courseId: pomodoro.courseId, date: now, active: true, event: "ICompletedAPomodoro", pomodoroId: pomodoroId});
		return Pomodoros.update({_id: pomodoroId}, {
			$set: {
				ongoing: false,
				completed: true,
				goalAccomplished: goalAccomplished,
				comment: comment,
				finishTime: now,
			}
		});
	},
});
