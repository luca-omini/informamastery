_computeAverageQualityRating = function(practiceProblemId) {
	console.log("_computeAverageQualityRating("+practiceProblemId+")");
	// easy, medium, hard
	var histogram = [0, 0, 0];
	var count = 0;
	var sum = 0;
	PracticeProblemSolutions.find({practiceProblemId: practiceProblemId}).forEach(function(practiceProblemSolution) {
		if (practiceProblemSolution.quality) {
			console.log("practiceProblemSolution.quality="+practiceProblemSolution.quality);
			histogram[practiceProblemSolution.quality]++;
			count++;
			sum+=Math.floor(practiceProblemSolution.quality);
		}
	});
	console.log("sum="+sum);
	console.log("count="+count);
	if (count>0) {
		var average = sum/count;
		return average;
	} else {
		return -1;
	}
};

_computeAverageQualityRatingAcrossProblems = function(practiceProblemIds) {
	console.log("_computeAverageQualityRatingAcrossProblems("+practiceProblemIds+")");
	// easy, medium, hard
	var histogram = [0, 0, 0];
	var count = 0;
	var sum = 0;
	PracticeProblemSolutions.find({practiceProblemId: {$in: practiceProblemIds}}).forEach(function(practiceProblemSolution) {
		if (practiceProblemSolution.quality) {
			console.log("practiceProblemSolution.quality="+practiceProblemSolution.quality);
			histogram[practiceProblemSolution.quality]++;
			count++;
			sum+=Math.floor(practiceProblemSolution.quality);
		}
	});
	console.log("sum="+sum);
	console.log("count="+count);
	if (count>0) {
		var average = sum/count;
		return average;
	} else {
		return -1;
	}
};

_convertQualityNumberToText = function(quality) {
	if (quality>=0) {
		var roundedQuality = Math.round(quality);
		var labels = ["very poor", "poor", "fair", "good", "very good", "excellent"];
		if (roundedQuality>=0 && roundedQuality<=5) {
			return labels[roundedQuality];
		} else {
			// should not happen
			return roundedQuality;
		}
	}
	return "";
};
