export const parseCodeResult = (hasOutput, stderr) => {
  let annotations = [];
  if (hasOutput && stderr !== '') {
    const rows = stderr.match(/:\d+/g).map((el) => el.replace(':', '')).map(Number);
    const messages = stderr.match(/error:[\s\S]*?(?=\n=|\^)/g);
    annotations = rows.map((el, index) => ({ row: el - 1, text: messages[index] += '^\n', type: 'error' }));
  }
  return annotations;
};

export const parseTestFailure = (hasOutput, testFailures) => {
  let annotations = [];
  if (hasOutput && testFailures !== []) {
    annotations = testFailures.map((failure) => {
      const testMessage = `Method ${failure.method} in class ${failure.class} has failed, line ${failure.line}`;

      return {
        row: failure.line - 1,
        text: testMessage,
        type: 'warning',
      };
    });
  }
  return annotations;
};

export const parseFQCN = (fileText) => {
  let fqcn = '';
  let javaPackage = fileText.match(/package.*/g);
  let javaClass = fileText.match(/class.* {/g);
  if (javaPackage && javaClass) {
    javaPackage = javaPackage[0].split(' ');
    javaClass = javaClass[0].split(' ');
    fqcn = `${javaPackage[1].replace(';', '')}.${javaClass[1].replace(' ', '')}`;
  } else if (!javaPackage && javaClass) {
    javaClass = javaClass[0].split(' ');
    fqcn = javaClass[1].trim();
  }
  return fqcn;
};
