// JavaScript functions related to mastery checks, usable on client AND server

createStudyTaskStatusSnapshots = function(userIds, topicId) {
  console.log("createStudyTaskStatusSnapshots("+userIds+", "+topicId+")");
  var snapshot = {};
  _.each(userIds, function(userId) {
    snapshot[userIds] = {};
    console.log(" userId: "+userId);
    StudyTasks.find({topicId: topicId}).forEach(function(studyTask) {
      snapshot[userIds][studyTask._id] = {};
      console.log("  studyTaskId: "+studyTask._id);
      if (studyTask.kind==="Textbook") {
        console.log("   Textbook");
        var sectionStatusInfos = Sections.find({sectionId: studyTask.detail.sectionIds, removed: {$ne: true}}).map(function(section) {
          console.log("    sectionId: "+section._id);
          var sectionStatus = SectionStatus.findOne({userId: userId, sectionId: section._id});
          console.log("    sectionStatus: "+sectionStatus);
          if (!sectionStatus) {
            sectionStatus = {};
          }
          var info = {};
          if (sectionStatus.recallText) {
      			info.stage = "review";
      		} else if (sectionStatus.finishedReading) {
      			info.stage = "recall";
      		} else if (sectionStatus.startedReading) {
      			info.stage = "read";
      		} else {
      			info.stage = "prepare";
      		}
          info.recallText = sectionStatus.recallText;
          console.log("    info: "+info);
          return info;
        });
        var minStage = _.reduce(_.map(sectionStatusInfos, function(i) {return i.stage;}), function(memo, s) {
          if (s==="prepare" || memo==="prepare") {
            return "prepare";
          } else if (s==="read" || memo==="read") {
            return "read";
          } else if (s==="recall" || memo==="recall") {
            return "recall";
          } else {
            return "review";
          }
        }, "review");
        var maxStage = _.reduce(_.map(sectionStatusInfos, function(i) {return i.stage;}), function(memo, s) {
          if (s==="review" || memo==="review") {
            return "review";
          } else if (s==="recall" || memo==="recall") {
            return "recall";
          } else if (s==="read" || memo==="read") {
            return "read";
          } else {
            return "prepare";
          }
        }, "prepare");
        var stage;
        if (maxStage==="prepare") {
          stage = "prepare";
        } else if (minStage==="review") {
          stage = "review";
        } else {
          stage = minStage;
        }
        var recallTexts = _.reduce(_.map(sectionStatusInfos, function(i) {return i.recallText;}), function(memo, rt) {
          return memo+" "+rt;
        }, "");
        snapshot[userIds][studyTask._id].stage = stage;
        snapshot[userIds][studyTask._id].recallText = recallTexts;
      } else if (studyTask.kind==="ClassSession") {
        console.log("   ClassSession");
        // TODO: allow entry of recallTexts for class sessions
        snapshot[userIds][studyTask._id].stage = undefined;
        snapshot[userIds][studyTask._id].recallText = undefined;
      } else {
        console.log("   "+studyTask.kind);
        var status = StudyTaskStatus.findOne({userId: userId, studyTaskId: studyTask._id});
        if (!status) {
          status = {};
        }
        var stage = "";
        if (status.recallText) {
          stage="review";
        } else if (status.finished) {
          stage="recall";
        } else if (status.started) {
          stage="study";
        } else {
          stage="prepare";
        }
        snapshot[userIds][studyTask._id].stage = stage;
        snapshot[userIds][studyTask._id].recallText = status.recallText;
      }
    });
  });
  console.log("snapshot: ", snapshot);
  return snapshot;
};

createSkillStatusSnapshots = function(userIds, topicId) {
  //TODO
  return {};
};

createPracticeProblemStatusSnapshots = function(userIds, topicId) {
  //TODO
  return {};
};
