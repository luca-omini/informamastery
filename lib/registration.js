isOnTeachingTeam = function(userId, courseId) {
	//console.log("isOnTeachingTeam("+userId+", "+courseId+")");
	return isInstructor(userId, courseId) || isAssistant(userId, courseId);
}

isInstructor = function(userId, courseId) {
	//console.log("isInstructor("+userId+", "+courseId+")");
	return InstructorRegistrations.find({instructorId: userId, courseId: courseId}).count()>0;
}

isAssistant = function(userId, courseId) {
	//console.log("isAssistant("+userId+", "+courseId+")");
	return AssistantRegistrations.find({assistantId: userId, courseId: courseId}).count()>0;
}

isStudent = function(userId, courseId) {
	//console.log("isStudent("+userId+", "+courseId+")");
	return StudentRegistrations.find({studentId: userId, courseId: courseId}).count()>0;
}

isApprovedStudent = function(userId, courseId) {
	//console.log("isApprovedStudent("+userId+", "+courseId+")");
	return StudentRegistrations.find({studentId: userId, courseId: courseId, approved: true}).count()>0;
}

isUserApprovedNotWithdrawnStudent = function(userId, courseId) {
  console.log("isUserApprovedNotWithdrawnStudent("+userId+", "+courseId+")");
  console.log("StudentRegistrations: ", StudentRegistrations.find({studentId: userId, courseId: courseId}).fetch());
  return !!StudentRegistrations.findOne({studentId: userId, courseId: courseId, approved: true, widthdrawn: {$ne: true}});
}

isRegisteredAsAnything = function(userId, courseId) {
	//console.log("isRegisteredAsAnything("+userId+", "+courseId+")");
    return (!!StudentRegistrations.findOne({studentId: userId, courseId: courseId}))
      || (!!AssistantRegistrations.findOne({assistantId: userId, courseId: courseId}))
      || (!!InstructorRegistrations.findOne({instructorId: userId, courseId: courseId}));
}

isAdmin = function(userId) {
	//console.log("isAdmin("+userId+")");
	return Roles.userIsInRole(userId, 'admin', Roles.GLOBAL_GROUP);
}


Meteor.methods({
	updateProfile: function(firstName, lastName, skypeName, faceTimeId, gmailAddress, biography, howDoYouKnowYouAreLearning) {
		console.log("Meteor.methods.updateProfile("+firstName+", "+lastName+", "+skypeName+", "+faceTimeId+", "+gmailAddress+", "+biography+", "+howDoYouKnowYouAreLearning+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update your profile.");
		}
		return Meteor.users.update({_id: this.userId}, { $set: {
			"profile.firstName": firstName,
			"profile.lastName": lastName,
			"profile.biography": biography,
			"profile.howDoYouKnowYouAreLearning": howDoYouKnowYouAreLearning,
			"profile.skypeName": skypeName,
			"profile.faceTimeId": faceTimeId,
			"profile.gmailAddress": gmailAddress,
		}});
	},
	selfRegisterAsStudent: function(courseId) {
		console.log("Meteor.methods.selfRegisterAsStudent("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to register for a course.");
		}
		if (Courses.find({_id: courseId}).count()!=1) {
			throw new Meteor.Error("course-does-not-exist", "The given course does not exist.");
		}
		if (StudentRegistrations.find({studentId: this.userId, courseId: courseId}).count()>0) {
			throw new Meteor.Error("already-registered", "This user is already registered as a student for this course.");
		}
		return StudentRegistrations.insert({
			creatorId: this.userId,
			creationDate: new Date(),
			studentId: this.userId,
			courseId: courseId,
			approved: false,
			approvalDate: undefined,
			approverId: undefined,
			withdrawn: false,
			withdrawalDate: undefined,
			completed: false,
			completionDate: undefined,
			completerId: undefined,
		});
	},
	registerAndApproveUserAsStudent: function(studentId, courseId) {
		console.log("Meteor.methods.registerAndApproveUserAsStudent("+studentId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to register for a course.");
		}
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to register a user as a student.");
		}
		if (Meteor.users.find({_id: studentId}).count()!=1) {
			throw new Meteor.Error("user-does-not-exist", "The given user does not exist.");
		}
		if (Courses.find({_id: courseId}).count()!=1) {
			throw new Meteor.Error("course-does-not-exist", "The given course does not exist.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()>0) {
			throw new Meteor.Error("already-registered", "This user is already registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.insert({
			creatorId: this.userId,
			creationDate: now,
			studentId: studentId,
			courseId: courseId,
			approved: true,
			approvalDate: now,
			approverId: this.userId,
			withdrawn: false,
			withdrawalDate: undefined,
			completed: false,
			completionDate: undefined,
			completerId: undefined,
		});
	},
	approveUserAsStudent: function(studentId, courseId) {
		console.log("Meteor.methods.approveUserAsStudent("+studentId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to approve a user as a student.");
		}
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to approve a user as a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.update({studentId: studentId, courseId: courseId}, {$set: {
			approved: true,
			approvalDate: now,
			approverId: this.userId
		}});
	},
	undoApprovalOfUserAsStudent: function(studentId, courseId) {
		console.log("Meteor.methods.undoApprovalOfUserAsStudent("+studentId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to undo the approval of a user as a student.");
		}
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to undo the approval of a user as a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.update({studentId: studentId, courseId: courseId}, {$set: {
			approved: false,
			approvalUndoneDate: now
		}});
	},
	withdrawUserAsStudent: function(studentId, courseId) {
		console.log("Meteor.methods.withdrawUserAsStudent("+studentId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to withdraw a user as a student.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor of this course to withdraw a user as a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.update({studentId: studentId, courseId: courseId, withdrawn: false}, {$set: {
			withdrawn: true,
			withdrawalDate: now,
			withdrawerId: this.userId,
		}});
	},
	undoWithdrawalOfUserAsStudent: function(studentId, courseId) {
		console.log("Meteor.methods.undoWithdrawalOfUserAsStudent("+studentId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to undo the withdrawal of a user as a student.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor of this course to undo the withdrawal of a user as a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId, withdrawn: true}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.update({studentId: studentId, courseId: courseId}, {$set: {
			withdrawn: false,
			withdrawalUndoneDate: now,
		}});
	},
	withdrawUserAsAssistant: function(assistantId, courseId) {
		console.log("Meteor.methods.withdrawUserAsAssistant("+assistantId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to withdraw a user as an assistant.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor of this course to withdraw a user as an assistant.");
		}
		if (AssistantRegistrations.find({assistantId: assistantId, courseId: courseId}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user was never registered as an assistant for this course.");
		}
		var now = new Date();
		return AssistantRegistrations.update({assistantId: assistantId, courseId: courseId}, {$set: {
			withdrawn: true,
			withdrawalDate: now,
			withdrawerId: this.userId,
		}});
	},
	undoWithdrawalOfUserAsAssistant: function(assistantId, courseId) {
		console.log("Meteor.methods.undoWithdrawalOfUserAsAssistant("+assistantId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to undo the withdrawal of a user as an assistant.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor of this course to undo the withdrawal of a user as an assistant.");
		}
		if (AssistantRegistrations.find({assistantId: assistantId, courseId: courseId}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user was never registered as an assistant for this course.");
		}
		var now = new Date();
		return AssistantRegistrations.update({assistantId: assistantId, courseId: courseId}, {$set: {
			withdrawn: false,
			withdrawalUndoneDate: now,
		}});
	},
	updateGuerillamailAddress: function(studentId, courseId, guerillamailCleartextAddress, guerillamailScrambledAddress) {
		console.log("Meteor.methods.updateGuerillamailAddress("+studentId+", "+courseId+", "+guerillamailCleartextAddress+", "+guerillamailScrambledAddress+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update the Guerilla Mail address of a student.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor of this course to update the Guerilla Mail addresss of a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()==0) {
			throw new Meteor.Error("not-yet-registered", "This user is not yet registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.update({studentId: studentId, courseId: courseId}, {$set: {
			guerillamailCleartextAddress: guerillamailCleartextAddress,
			guerillamailScrambledAddress: guerillamailScrambledAddress,
		}});
	},
	registerUserAsStudent: function(studentId, courseId) {
		console.log("Meteor.methods.registerUserAsStudent("+studentId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to register a user as a student.");
		}
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to register a user as a student.");
		}
		if (StudentRegistrations.find({studentId: studentId, courseId: courseId}).count()>0) {
			throw new Meteor.Error("already-registered", "This user is already registered as a student for this course.");
		}
		var now = new Date();
		return StudentRegistrations.insert({
			creatorId: this.userId,
			creationDate: now,
			studentId: studentId,
			courseId: courseId,
			approved: true,
			approvalDate: now,
			approverId: this.userId,
			withdrawn: false,
			withdrawalDate: undefined,
			completed: false,
			completionDate: undefined,
			completerId: undefined,
		});
	},
	registerUserAsAssistant: function(assistantId, courseId) {
		console.log("Meteor.methods.registerUserAsAssistant("+assistantId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to register a user as an assistant.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an instructor of this course to register a user as an assistant.");
		}
		if (AssistantRegistrations.find({assistantId: assistantId, courseId: courseId}).count()>0) {
			throw new Meteor.Error("already-registered", "This user is already registered as an assistant for this course.");
		}
		var now = new Date();
		return AssistantRegistrations.insert({
			creatorId: this.userId,
			creationDate: now,
			assistantId: assistantId,
			courseId: courseId,
		});
	},
	registerUserAsInstructor: function(instructorId, courseId) {
		console.log("Meteor.methods.registerUserAsInstructor("+instructorId+", "+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to register a user as an instructor.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an admin or an instructor of this course to register another user as an instructor.");
		}
		if (InstructorRegistrations.find({instructorId: instructorId, courseId: courseId}).count()>0) {
			throw new Meteor.Error("already-registered", "This user is already registered as an instructor for this course.");
		}
		var now = new Date();
		return InstructorRegistrations.insert({
			creatorId: this.userId,
			creationDate: now,
			instructorId: instructorId,
			courseId: courseId,
		});
	},
	createUsersAndRegisterAndApproveAsStudents: function(courseId, students) {
		var now = new Date();
		var self = this;
		logMethodCall(now, "createUsersAndRegisterAndApproveAsStudents", arguments);
		if (!self.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		if (!isOnTeachingTeam(self.userId, courseId) && !isAdmin(self.userId)) {
			throw new Meteor.Error("not-instructor", "You must be an admin or an instructor of this course to register another user as an instructor.");
		}
		_.each(students, function(student, index, list) {
			console.log("student:", student);
			var user = Meteor.users.findOne({"emails.address": student.email});
			var registration;
			if (user) {
				registration = StudentRegistrations.findOne({courseId: courseId, studentId: user._id});
			}
			console.log("user:", user);
			console.log("registration:", registration);
			var userId;
			if (!user) {
				// create user
				userId = Accounts.createUser({
					email: student.email,
					profile: {
						firstName: student.first,
						lastName: student.last,
					},
				});
				console.log("created user:", Meteor.users.findOne({_id: userId}));
			} else {
				userId = user._id;
			}
			if (!registration) {
				// register and approve user
				registration = StudentRegistrations.insert({
					creatorId: self.userId,
					creationDate: now,
					studentId: userId,
					courseId: courseId,
					approved: true,
					approvalDate: now,
					approverId: self.userId,
					withdrawn: false,
					withdrawalDate: undefined,
					completed: false,
					completionDate: undefined,
					completerId: undefined,
				});
				console.log("registered and approved as student:", registration);
			}
		});
	},
	impersonate: function(userId) {
		check(userId, String);

		if (!Meteor.users.findOne(userId)) {
			throw new Meteor.Error(404, 'User not found');
		}
		if (!isAdmin(this.userId)) {
			throw new Meteor.Error(403, 'Permission denied');
		}
		this.setUserId(userId);
	},
	studentPhotoFileUploadFinished: function(studentPhotoFileId) {
		var now = new Date();
		logMethodCall(now, "studentPhotoFileUploadFinished", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var file = StudentPhotoFiles.findOne({_id: new Mongo.ObjectID(studentPhotoFileId)});
		if (!file) {
			throw new Meteor.Error("file-does-not-exist", "The specified file does not exist.");
		}
		console.log("StudentPhotoFile:", file);
		var courseId = file.metadata.courseId;
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit. How could you actually upload this?");
		}
		var studentRegistrationId = file.metadata.studentRegistrationId;
		var studentRegistration = StudentRegistrations.findOne({_id: studentRegistrationId});
		if (!studentRegistration) {
			throw new Meteor.Error("no-student-registration", "There is no student registration for this photo file");
		}
		var userId = file.metadata.userId;
		var user = Meteor.users.findOne({_id: userId});
		if (!user) {
			throw new Meteor.Error("no-user", "There is no user for this photo file");
		}
		// TODO: Shouldn't we delete the uploaded file if any exception is thrown above??
		console.log("Meteor.methods.studentPhotoFileUploadFinished: studentRegistrationId:", studentRegistration._id, "userId:", user._id, "ownerId:", this.userId, "date:", now, "file:", file);
	},
	removeStudentPhoto: function(studentRegistrationId) {
		var now = new Date();
		logMethodCall(now, "removeStudentPhoto", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var studentRegistration = StudentRegistrations.findOne({_id: studentRegistrationId});
		if (!studentRegistration) {
			throw new Meteor.Error("no-student-registration", "There is no student registration with this id");
		}
		var courseId = studentRegistration.courseId;
		if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove student photos in this course");
		}
		return StudentPhotoFiles.remove({"metadata.studentRegistrationId": studentRegistrationId});
	},
});
