_updateAnnotationResponseScore = function(annotationResponseId) {
	var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId});
	var score = (annotationResponse.upvoteUserIds?annotationResponse.upvoteUserIds.length:0)
		- (annotationResponse.downvoteUserIds?annotationResponse.downvoteUserIds.length:0);
	AnnotationResponses.update({_id: annotationResponseId}, {$set: {score: score}});
};


_updateAnnotationScore = function(annotationId) {
	var annotation = Annotations.findOne({_id: annotationId});
	var score = (annotation.upvoteUserIds?annotation.upvoteUserIds.length:0)
		- (annotation.downvoteUserIds?annotation.downvoteUserIds.length:0);
	Annotations.update({_id: annotationId}, {$set: {score: score}});
};


Meteor.methods({
	createSectionAnnotation: function(sectionId) {
		var now = new Date();
		logMethodCall(now, "createSectionAnnotation", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var section = Sections.findOne({_id: sectionId});
		if (!section) {
			throw new Meteor.Error("section-does-not-exist", "The specified section does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, section.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create an annotation.");
		}
		var userId = this.userId;
		return Annotations.insert({
			courseId: section.courseId,
			sectionId: sectionId,
			userId: userId,
			creationDate: now,
			modificationDate: now,
			removed: false,
			kind: "none",
			score: 0,
			log: [{what: "Created", when: now, who: userId}],
		});
	},
	createSkillAnnotation: function(skillId) {
		var now = new Date();
		logMethodCall(now, "createSkillAnnotation", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var skill = Skills.findOne({_id: skillId});
		if (!skill) {
			throw new Meteor.Error("skill-does-not-exist", "The specified skill does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, skill.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create an annotation.");
		}
		var userId = this.userId;
		return Annotations.insert({
			courseId: skill.courseId,
			skillId: skillId,
			userId: userId,
			creationDate: now,
			modificationDate: now,
			removed: false,
			kind: "none",
			score: 0,
			log: [{what: "Created", when: now, who: userId}],
		});
	},
	createContentItemAnnotation: function(contentItemId) {
		var now = new Date();
		logMethodCall(now, "createContentItemAnnotation", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("item-does-not-exist", "The specified item does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create an annotation.");
		}
		var userId = this.userId;
		return Annotations.insert({
			courseId: contentItem.courseId,
			contentItemId: contentItemId,
			userId: userId,
			creationDate: now,
			modificationDate: now,
			removed: false,
			kind: "none",
			score: 0,
			log: [{what: "Created", when: now, who: userId}],
		});
	},
	updateAnnotationKind: function(annotationId, kind) {
		var now = new Date();
		logMethodCall(now, "updateAnnotationKind", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotation = Annotations.findOne({_id: annotationId, removed: false});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (annotation.userId!==this.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this annotation.");
		}
		var userId = this.userId;
		// In the future, we may want to have users explicitly _publish_ annotations
		// and then we'd insert a feed event only when an annotation is published
		FeedEvents.insert({userId: this.userId, courseId: annotation.courseId, date: now, active: true, event: "ICreatedAnAnnotation", annotationId: annotationId});
		return Annotations.update({_id: annotationId}, {
			$set: {
				modificationDate: now,
				kind: kind,
			},
			$push: {
				log: {what: "Modified", when: now, who: userId},
			},
		});
	},
	updateAnnotationText: function(annotationId, text) {
		var now = new Date();
		logMethodCall(now, "updateAnnotationText", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotation = Annotations.findOne({_id: annotationId, removed: false});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (annotation.userId!==this.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this annotation.");
		}
		var userId = this.userId;
		return Annotations.update({_id: annotationId}, {
			$set: {
				modificationDate: now,
				text: text,
			},
			$push: {
				log: {what: "Modified", when: now, who: userId},
			},
		});
	},
	removeAnnotation: function(annotationId) {
		var now = new Date();
		logMethodCall(now, "removeAnnotation", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotation = Annotations.findOne({_id: annotationId, removed: {$ne: true}});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (annotation.userId!==this.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this annotation.");
		}
		var userId = this.userId;
		return Annotations.update({_id: annotationId}, {
			$set: {
				removalDate: now,
				removed: true,
			},
			$push: {
				log: {what: "Removed", when: now, who: userId},
			},
		});
	},
	submitNewAnswer: function(annotationId, answerText) {
		var now = new Date();
		logMethodCall(now, "submitNewAnswer", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotation = Annotations.findOne({_id: annotationId, removed: false});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotation.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit an answer.");
		}
		var userId = this.userId;
		var annotationResponseId = AnnotationResponses.insert({
			courseId: annotation.courseId,
			annotationId: annotationId,
			creationDate: now,
			modificationDate: now,
			userId: userId,
			text: answerText,
			score: 0,
			removed: false,
		});
		FeedEvents.insert({userId: this.userId, courseId: annotation.courseId, date: now, active: true, event: "IAnsweredAQuestion", annotationResponseId: annotationResponseId});
		FeedEvents.insert({userId: annotation.userId, courseId: annotation.courseId, date: now, active: false, event: "SomeoneAnsweredMyQuestion", annotationResponseId: annotationResponseId});
		return annotationResponseId;
	},
	updateAnswerText: function(annotationResponseId, answerText) {
		var now = new Date();
		logMethodCall(now, "updateAnswerText", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId});
		if (!annotationResponse) {
			throw new Meteor.Error("annotation-response-does-not-exist", "The specified annotation response does not exist.");
		}
		if (this.userId!==annotationResponse.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this answer.");
		}
		var userId = this.userId;
		return AnnotationResponses.update({
			_id: annotationResponseId,
		}, {
			$set: {
				text: answerText,
				modificationDate: now,
			},
		});
	},
	removeAnswer: function(annotationResponseId) {
		var now = new Date();
		logMethodCall(now, "removeAnswer", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId, removed: {$ne: true}});
		if (!annotationResponse) {
			throw new Meteor.Error("annotation-response-does-not-exist", "The specified annotation response does not exist.");
		}
		if (this.userId!==annotationResponse.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this answer.");
		}
		var userId = this.userId;
		return AnnotationResponses.update({
			_id: annotationResponseId,
		}, {
			$set: {
				removalDate: now,
				removed: true,
			},
			$push: {
				log: {what: "Removed", when: now, who: userId},
			},
		});
	},
	acceptAnswer: function(annotationResponseId) {
		var now = new Date();
		logMethodCall(now, "acceptAnswer", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId});
		if (!annotationResponse) {
			throw new Meteor.Error("annotation-response-does-not-exist", "The specified annotation response does not exist.");
		}
		var annotation = Annotations.findOne({_id: annotationResponse.annotationId});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The corresponding annotation does not exist.");
		}
		if (this.userId!==annotation.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to accept an answer.");
		}
		var userId = this.userId;
		FeedEvents.insert({userId: this.userId, courseId: annotation.courseId, date: now, active: true, event: "IAcceptedAnAnswer", annotationResponseId: annotationResponseId});
		FeedEvents.insert({userId: annotationResponse.userId, courseId: annotation.courseId, date: now, active: false, event: "SomeoneAcceptedMyAnswer", annotationResponseId: annotationResponseId});
		return AnnotationResponses.update({
			_id: annotationResponseId,
		}, {
			$set: {
				accepted: true,
			},
		});
	},
	upvoteQuestion: function(annotationId) {
		var now = new Date();
		logMethodCall(now, "upvoteQuestion", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to upvote a question.");
		}
		var annotation = Annotations.findOne({_id: annotationId});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotation.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to upvote.");
		}
		if (annotation.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not upvote your own question.");
		}
		Annotations.update({_id: annotationId}, {
			$addToSet: {
				upvoteUserIds: this.userId,
			},
			$pullAll: {
				downvoteUserIds: [this.userId],
			}
		});
		_updateAnnotationScore(annotationId);
	},
	downvoteQuestion: function(annotationId) {
		var now = new Date();
		logMethodCall(now, "downvoteQuestion", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to downvote a question.");
		}
		var annotation = Annotations.findOne({_id: annotationId});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotation.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to downvote.");
		}
		if (annotation.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not downvote your own question.");
		}
		Annotations.update({_id: annotationId}, {
			$addToSet: {
				downvoteUserIds: this.userId,
			},
			$pullAll: {
				upvoteUserIds: [this.userId],
			}
		});
		_updateAnnotationScore(annotationId);
	},
	clearvoteQuestion: function(annotationId) {
		var now = new Date();
		logMethodCall(now, "clearvoteQuestion", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to clear your vote on a question.");
		}
		var annotation = Annotations.findOne({_id: annotationId});
		if (!annotation) {
			throw new Meteor.Error("annotation-does-not-exist", "The specified annotation does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotation.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to clear your vote.");
		}
		if (annotation.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not clear your vote on your own question.");
		}
		Annotations.update({_id: annotationId}, {
			$pullAll: {
				downvoteUserIds: [this.userId],
				upvoteUserIds: [this.userId],
			}
		});
		_updateAnnotationScore(annotationId);
	},
	upvoteAnswer: function(annotationResponseId) {
		var now = new Date();
		logMethodCall(now, "upvoteAnswer", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to vote on an answer.");
		}
		var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId});
		if (!annotationResponse) {
			throw new Meteor.Error("annotation-response-does-not-exist", "The specified annotation response does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotationResponse.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to vote.");
		}
		if (annotationResponse.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not vote on your own answer.");
		}
		AnnotationResponses.update({_id: annotationResponseId}, {
			$addToSet: {
				upvoteUserIds: this.userId,
			},
			$pullAll: {
				downvoteUserIds: [this.userId],
			}
		});
		_updateAnnotationResponseScore(annotationResponseId);
	},
	downvoteAnswer: function(annotationResponseId) {
		var now = new Date();
		logMethodCall(now, "downvoteAnswer", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to vote on an answer.");
		}
		var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId});
		if (!annotationResponse) {
			throw new Meteor.Error("annotation-response-does-not-exist", "The specified annotation response does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotationResponse.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to vote.");
		}
		if (annotationResponse.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not vote on your own answer.");
		}
		AnnotationResponses.update({_id: annotationResponseId}, {
			$addToSet: {
				downvoteUserIds: this.userId,
			},
			$pullAll: {
				upvoteUserIds: [this.userId],
			}
		});
		_updateAnnotationResponseScore(annotationResponseId);
	},
	clearvoteAnswer: function(annotationResponseId) {
		var now = new Date();
		logMethodCall(now, "clearvoteAnswer", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to vote on an answer.");
		}
		var annotationResponse = AnnotationResponses.findOne({_id: annotationResponseId});
		if (!annotationResponse) {
			throw new Meteor.Error("annotation-response-does-not-exist", "The specified annotation response does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, annotationResponse.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to vote.");
		}
		if (annotationResponse.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not vote on your own answer.");
		}
		AnnotationResponses.update({_id: annotationResponseId}, {
			$pullAll: {
				upvoteUserIds: [this.userId],
				downvoteUserIds: [this.userId],
			}
		});
		_updateAnnotationResponseScore(annotationResponseId);
	},
});
