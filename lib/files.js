Meteor.methods({
  courseFileUploadFinished: function(courseFileId) {
    var now = new Date();
    logMethodCall(now, "courseFileUploadFinished", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in.");
    }
    var file = CourseFiles.findOne({_id: new Mongo.ObjectID(courseFileId)});
    if (!file) {
      throw new Meteor.Error("file-does-not-exist", "The specified file does not exist.");
    }
    console.log("CourseFile:", file);
    var courseId = file.metadata.courseId;
    if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
      throw new Meteor.Error("no-permission", "You don't have permissions to submit. How could you actually upload this?");
    }
    // TODO: Shouldn't we delete the uploaded file if any exception is thrown above??
    console.log("Meteor.methods.courseFileUploadFinished: ownerId:", this.userId, "date:", now, "file:", file);
  },
  removeCourseFile: function(courseFileId) {
    var now = new Date();
    logMethodCall(now, "removeCourseFile", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in.");
    }
    var file = CourseFiles.findOne({_id: new Mongo.ObjectID(courseFileId)});
    if (!file) {
      throw new Meteor.Error("file-does-not-exist", "The specified file does not exist.");
    }
    var courseId = file.metadata.courseId;
    if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
      throw new Meteor.Error("no-permission", "You don't have permissions to remove files in this course");
    }
    return CourseFiles.remove({_id: courseFileId});
  },
});
