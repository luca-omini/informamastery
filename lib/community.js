Meteor.methods({
  updateCommunityGetToKnowResults: function(requestedUserId, pictureByName, correct) {
    var now = new Date();
		logMethodCall(now, "updateCommunityGetToKnowResults", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a buddy preference.");
		}
    console.log(Meteor.users.findOne({_id: this.userId}));
    var update = {};
    update["communityKnowledge."+requestedUserId+"."+(pictureByName?"pictureByName":"nameByPicture")+"."+(correct?"correct":"incorrect")] = 1;
    console.log(update);
    Meteor.users.update({_id: this.userId},
      {$inc: update}
    );
    console.log(Meteor.users.findOne({_id: this.userId}));
  },
});
