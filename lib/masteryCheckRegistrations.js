Meteor.methods({
  createMasteryCheckRegistration: function(courseId, labId) {
    var now = new Date();
    logMethodCall(now, "createMasteryCheckRegistration", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to create a mastery check registration.");
    }
    var creatorId = this.userId;
    if (labId) {
      // check related to a lab, with team
      var lab = Labs.findOne({_id: labId});
      if (!lab) {
        throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
      }
      var labTeam = LabTeams.findOne({labId: labId, memberIds: creatorId});
      if (!labTeam) {
        throw new Meteor.Error("lab-has-no-team", "The specified lab has no team.");
      }
      studentIds = labTeam.memberIds;
    } else {
      // check unrelated to a lab, individual
      studentIds = [creatorId];
    }
    var masteryCheckRegistrationId = MasteryCheckRegistrations.insert({
      courseId: courseId,
      creatorId: creatorId,
      studentIds: studentIds,
      creationDate: now,
      labId: labId,
    });
    return masteryCheckRegistrationId;
  },

  addTopicToMasteryCheckRegistration: function(masteryCheckRegistrationId, topicId) {
    var now = new Date();
    logMethodCall(now, "addTopicToMasteryCheckRegistration", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to update a mastery check registration.");
    }
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId, studentIds: this.userId});
    if (!masteryCheckRegistration) {
      throw new Meteor.Error("mastery-check-registration-does-not-exist", "You have no such mastery check registration.");
    }
    MasteryCheckRegistrations.update({
      _id: masteryCheckRegistrationId,
    }, {
      $addToSet: {
        topicIds: topicId,
      },
    });
  },

  removeTopicFromMasteryCheckRegistration: function(masteryCheckRegistrationId, topicId) {
    var now = new Date();
    logMethodCall(now, "removeTopicFromMasteryCheckRegistration", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to update a mastery check registration.");
    }
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId, studentIds: this.userId});
    if (!masteryCheckRegistration) {
      throw new Meteor.Error("mastery-check-registration-does-not-exist", "You have no such mastery check registration.");
    }
    MasteryCheckRegistrations.update({
      _id: masteryCheckRegistrationId,
    }, {
      $pull: {
        topicIds: topicId,
      },
    });
  },

  deleteMasteryCheckRegistration: function(masteryCheckRegistrationId) {
    var now = new Date();
    logMethodCall(now, "deleteMasteryCheckRegistration", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to delete a mastery check registration.");
    }
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId});
    if (!masteryCheckRegistration) {
      throw new Meteor.Error("mastery-check-registration-does-not-exist", "There is no such mastery check registration.");
    }
    if (isOnTeachingTeam(this.userId, masteryCheckRegistration.courseId)) {
      MasteryCheckRegistrations.remove({_id: masteryCheckRegistrationId});
    } else {
      // only delete if we are part of the team
      MasteryCheckRegistrations.remove({_id: masteryCheckRegistrationId, studentIds: this.userId});
    }
  },
});
