Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound'
});


// Copied from:
// https://github.com/iron-meteor/iron-router/commit/65b844eb0d988d93b1ac8a5bf54966d4dd2f0c46#commitcomment-9236380
Router.onAfterAction(function() {
  //console.log("Router.onAfterAction");
  var self = this;
  // always start by resetting scroll to top of the page
  $(window).scrollTop(0);
  // if there is a hash in the URL, handle it
  if (this.params.hash) {
    // now this is important : Deps.afterFlush ensures that iron-router rendering
    // process has finished inserting the current route template into DOM so we
    // can manipulate it via jQuery, if you skip this part the HTML element you
    // want to scroll to might not yet be present in the DOM (this is probably
    // why your code fails in the first place)
    Tracker.afterFlush(function() {
      //console.log("Router.onAfterAction :: Tracker.afterFlush");
      //console.log("self.params.hash="+self.params.hash);
      //console.log($("#"+self.params.hash));
      if (typeof $("#" + self.params.hash).offset() != "undefined"){
        var scrollTop = $("#" + self.params.hash).offset().top;
        //console.log("scrollTop="+scrollTop);
        $("html,body").animate({
          scrollTop: scrollTop
        });

      }

    });
  }
});


//--- routes

Router.route('/', function() {
  this.render('home');
}, {
  name: 'home',
});

Router.route('/hrm', function() {
  this.render('hrm');
}, {
  name: 'hrm',
});

Router.route('/about', function() {
  this.render('about');
}, {
  name: 'about',
});

Router.route('/learning-tips', function() {
  this.render('learning-tips');
}, {
  name: 'learning-tips',
});

Router.route('/privacy', function() {
  this.render('privacy');
}, {
  name: 'privacy',
});

Router.route('/terms-of-use', function() {
  this.render('terms-of-use');
}, {
  name: 'terms-of-use',
});


Router.route('/admin/dashboard', function() {
  this.render('adminDashboardPage');
}, {
  name: 'adminDashboardPage',
});


Router.route('/admin/labSubmissionFiles', function() {
  this.render('adminLabSubmissionFilesPage');
}, {
  name: 'adminLabSubmissionFilesPage',
});


Router.route('/admin/courses', function() {
  this.render('adminCourseListPage');
}, {
  name: 'adminCourseListPage',
});


Router.route('/admin/users', function() {
  this.render('adminUserListPage');
}, {
  name: 'adminUserListPage',
});


Router.route('/user/:_id', function() {
  var user = Meteor.users.findOne({_id: this.params._id});
  this.render('adminUserPage', {
    data: function() {
      return user;
    }
  });
}, {
  name: 'adminUserPage',
});


Router.route('/profile', function() {
  this.render('profilePage');
}, {
  name: 'profilePage',
});


Router.route('/courses', function() {
  this.layout('sidebarLayout');
  this.render('courseListPage');
  this.render('coursesSidebar', {
    to: 'sidebar',
  });
}, {
  name: "courseListPage",
  waitOn: function() {
    Meteor.subscribe("courses");
  },
});


Router.route('/course/:courseId', function() {
  console.log("ROUTE coursePage");
  var c = Courses.findOne({_id: this.params.courseId});
  Session.set('course', c);
  if (Meteor.userId()) {
  //if (Meteor.userId() && isStudent(Meteor.userId(), c._id)) {
    //console.log("REDIRECTING to studentPage");
    //var studentRegistration = StudentRegistrations.findOne({studentId: Meteor.userId(), courseId: c._id});
    //console.log(studentRegistration);
    //Router.go("studentPage", {courseId: c._id, _id: studentRegistration._id});
    Router.go("userHomePage", {courseId: c._id, userId: Meteor.userId()});
  } else {
    console.log("REDIRECTING to themeListPage");
    Router.go("themeListPage", {courseId: c._id});
  }
}, {
  name: 'coursePage'
});


Router.route('/course/:courseId/syllabus', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('syllabusPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'syllabusPage'
});


Router.route('/course/:courseId/syllabus/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('syllabusEditPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'syllabusEditPage'
});


Router.route('/course/:_id/community', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('communityPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'communityPage'
});

Router.route('/course/:courseId/community/studentTable', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('communityStudentTablePage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'communityStudentTablePage'
});

Router.route('/course/:courseId/community/addStudents', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('communityAddStudentsPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'communityAddStudentsPage'
});

Router.route('/course/:courseId/community/studentPhotos', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('communityStudentPhotosPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'communityStudentPhotosPage'
});

Router.route('/course/:courseId/community/getToKnowStudents', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('communityGetToKnowStudentsPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'communityGetToKnowStudentsPage'
});


Router.route('/course/:courseId/misconceptions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('misconceptionListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'misconceptionListPage'
});

Router.route('/course/:courseId/misconceptions/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('misconceptionListEditPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'misconceptionListEditPage'
});

Router.route('/course/:courseId/misconception/:misconceptionId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('misconceptionPage', {
    data: function() {
      return {
        course: course,
        misconception: Misconceptions.findOne({_id: this.params.misconceptionId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'misconceptionPage'
});

Router.route('/course/:courseId/misconception/:misconceptionId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('misconceptionEditPage', {
    data: function() {
      return {
        course: course,
        misconception: Misconceptions.findOne({_id: this.params.misconceptionId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'misconceptionEditPage'
});


Router.route('/course/:courseId/skills', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('skillListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'skillListPage'
});


Router.route('/course/:courseId/studyTasks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  console.log("subscribing to details-for-study-task-list", this.params.courseId);
  this.subscribe('details-for-study-task-list', this.params.courseId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('studyTaskListPage', {
      data: function() {
        return course;
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studyTaskListPage'
});


Router.route('/course/:courseId/topics', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('topicListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicListPage'
});


Router.route('/course/:courseId/themes', function() {
  this.layout('sidebarLayout');
  var c = Courses.findOne({_id: this.params.courseId});
  var ts = Themes.find({courseId: this.params.courseId, removed: {$ne: true}});
  Session.set('course', c);
  this.render('themeListPage', {
    data: function() {
      return {
        course: c,
        themes: ts,
        topics: Topics.find({courseId: this.params.courseId, removed: {$ne: true}}),
      }
    }
  });
  this.render('courseMenu', {
    data: function() {
      return c
    },
    to: 'sidebar',
  });
}, {
  name: 'themeListPage'
});


Router.route('/course/:courseId/themes/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var themes = Themes.find({courseId: this.params.courseId, removed: {$ne: true}});
  console.log("Themes:");
  console.log(themes.fetch());
  this.render('themeListEditPage', {
    data: function() {
      return {
        course: course,
        themes: themes,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'themeListEditPage'
});


Router.route('/course/:courseId/labs', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labListPage'
});


Router.route('/course/:courseId/labs/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var labs = Labs.find({courseId: this.params.courseId, removed: {$ne: true}});
  console.log("Labs:");
  console.log(labs.fetch());
  this.render('labListEditPage', {
    data: function() {
      return {
        course: course,
        labs: labs,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labListEditPage'
});

Router.route('/course/:courseId/labs/outcome', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labListOutcomePage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labListOutcomePage'
});


Router.route('/course/:courseId/lab/:labId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
        contentItems: ContentItems.find({containerId: this.params.labId, removed: {$ne: true}}, {sort: {index: 1}}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labPage'
});


Router.route('/course/:courseId/lab/:labId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labEditPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
        contentItems: ContentItems.find({containerId: this.params.labId, removed: {$ne: true}}, {sort: {index: 1}}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labEditPage'
});


Router.route('/course/:courseId/lab/:labId/teams', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labTeamsPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
        teams: LabTeams.find({labId: this.params.labId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labTeamsPage'
});

Router.route('/course/:courseId/lab/:labId/reference', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labReferenceSolutionPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labReferenceSolutionPage'
});

Router.route('/course/:courseId/lab/:labId/rubric', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labRubricPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labRubricPage'
});

Router.route('/course/:courseId/lab/:labId/grading', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labGradingPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labGradingPage'
});

Router.route('/course/:courseId/lab/:labId/grading/team/:teamId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labTeamGradingPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
        team: LabTeams.findOne({_id: this.params.teamId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labTeamGradingPage'
});

Router.route('/course/:courseId/lab/:labId/grading/item/:itemId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labItemGradingPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
        item: ContentItems.findOne({_id: this.params.itemId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labItemGradingPage'
});

Router.route('/course/:courseId/lab/:labId/feedback', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labFeedbackPage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
        labFeedbacks: LabFeedbacks.find({labId: this.params.labId}, {sort: {modificationDate: -1}}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labFeedbackPage'
});

Router.route('/course/:courseId/lab/:labId/outcome', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('labOutcomePage', {
    data: function() {
      return {
        course: course,
        lab: Labs.findOne({_id: this.params.labId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'labOutcomePage'
});


Router.route('/course/:courseId/files', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('courseFilesPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'courseFilesPage'
});


Router.route('/course/:courseId/students', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('studentListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentListPage'
});


Router.route('/course/:_id/questions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('courseQuestionsPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'courseQuestionsPage'
});

Router.route('/course/:courseId/communityOpenQuestions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('communityOpenQuestionsPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'communityOpenQuestionsPage'
});

Router.route('/course/:courseId/courseQuestions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('courseQuestionListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'courseQuestionListPage'
});


Router.route('/course/:courseId/question/:questionId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('questionPage', {
    data: function() {
      return {
        course: course,
        question: Annotations.findOne({_id: this.params.questionId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'questionPage'
});


Router.route('/course/:courseId/allPomodoros', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('allPomodoroListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'allPomodoroListPage'
});


Router.route('/course/:courseId/feed', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('courseFeedPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'courseFeedPage'
});


Router.route('/course/:courseId/masteryCheck/:masteryCheckId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('newMasteryCheckPage', {
    data: function() {
      return MasteryChecks.findOne({_id: this.params.masteryCheckId});
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'newMasteryCheckPage'
});
Router.route('/course/:courseId/masteryChecks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('masteryCheckListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'masteryCheckListPage'
});
Router.route('/course/:_id/masteryChecksOngoing', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('masteryCheckOngoingListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'masteryCheckOngoingListPage'
});
Router.route('/course/:_id/masteryCheckRegistrations', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('masteryCheckRegistrationListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'masteryCheckRegistrationListPage'
});
Router.route('/course/:_id/masteryCheckQueue', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('masteryCheckQueuePage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'masteryCheckQueuePage'
});
Router.route('/course/:_id/masteryCheckPreparation', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('masteryCheckPreparationPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'masteryCheckPreparationPage'
});


Router.route('/course/:_id/nextMasteryCheckTopics', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('nextMasteryCheckTopicsPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'nextMasteryCheckTopicsPage'
});


Router.route('/course/:_id/topicStudentMatrix', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('topicStudentMatrixPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicStudentMatrixPage'
});


Router.route('/course/:_id/weekStudentMatrix', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params._id});
  Session.set('course', course);
  this.render('weekStudentMatrixPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'weekStudentMatrixPage'
});


Router.route('/course/:courseId/student/:_id', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id})
  console.log("subscribing to details-for-user-recalls", studentRegistration.studentId, this.params.courseId);
  this.subscribe('details-for-user-recalls', studentRegistration.studentId, this.params.courseId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('studentPage', {
      data: function() {
        return studentRegistration;
      },
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentPage'
});


Router.route('/course/:courseId/student/:_id/feed', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentFeedPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentFeedPage'
});


Router.route('/course/:courseId/student/:_id/grade', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentGradePage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentGradePage'
});


Router.route('/course/:courseId/student/:_id/pomodoros', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentPomodoroListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentPomodoroListPage'
});


Router.route('/course/:courseId/student/:_id/questions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentQuestionListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentQuestionListPage'
});


Router.route('/course/:courseId/student/:_id/notes', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentNoteListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentNoteListPage'
});


Router.route('/course/:courseId/student/:_id/tags', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentTagListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentTagListPage'
});


Router.route('/course/:courseId/student/:_id/bookmarks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentBookmarkListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentBookmarkListPage'
});


Router.route('/course/:courseId/student/:_id/questionsOld', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentQuestionPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentQuestionPage'
});


Router.route('/course/:courseId/ttPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('ttPracticeProblemsPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'ttPracticeProblemsPage'
});


Router.route('/course/:courseId/coursePracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('coursePracticeProblemListPage', {
    data: function() {
      return course;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'coursePracticeProblemListPage'
});


Router.route('/course/:courseId/student/:_id/ownPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentOwnPracticeProblemListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentOwnPracticeProblemListPage'
});


Router.route('/course/:courseId/student/:_id/solvedPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentSolvedPracticeProblemListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentSolvedPracticeProblemListPage'
});


Router.route('/course/:courseId/student/:_id/unsolvedPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentUnsolvedPracticeProblemListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentUnsolvedPracticeProblemListPage'
});


Router.route('/course/:courseId/student/:_id/masteredTopics', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentPassedChecksPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentPassedChecksPage'
});


Router.route('/course/:courseId/student/:_id/bugs', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentFailedChecksPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentFailedChecksPage'
});





Router.route('/course/:courseId/user/:userId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userPage'
});


Router.route('/course/:courseId/user/:userId/home', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userHomePage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userHomePage'
});


Router.route('/course/:courseId/user/:userId/skills', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userSkillListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userSkillListPage'
});


Router.route('/course/:courseId/user/:userId/studyTasks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  console.log("subscribing to details-for-user-study-task-list", this.params.courseId, this.params.userId);
  this.subscribe('details-for-user-study-task-list', this.params.courseId, this.params.userId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('userStudyTaskListPage', {
      data: function() {
        return {
          user: Meteor.users.findOne({_id: this.params.userId}),
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userStudyTaskListPage'
});


Router.route('/course/:courseId/user/:userId/labList', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userLabListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userLabListPage'
});


Router.route('/course/:courseId/user/:userId/lab/:labId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userLabPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        lab: Labs.findOne({_id: this.params.labId}),
        labTeam: LabTeams.findOne({labId: this.params.labId, memberIds: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userLabPage'
});


Router.route('/course/:courseId/user/:userId/lab/:labId/feedback', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userLabFeedbackPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        lab: Labs.findOne({_id: this.params.labId}),
        labTeam: LabTeams.findOne({labId: this.params.labId, memberIds: this.params.userId}),
        labFeedback: LabFeedbacks.findOne({labId: this.params.labId, userId: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userLabFeedbackPage'
});


Router.route('/course/:courseId/user/:userId/reading', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  console.log("subscribing to details-for-user-recalls", this.params.userId, this.params.courseId);
  this.subscribe('details-for-user-recalls', this.params.userId, this.params.courseId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('userReadingPage', {
      data: function() {
        return {
          user: Meteor.users.findOne({_id: this.params.userId}),
        };
      },
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userReadingPage'
});


Router.route('/course/:courseId/user/:userId/practiceProblems/own', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userPracticeProblemOwnListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userPracticeProblemOwnListPage'
});


Router.route('/course/:courseId/user/:userId/practiceProblems/unsolved', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userPracticeProblemUnsolvedListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userPracticeProblemUnsolvedListPage'
});


Router.route('/course/:courseId/user/:userId/practiceProblems/solved', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userPracticeProblemSolvedListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userPracticeProblemSolvedListPage'
});


Router.route('/course/:courseId/user/:userId/quizzes', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userQuizListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userQuizListPage'
});


Router.route('/course/:courseId/user/:userId/currentMasteryCheck', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userMasteryCheckCurrentPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userMasteryCheckCurrentPage'
});


Router.route('/course/:courseId/user/:userId/masteryChecks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userMasteryCheckListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userMasteryCheckListPage'
});


Router.route('/course/:courseId/user/:userId/failedTopics', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userFailedTopicListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userFailedTopicListPage'
});


Router.route('/course/:courseId/user/:userId/passedTopics', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userPassedTopicListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userPassedTopicListPage'
});


Router.route('/course/:courseId/user/:userId/questions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userQuestionListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userQuestionListPage'
});


Router.route('/course/:courseId/user/:userId/notes', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userNoteListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userNoteListPage'
});


Router.route('/course/:courseId/user/:userId/tags', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userTagListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userTagListPage'
});


Router.route('/course/:courseId/user/:userId/bookmarks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userBookmarkListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        course: course,
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userBookmarkListPage'
});


Router.route('/course/:courseId/user/:userId/pomodoros', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userPomodoroListPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userPomodoroListPage'
});


Router.route('/course/:courseId/user/:userId/feed', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userFeedPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userFeedPage'
});






Router.route('/course/:courseId/student/:_id/reading', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  console.log("subscribing to details-for-user-recalls", studentRegistration.studentId, this.params.courseId);
  this.subscribe('details-for-user-recalls', studentRegistration.studentId, this.params.courseId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('studentReadingPage', {
      data: function() {
        return studentRegistration;
      },
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentReadingPage'
});


Router.route('/course/:courseId/student/:_id/quizzes', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studentRegistration = StudentRegistrations.findOne({_id: this.params._id});
  this.render('studentQuizListPage', {
    data: function() {
      return studentRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studentQuizListPage'
});


Router.route('/course/:courseId/user/:userId/quizAttempt/:quizAttemptId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('userQuizAttemptPage', {
    data: function() {
      return {
        user: Meteor.users.findOne({_id: this.params.userId}),
        quizAttempt: QuizAttempts.findOne({_id: this.params.quizAttemptId}),
      };
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'userQuizAttemptPage'
});



Router.route('/course/:courseId/assistant/:_id', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var assistantRegistration = AssistantRegistrations.findOne({_id: this.params._id});
  this.render('assistantPage', {
    data: function() {
      return assistantRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'assistantPage'
});


Router.route('/course/:courseId/instructor/:_id', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var instructorRegistration = InstructorRegistrations.findOne({_id: this.params._id});
  this.render('instructorPage', {
    data: function() {
      return instructorRegistration;
    },
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'instructorPage'
});


Router.route('/masteryCheck/:masteryCheckId', function() {
  this.layout('sidebarLayout');
  var masteryCheck = MasteryChecks.findOne({_id: this.params.masteryCheckId});
  var course = masteryCheck ? Courses.findOne({_id: masteryCheck.courseId}) : undefined;
  Session.set('course', course);
  this.render('masteryCheckPage', {
    data: function() {
      return masteryCheck;
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'masteryCheckPage'
});


Router.route('/course/:courseId/theme/:themeId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var theme = Themes.findOne({_id: this.params.themeId});
  this.render('themePage', {
    data: function() {
      return theme;
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'themePage'
});


Router.route('/course/:courseId/theme/:themeId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var theme = Themes.findOne({_id: this.params.themeId});
  var topics = Topics.find({themeId: this.params.themeId, removed: {$ne: true}});
  this.render('themeEditPage', {
    data: function() {
      return {
        theme: theme,
        topics: topics,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'themeEditPage'
});


Router.route('/course/:courseId/topic/:topicId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  console.log("subscribing to details-for-topic", this.params.topicId);
  this.subscribe('details-for-topic', this.params.topicId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('topicPage', {
      data: function() {
        return {
          course: course,
          topic: topic,
          theme: theme,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicPage'
});


Router.route('/course/:courseId/topic/:topicId/feedback', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicFeedbackPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicFeedbackPage'
});


Router.route('/course/:courseId/topic/:topicId/passedChecks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicPassedChecksPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicPassedChecksPage'
});


Router.route('/course/:courseId/topic/:topicId/failedChecks', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicFailedChecksPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicFailedChecksPage'
});


Router.route('/course/:courseId/topic/:topicId/questions', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicQuestionsPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicQuestionsPage'
});


Router.route('/course/:courseId/topic/:topicId/ownPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicOwnPracticeProblemListPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicOwnPracticeProblemListPage'
});


Router.route('/course/:courseId/topic/:topicId/solvedPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicSolvedPracticeProblemListPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicSolvedPracticeProblemListPage'
});


Router.route('/course/:courseId/topic/:topicId/unsolvedPracticeProblems', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicUnsolvedPracticeProblemListPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicUnsolvedPracticeProblemListPage'
});


Router.route('/course/:courseId/topic/:topicId/practiceProblem/:practiceProblemId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  var practiceProblem = PracticeProblems.findOne({_id: this.params.practiceProblemId});
  this.render('editPracticeProblemPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
        practiceProblem: practiceProblem,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'editPracticeProblemPage'
});


Router.route('/course/:courseId/topic/:topicId/practiceProblem/:practiceProblemId/view', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  var practiceProblem = PracticeProblems.findOne({_id: this.params.practiceProblemId});
  this.render('viewPracticeProblemPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
        practiceProblem: practiceProblem,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'viewPracticeProblemPage'
});


Router.route('/course/:courseId/topic/:topicId/print', function() {
  this.layout('printLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicPrint', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
}, {
  name: 'topicPrint'
});


Router.route('/course/:courseId/topic/:topicId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var topic = Topics.findOne({_id: this.params.topicId});
  var theme = topic? Themes.findOne({_id: topic.themeId}) : undefined;
  this.render('topicEditPage', {
    data: function() {
      return {
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'topicEditPage'
});


Router.route('/course/:courseId/studyTask/:studyTaskId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studyTask = StudyTasks.findOne({_id: this.params.studyTaskId});
  var topic = studyTask ? Topics.findOne({_id: studyTask.topicId}) : undefined;
  var theme = topic ? Themes.findOne({_id: topic.themeId}) : undefined;
  var course = topic ? Courses.findOne({_id: topic.courseId}) : undefined;
  console.log("subscribing to study-task-status", this.params.sectionId);
  this.subscribe('study-task-status', this.params.studyTaskId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('studyTaskPage', {
      data: function() {
        return {
          course: course,
          theme: theme,
          topic: topic,
          studyTask: studyTask,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  /*
  this.render('studyTaskPage', {
    data: function() {
      var theme = topic ? Themes.findOne({_id: topic.themeId}) : undefined;
      return {
        studyTask: studyTask,
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  */
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studyTaskPage'
});


Router.route('/course/:courseId/studyTask/:studyTaskId/recalls', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studyTask = StudyTasks.findOne({_id: this.params.studyTaskId});
  var topic = studyTask ? Topics.findOne({_id: studyTask.topicId}) : undefined;
  var theme = topic ? Themes.findOne({_id: topic.themeId}) : undefined;
  var course = topic ? Courses.findOne({_id: topic.courseId}) : undefined;
  console.log("subscribing to study-task-status", this.params.sectionId);
  this.subscribe('study-task-status', this.params.studyTaskId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('studyTaskRecallsPage', {
      data: function() {
        return {
          course: course,
          theme: theme,
          topic: topic,
          studyTask: studyTask,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studyTaskRecallsPage'
});


Router.route('/course/:courseId/studyTask/:studyTaskId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var studyTask = StudyTasks.findOne({_id: this.params.studyTaskId});
  var topic = studyTask ? Topics.findOne({_id: studyTask.topicId}) : undefined;
  this.render('studyTaskEditPage', {
    data: function() {
      var theme = topic ? Themes.findOne({_id: topic.themeId}) : undefined;
      return {
        studyTask: studyTask,
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'studyTaskEditPage'
});



Router.route('/course/:courseId/skill/:skillId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var skill = Skills.findOne({_id: this.params.skillId});
  var topic = skill ? Topics.findOne({_id: skill.topicId}) : undefined;
  this.render('skillPage', {
    data: function() {
      var theme = topic ? Themes.findOne({_id: topic.themeId}) : undefined;
      return {
        skill: skill,
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'skillPage'
});


Router.route('/course/:courseId/skill/:skillId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var skill = Skills.findOne({_id: this.params.skillId});
  var topic = skill ? Topics.findOne({_id: skill.topicId}) : undefined;
  this.render('skillEditPage', {
    data: function() {
      var theme = topic ? Themes.findOne({_id: topic.themeId}) : undefined;
      return {
        skill: skill,
        course: course,
        topic: topic,
        theme: theme,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'skillEditPage'
});



Router.route('/course/:courseId/book/:bookId/chapter/:chapterId/section/:sectionId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapter = Chapters.findOne({_id: this.params.chapterId});
  var section = Sections.findOne({_id: this.params.sectionId});
  console.log("subscribing to details-for-section", this.params.sectionId);
  this.subscribe('details-for-section', this.params.sectionId).wait();
  if (this.ready()) {
    console.log("(sectionPage) subscription ready.");
    this.render('sectionPage', {
      data: function() {
        return {
          course: course,
          book: book,
          chapter: chapter,
          section: section,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'sectionPage'
});



Router.route('/course/:courseId/book/:bookId/chapter/:chapterId/section/:sectionId/recalls', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapter = Chapters.findOne({_id: this.params.chapterId});
  var section = Sections.findOne({_id: this.params.sectionId});
  console.log("subscribing to details-for-section", this.params.sectionId);
  this.subscribe('details-for-section', this.params.sectionId).wait();
  if (this.ready()) {
    console.log("(sectionRecallsPage) subscription ready.");
    this.render('sectionRecallsPage', {
      data: function() {
        return {
          course: course,
          book: book,
          chapter: chapter,
          section: section,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'sectionRecallsPage'
});


Router.route('/course/:courseId/book/:bookId/chapter/:chapterId/section/:sectionId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapter = Chapters.findOne({_id: this.params.chapterId});
  var section = Sections.findOne({_id: this.params.sectionId});
  this.render('sectionEditPage', {
    data: function() {
      return {
        course: course,
        book: book,
        chapter: chapter,
        section: section,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'sectionEditPage'
});


Router.route('/course/:courseId/book/:bookId/chapter/:chapterId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapter = Chapters.findOne({_id: this.params.chapterId});
  var sections = Sections.find({chapterId: this.params.chapterId, removed: false}, {sort: {sortKey: 1}});
  console.log("subscribing to details-for-chapter", this.params.chapterId);
  this.subscribe('details-for-chapter', this.params.chapterId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('chapterPage', {
      data: function() {
        return {
          course: course,
          book: book,
          chapter: chapter,
          sections: sections,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'chapterPage'
});


Router.route('/course/:courseId/book/:bookId/chapter/:chapterId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapter = Chapters.findOne({_id: this.params.chapterId});
  var sections = Sections.find({chapterId: this.params.chapterId, removed: false}, {sort: {sortKey: 1}});
  this.render('chapterEditPage', {
    data: function() {
      return {
        course: course,
        book: book,
        chapter: chapter,
        sections: sections,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'chapterEditPage'
});


Router.route('/course/:courseId/book/:bookId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapters = Chapters.find({bookId: this.params.bookId, removed: false}, {sort: {sortKey: 1}});
  console.log("subscribing to details-for-book", this.params.bookId);
  this.subscribe('details-for-book', this.params.bookId).wait();
  if (this.ready()) {
    console.log("subscription ready.");
    this.render('bookPage', {
      data: function() {
        return {
          course: course,
          book: book,
          chapters: chapters,
        };
      }
    });
  } else {
    console.log("subscription loading...");
    this.render('loading');
  }
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'bookPage'
});


Router.route('/course/:courseId/book/:bookId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var book = Books.findOne({_id: this.params.bookId});
  var chapters = Chapters.find({bookId: this.params.bookId, removed: false}, {sort: {sortKey: 1}});
  this.render('bookEditPage', {
    data: function() {
      return {
        course: course,
        book: book,
        chapters: chapters,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'bookEditPage'
});


Router.route('/course/:courseId/books', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var books = Books.find({courseId: this.params.courseId, removed: false});
  this.render('bookListPage', {
    data: function() {
      return {
        course: course,
        books: books,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'bookListPage'
});


Router.route('/course/:courseId/books/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var books = Books.find({courseId: this.params.courseId, removed: false});
  console.log("Books:");
  console.log(books.fetch());
  this.render('bookListEditPage', {
    data: function() {
      return {
        course: course,
        books: books,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'bookListEditPage'
});




Router.route('/course/:courseId/quiz/:quizId', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var quiz = Quizzes.findOne({_id: this.params.quizId});
  //var chapters = Chapters.find({bookId: this.params.bookId, removed: false}, {sort: {sortKey: 1}});
  this.render('quizPage', {
    data: function() {
      return {
        course: course,
        quiz: quiz,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizPage'
});

/*
// for debugging
Router.onBeforeAction(function () {
  console.log("this: ", this);
  console.log("params: ", this.params);
  this.next();
});
*/

Router.route('/course/:courseId/quiz/:quizId/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  // Could it be that this query on Quizzes causes iron router
  // to rerender the entire page whenever the current quiz changes??
  //var quiz = Quizzes.findOne({_id: this.params.quizId});
  //var chapters = Chapters.find({bookId: this.params.bookId, removed: false}, {sort: {sortKey: 1}});
  this.render('quizEditPage', {
    data: function() {
      var quiz = Quizzes.findOne({_id: this.params.quizId});
      return {
        course: course,
        quiz: quiz,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizEditPage'
});

Router.route('/course/:courseId/quiz/:quizId/outcome', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var quiz = Quizzes.findOne({_id: this.params.quizId});
  this.render('quizOutcomePage', {
    data: function() {
      return {
        course: course,
        quiz: quiz,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizOutcomePage'
});

Router.route('/course/:courseId/quiz/:quizId/feedback', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var quiz = Quizzes.findOne({_id: this.params.quizId});
  this.render('quizFeedbackPage', {
    data: function() {
      return {
        course: course,
        quiz: quiz,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizFeedbackPage'
});

Router.route('/course/:courseId/quiz/:quizId/run', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('quizRunPage', {
    data: function() {
      var quiz = Quizzes.findOne({_id: this.params.quizId});
      return {
        course: course,
        quiz: quiz,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizRunPage'
});

Router.route('/course/:courseId/quiz/:quizId/take', function() {
  this.layout('quizLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('quizTakePage', {
    data: function() {
      var quiz = Quizzes.findOne({_id: this.params.quizId});
      return {
        course: course,
        quiz: quiz,
      };
    }
  });
}, {
  name: 'quizTakePage'
});

Router.route('/course/:courseId/quizzes', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var quizzes = Quizzes.find({courseId: this.params.courseId, removed: false}, {sort: {date: 1}});
  this.render('quizListPage', {
    data: function() {
      return {
        course: course,
        quizzes: quizzes,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizListPage'
});


Router.route('/course/:courseId/quizzes/outcome', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var quizzes = Quizzes.find({courseId: this.params.courseId, removed: false}, {sort: {date: 1}});
  this.render('quizListOutcomePage', {
    data: function() {
      return {
        course: course,
        quizzes: quizzes,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizListOutcomePage'
});


Router.route('/course/:courseId/quizzes/edit', function() {
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  var quizzes = Quizzes.find({courseId: this.params.courseId, removed: false}, {sort: {date: 1}});
  console.log("Quizzes:");
  console.log(quizzes.fetch());
  this.render('quizListEditPage', {
    data: function() {
      return {
        course: course,
        quizzes: quizzes,
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'quizListEditPage'
});




Router.route('/course/:courseId/classSessions', function() {
  var self = this;
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('classSessionListPage', {
    data: function() {
      return {
        course: course,
        classSessions: ClassSessions.find({courseId: self.params.courseId, removed: false}, {sort: {date: 1}}),
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'classSessionListPage'
});


Router.route('/course/:courseId/classSessions/edit', function() {
  var self = this;
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('classSessionListEditPage', {
    data: function() {
      return {
        course: course,
        classSessions: ClassSessions.find({courseId: self.params.courseId, removed: false}, {sort: {date: 1}}),
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'classSessionListEditPage'
});


Router.route('/course/:courseId/classSession/:classSessionId', function() {
  var self = this;
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('classSessionPage', {
    data: function() {
      return {
        course: course,
        classSession: ClassSessions.findOne({_id: self.params.classSessionId}),
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'classSessionPage'
});


Router.route('/course/:courseId/classSession/:classSessionId/edit', function() {
  var self = this;
  this.layout('sidebarLayout');
  var course = Courses.findOne({_id: this.params.courseId});
  Session.set('course', course);
  this.render('classSessionEditPage', {
    data: function() {
      return {
        course: course,
        classSession: ClassSessions.findOne({_id: self.params.classSessionId}),
      };
    }
  });
  this.render('courseMenu', {
    data: function() {
      return course;
    },
    to: 'sidebar',
  });
}, {
  name: 'classSessionEditPage'
});
