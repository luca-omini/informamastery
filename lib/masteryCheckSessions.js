Meteor.methods({
  startNewMasteryCheckSession: function(courseId) {
    var now = new Date();
    logMethodCall(now, "beginMasteryCheckSession", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to begin a mastery check session.");
    }
    if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to begin a mastery check session.");
		}
    // stop potentially ongoing sessions
    MasteryCheckSessions.update({courseId: courseId, ongoing: true}, {
			$set: {
				ongoing: false,
				stopDate: now,
				stoppedBy: this.userId,
			},
		}, {multi:true});
    // start a new one
    return MasteryCheckSessions.insert({
      courseId: courseId,
			ongoing: true,
			startDate: now,
			startedBy: this.userId,
		});
  },

  stopOngoingMasteryCheckSessions: function(courseId) {
    var now = new Date();
    logMethodCall(now, "stopOngoingMasteryCheckSessions", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to stop a mastery check session.");
    }
    if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("not-on-teaching-team", "You must be on the teaching team of this course to stop a mastery check session.");
		}
    MasteryCheckSessions.update({courseId: courseId, ongoing: true}, {
			$set: {
				ongoing: false,
				stopDate: now,
				stoppedBy: this.userId,
			},
		}, {multi:true});
  },
});
