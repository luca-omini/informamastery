logMethodCall = function(date, methodName, arguments) {
  var userId = Meteor.userId();
  if (arguments) {
    console.log("METHOD", date, userId, methodName, arguments);
  } else {
    console.log("METHOD", date, userId, methodName);    
  }
}

logPublication = function(date, userId, publicationName, arguments) {
  if (arguments) {
    console.log("PUBLISH", date, userId, publicationName, arguments);
  } else {
    console.log("PUBLISH", date, userId, publicationName);
  }
}
