Meteor.methods({
	addLab: function(courseId) {
		console.log("Meteor.methods.addLab("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a lab.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, course._id) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a lab.");
		}
		var now = new Date();
		return Labs.insert({
			courseId: courseId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
			status: "hidden",
			log: [{what: "Created", when: now, who: this.userId}],
		});
	},
	updateLab: function(labId, title, description, deadlineDate, groupSize) {
		console.log("Meteor.methods.updateLab("+labId+", "+title+", "+description+", "+deadlineDate+", "+groupSize+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isInstructor(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$set: {
				title: title,
				description: description,
				deadlineDate: deadlineDate,
				groupSize: groupSize,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	showLabAsPreview: function(labId) {
		console.log("Meteor.methods.showLabAsPreview("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to show a lab as preview.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to show this lab as preview.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$set: {
				status: "preview",
			},
			$push: {
				log: {what: "shownAsPreview", when: now, who: this.userId},
			},
		});
	},
	hideLab: function(labId) {
		console.log("Meteor.methods.hideLab("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to hide a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to hide this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$set: {
				status: "hidden",
			},
			$push: {
				log: {what: "Hidden", when: now, who: this.userId},
			},
		});
	},
	openLab: function(labId) {
		console.log("Meteor.methods.openLab("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to open a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to open this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$set: {
				status: "open",
			},
			$push: {
				log: {what: "Opened", when: now, who: this.userId},
			},
		});
	},
	closeLab: function(labId) {
		console.log("Meteor.methods.closeLab("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to close a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to close this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$set: {
				status: "closed",
			},
			$push: {
				log: {what: "Closed", when: now, who: this.userId},
			},
		});
	},
	reopenLab: function(labId) {
		console.log("Meteor.methods.reopenLab("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to reopen a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to reopen this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$set: {
				status: "open",
			},
			$push: {
				log: {what: "Reopened", when: now, who: this.userId},
			},
		});
	},
	addLabTopic: function(labId, topicId) {
		console.log("Meteor.methods.addLabTopic("+labId+", "+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isInstructor(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$addToSet: {
				topicIds: topicId,
			},
		});
	},
	removeLabTopic: function(labId, topicId) {
		console.log("Meteor.methods.removeLabTopic("+labId+", "+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isInstructor(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this lab.");
		}
		var now = new Date();
		return Labs.update({_id: labId}, {
			$pullAll: {
				topicIds: [topicId],
			},
		});
	},
	removeLab: function(labId) {
		console.log("Meteor.methods.removeLab("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a lab.");
		}
		var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
		if (!isInstructor(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this lab.");
		}
		var now = new Date();
		//TODO: check this works!
		ContentItems.update({containerId: labId}, {
			$set: {
				removed: true,
			},
		});
		Labs.update({_id: labId}, {
			$set: {
				removed: true,
			},
		});
	},
});
