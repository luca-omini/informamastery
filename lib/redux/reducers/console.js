import { REMOVE_OUTPUT, UPDATE_CONSOLE_OUTPUT } from '../actionTypes';

const initialState = {
  hasOutput: false,

};

const runner = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CONSOLE_OUTPUT: {
      return {
        ...state,
        hasOutput: true,
        src_compile: action.payload.src_compile,
        src_run: action.payload.src_run,
        test_compile: action.payload.test_compile,
        test_results: action.payload.test_results,
      };
    }
    case REMOVE_OUTPUT: {
      return {
        hasOutput: false,
      };
    }
    default:
      return state;
  }
};

export default runner;
