import { combineReducers } from 'redux';
import runner from './console';
import studyTaskEdit from './studyTaskEdit';

export default combineReducers({ runner, studyTaskEdit });
