import { SAVE_CODE } from '../actionTypes';


const initialState = {
  sources: [
    {
      code: 'package mypkg;\n\npublic class HelloWorld {\n    private static String msg = "HelloWorld";\n\n    public static void main (String[] args) {\n        System.out.println(msg);\n    }\n\n    public String getMsg(){\n        return msg;\n    }\n}\n',
      fqcn: 'mypkg.HelloWorld',
    },
  ],
  tests: [
    {
      code: 'import mypkg.HelloWorld;\nimport org.junit.Test;\nimport static org.junit.Assert.assertEquals;\n\npublic class HelloWorldTest {\n    HelloWorld hw = new HelloWorld();\n\n    @Test\n    public void testPrintMessage() {\n        assertEquals("HelloWorld", hw.getMsg());\n    }\n\n    @Test\n    public void failTest() {\n        assertEquals("HelloFail", hw.getMsg());\n    }\n}',
      fqcn: 'HelloWorldTest',
    },
  ],
};


const editor = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_CODE: {
      const newCode = action.payload;
      return {
        ...state,
        sources: [
          {
            code: newCode,
            fqcn: 'mypkg.HelloWorld',
          },
        ],
        tests: [
          {
            code: 'import mypkg.HelloWorld;\nimport org.junit.Test;\nimport static org.junit.Assert.assertEquals;\n\npublic class HelloWorldTest {\n    HelloWorld hw = new HelloWorld();\n\n    @Test\n    public void testPrintMessage() {\n        assertEquals("HelloWorld", hw.getMsg());\n    }\n\n    @Test\n    public void failTest() {\n        assertEquals("HelloFAIL", hw.getMsg());\n    }\n}',
            fqcn: 'HelloWorldTest',
          },
        ],
      };
    }
    default:
      return state;
  }
};

export default editor;
