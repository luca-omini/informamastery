import {
  SET_CODE_SOL, SET_CODE_SOURCE, SET_CODE_TEST,
  SET_DESCR,
  SET_DURATION,
  SET_FQCN_SOL, SET_FQCN_SOURCE,
  SET_FQCN_TEST,
  SET_SHORT_DESCR, SET_STUDYTASK, SET_STUDYTASK_ID, SET_TIMEOUT,
  SET_TITLE,
} from '../actionTypes';

const initialState = {
  id: '',
  courseId: '',
  title: '',
  duration: '',
  body: '',
  description: '',
  timeout: 30,
  solution: {
    code: '',
    fqcn: '',
  },
  tests: {
    code: '',
    fqcn: '',
  },
  sources: {
    code: '',
    fqcn: '',
  },
};

const studyTaskEdit = (state = initialState, action) => {
  switch (action.type) {
    case SET_STUDYTASK: {
      return {
        ...state,
        id: action.payload.id,
        courseId: action.payload.courseId,
        title: action.payload.title,
        duration: action.payload.duration,
        body: action.payload.body,
        description: action.payload.description,
        solution: action.payload.solution,
        tests: action.payload.tests,
        sources: action.payload.sources,
      };
    }
    case SET_STUDYTASK_ID: {
      return {
        ...state,
        id: action.payload,
      };
    }
    case SET_TITLE: {
      return {
        ...state,
        title: action.payload,
      };
    }
    case SET_SHORT_DESCR: {
      return {
        ...state,
        description: action.payload,
      };
    }
    case SET_DURATION: {
      return {
        ...state,
        duration: action.payload,
      };
    }
    case SET_DESCR: {
      return {
        ...state,
        body: action.payload,
      };
    }
    case SET_TIMEOUT: {
      return {
        ...state,
        timeout: action.payload,
      };
    }
    case SET_FQCN_TEST: {
      return {
        ...state,
        tests: {
          ...state.tests,
          fqcn: action.payload,
        },
      };
    }
    case SET_FQCN_SOL: {
      return {
        ...state,
        solution: {
          ...state.solution,
          fqcn: action.payload,
        },
      };
    }
    case SET_FQCN_SOURCE: {
      return {
        ...state,
        sources: {
          ...state.sources,
          fqcn: action.payload,
        },
      };
    }
    case SET_CODE_TEST: {
      return {
        ...state,
        tests: {
          ...state.tests,
          code: action.payload,
        },
      };
    }
    case SET_CODE_SOL: {
      return {
        ...state,
        solution: {
          ...state.solution,
          code: action.payload,
        },
      };
    }
    case SET_CODE_SOURCE: {
      return {
        ...state,
        sources: {
          ...state.sources,
          code: action.payload,
        },
      };
    }
    default:
      return state;
  }
};

export default studyTaskEdit;
