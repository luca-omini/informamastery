import {
  REMOVE_OUTPUT,
  SET_CODE_SOL, SET_CODE_SOURCE, SET_CODE_TEST,
  SET_DESCR,
  SET_DURATION, SET_FQCN_SOL, SET_FQCN_SOURCE, SET_FQCN_TEST,
  SET_SHORT_DESCR, SET_STUDYTASK, SET_STUDYTASK_ID, SET_TIMEOUT,
  SET_TITLE,
  UPDATE_CONSOLE_OUTPUT,
} from './actionTypes';

export const setStudyTask = (payload) => ({
  type: SET_STUDYTASK,
  payload,
});

export const updateConsoleOutput = (payload) => ({
  type: UPDATE_CONSOLE_OUTPUT,
  payload,
});

export const setFormTitle = (payload) => ({
  type: SET_TITLE,
  payload,
});

export const setFormShortDesc = (payload) => ({
  type: SET_SHORT_DESCR,
  payload,
});

export const setFormDuration = (payload) => ({
  type: SET_DURATION,
  payload,
});

export const setFormDescr = (payload) => ({
  type: SET_DESCR,
  payload,
});

export const setCodeTest = (payload) => ({
  type: SET_CODE_TEST,
  payload,
});

export const setCodeSol = (payload) => ({
  type: SET_CODE_SOL,
  payload,
});

export const setCodeSource = (payload) => ({
  type: SET_CODE_SOURCE,
  payload,
});

export const setFqcnTest = (payload) => ({
  type: SET_FQCN_TEST,
  payload,
});

export const setFqcnSol = (payload) => ({
  type: SET_FQCN_SOL,
  payload,
});

export const setFqcnSource = (payload) => ({
  type: SET_FQCN_SOURCE,
  payload,
});

export const setStudyTaskId = (payload) => ({
  type: SET_STUDYTASK_ID,
  payload,
});

export const removeOutput = () => ({
  type: REMOVE_OUTPUT,
});

export const setTimeoutRunner = (payload) => ({
  type: SET_TIMEOUT,
  payload,
});
