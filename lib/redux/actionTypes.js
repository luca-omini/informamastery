export const SAVE_CODE = 'SAVE_CODE';
export const UPDATE_CONSOLE_OUTPUT = 'UPDATE_CONSOLE_OUTPUT';

// Edit action
export const SET_STUDYTASK = 'SET_STUDYTASK';
export const SET_TITLE = 'SET_TITLE';
export const SET_SHORT_DESCR = 'SET_SHORT_DESCR';
export const SET_DURATION = 'SET_DURATION';
export const SET_DESCR = 'SET_DESCR';
export const SET_CODE_TEST = 'SET_CODE_TEST';
export const SET_CODE_SOL = 'SET_CODE_SOL';
export const SET_CODE_SOURCE = 'SET_CODE_SOURCE';
export const SET_FQCN_TEST = 'SET_FQCN_TEST';
export const SET_FQCN_SOL = 'SET_FQCN_SOL';
export const SET_FQCN_SOURCE = 'SET_FQCN_SOURCE';
export const SET_STUDYTASK_ID = 'SET_STUDYTASK_ID';

export const REMOVE_OUTPUT = 'REMOVE_PUTPUT';

export const SET_TIMEOUT = 'SET_TIMEOUT';
