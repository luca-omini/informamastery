Meteor.methods({
	createPracticeProblem: function(topicId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create a practice problem.");
		}
		var topic = Topics.findOne({_id: topicId});
		if (!topic) {
			throw new Meteor.Error("topic-does-not-exist", "The specified topic does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, topic.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a practice problem.");
		}
		var choices = [];
		for (var i=0; i<5; i++) {
			choices.push({label: "ABCDE".charAt(i), text: "", isCorrect: false});
		}
		var now = new Date();
		return PracticeProblems.insert({
			courseId: topic.courseId,
			topicId: topic._id,
			skillIds: [],
			userId: this.userId,
			createdDate: now,
			kind: "MultipleChoice",
			title: "",
			question: "",
			choices: choices,
			explanation: "",
			published: false,
			archived: false,
		});
	},
	clonePracticeProblem: function(practiceProblemId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to clone a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to clone a practice problem.");
		}
		if (!practiceProblem.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can only clone your own practice problems.");
		}
		var now = new Date();
		return PracticeProblems.insert({
			courseId: practiceProblem.courseId,
			topicId: practiceProblem.topicId,
			skillIds: practiceProblem.skillIds,
			userId: this.userId,
			createdDate: now,
			kind: practiceProblem.kind,
			title: practiceProblem.title,
			question: practiceProblem.question,
			choices: practiceProblem.choices,
			explanation: practiceProblem.explanation,
			published: false,
			archived: false,
			clonedFromPracticeProblemId: practiceProblem._id
		});
	},
	updatePracticeProblem: function(practiceProblemId, skillIds, title, question, choices, explanation) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit a practice problem.");
		}
		if (!practiceProblem.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can only update your own practice problems.");
		}
		if (practiceProblem.published || practiceProblem.archived) {
			throw new Meteor.Error("no-permission", "You can only update unpublished and unarchived practice problems.");
		}
		check(skillIds, [String]);
		check(title, String);
		check(question, String);
		check(explanation, String);
		check(choices, [{label: String, text: String, isCorrect: Boolean}]);
		var now = new Date();
		return PracticeProblems.update({_id: practiceProblemId}, {$set: {
			skillIds: skillIds,
			title: title.trim(),
			question: question,
			choices: choices,
			explanation: explanation,
		}});
	},
	publishPracticeProblem: function(practiceProblemId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to publish a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to publish a practice problem.");
		}
		if (!practiceProblem.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can only publish your own practice problems.");
		}
		if (practiceProblem.published || practiceProblem.archived) {
			throw new Meteor.Error("no-permission", "You can only publish unpublished and unarchived practice problems.");
		}
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: practiceProblem.courseId, date: now, active: true, event: "IPublishedAPracticeProblem", practiceProblemId: practiceProblemId});
		return PracticeProblems.update({_id: practiceProblemId}, {$set: {
			published: true,
			publishedDate: now,
		}});
	},
	archivePracticeProblem: function(practiceProblemId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to archive a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to archive a practice problem.");
		}
		if (!practiceProblem.userId===this.userId && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You can only archive your own practice problems.");
		}
		if (practiceProblem.archived) {
			throw new Meteor.Error("no-permission", "You can only archive unarchived practice problems.");
		}
		var now = new Date();
		return PracticeProblems.update({_id: practiceProblemId}, {$set: {
			archived: true,
			archivedDate: now,
		}});
	},
	featurePracticeProblem: function(practiceProblemId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to feature a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to feature a practice problem.");
		}
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: practiceProblem.courseId, date: now, active: true, event: "IFeaturedAPracticeProblem", practiceProblemId: practiceProblemId});
		FeedEvents.insert({userId: practiceProblem.userId, courseId: practiceProblem.courseId, date: now, active: false, event: "SomeoneFeaturedMyPracticeProblem", practiceProblemId: practiceProblemId});
		return PracticeProblems.update({_id: practiceProblemId}, {$set: {
			featured: true,
			featuredDate: now,
		}});
	},
	unfeaturePracticeProblem: function(practiceProblemId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to unfeature a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to unfeature a practice problem.");
		}
		var now = new Date();
		return PracticeProblems.update({_id: practiceProblemId}, {$set: {
			featured: false,
			unfeaturedDate: now,
		}});
	},
	submitPracticeProblemSolution: function(practiceProblemId, selectedChoices, duration) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to submit a solution to a practice problem.");
		}
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemId});
		if (!practiceProblem) {
			throw new Meteor.Error("practice-problem-does-not-exist", "The specified practice problem does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit.");
		}
		if (!practiceProblem.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not submit solutions to your own practice problems.");
		}
		var now = new Date();
		var practiceProblemSolutionId = PracticeProblemSolutions.insert({
			courseId: practiceProblem.courseId,
			topicId: practiceProblem.topicId,
			userId: this.userId,
			createdDate: now,
			practiceProblemId: practiceProblem._id,
			selectedChoices: selectedChoices,
			duration: duration,
		});
		FeedEvents.insert({userId: this.userId, courseId: practiceProblem.courseId, date: now, active: true, event: "ISubmittedAPracticeProblemSolution", practiceProblemSolutionId: practiceProblemSolutionId});
		FeedEvents.insert({userId: practiceProblem.userId, courseId: practiceProblem.courseId, date: now, active: false, event: "SomeoneSubmittedASolutionToMyPracticeProblem", practiceProblemSolutionId: practiceProblemSolutionId});
		return practiceProblemSolutionId;
	},
	submitPracticeProblemFeedback: function(practiceProblemSolutionId, difficulty, quality, comment) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to submit feedback to a practice problem.");
		}
		var practiceProblemSolution = PracticeProblemSolutions.findOne({_id: practiceProblemSolutionId});
		if (!practiceProblemSolution) {
			throw new Meteor.Error("practice-problem-solution-does-not-exist", "The specified practice problem solution does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblemSolution.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit.");
		}
		if (!practiceProblemSolution.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can only associate feedback with your own practice problem solution.");
		}
		//check(difficulty, Match.Integer);
		//check(quality, Match.Integer);
		check(comment, String);
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: practiceProblemSolution.courseId, date: now, active: true, event: "ISubmittedPracticeProblemFeedback", practiceProblemSolutionId: practiceProblemSolutionId});
		var practiceProblem = PracticeProblems.findOne({_id: practiceProblemSolution.practiceProblemId});
		FeedEvents.insert({userId: practiceProblem.userId, courseId: practiceProblemSolution.courseId, date: now, active: false, event: "SomeoneSubmittedFeedbackToMyPracticeProblem", practiceProblemSolutionId: practiceProblemSolutionId});
		return PracticeProblemSolutions.update({_id: practiceProblemSolutionId}, {$set: {
			feedbackDate: now,
			difficulty: difficulty,
			quality: quality,
			comment: comment,
			commentAgreeingUserIds: [],
			commentDisagreeingUserIds: [],
		}});
	},
	agreeWithPracticeProblemComment: function(practiceProblemSolutionId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to agree with a comment on a practice problem.");
		}
		var practiceProblemSolution = PracticeProblemSolutions.findOne({_id: practiceProblemSolutionId});
		if (!practiceProblemSolution) {
			throw new Meteor.Error("practice-problem-solution-does-not-exist", "The specified practice problem solution does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblemSolution.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit.");
		}
		if (practiceProblemSolution.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not agree with your own comment.");
		}
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: practiceProblemSolution.courseId, date: now, active: true, event: "IAgreedWithAPracticeProblemComment", practiceProblemSolutionId: practiceProblemSolutionId});
		FeedEvents.insert({userId: practiceProblemSolution.userId, courseId: practiceProblemSolution.courseId, date: now, active: false, event: "SomeoneAgreedWithMyPracticeProblemComment", practiceProblemSolutionId: practiceProblemSolutionId});
		return PracticeProblemSolutions.update({_id: practiceProblemSolutionId}, {
			$addToSet: {
				commentAgreeingUserIds: this.userId,
			},
			$pullAll: {
				commentDisagreeingUserIds: [this.userId],
			}
		});
	},
	disagreeWithPracticeProblemComment: function(practiceProblemSolutionId) {
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to disagree with a comment on a practice problem.");
		}
		var practiceProblemSolution = PracticeProblemSolutions.findOne({_id: practiceProblemSolutionId});
		if (!practiceProblemSolution) {
			throw new Meteor.Error("practice-problem-solution-does-not-exist", "The specified practice problem solution does not exist.");
		}
		if (!isRegisteredAsAnything(this.userId, practiceProblemSolution.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to submit.");
		}
		if (practiceProblemSolution.userId===this.userId) {
			throw new Meteor.Error("no-permission", "You can not disagree with your own comment.");
		}
		var now = new Date();
		FeedEvents.insert({userId: this.userId, courseId: practiceProblemSolution.courseId, date: now, active: true, event: "IDisagreedWithAPracticeProblemComment", practiceProblemSolutionId: practiceProblemSolutionId});
		FeedEvents.insert({userId: practiceProblemSolution.userId, courseId: practiceProblemSolution.courseId, date: now, active: false, event: "SomeoneDisagreedWithMyPracticeProblemComment", practiceProblemSolutionId: practiceProblemSolutionId});
		return PracticeProblemSolutions.update({_id: practiceProblemSolutionId}, {
			$addToSet: {
				commentDisagreeingUserIds: this.userId,
			},
			$pullAll: {
				commentAgreeingUserIds: [this.userId],
			},
		});
	},
});
