Meteor.methods({
  createRubric: function(itemId) {
    var now = new Date();
		logMethodCall(now, "createRubric", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var item = ContentItems.findOne({_id: itemId});
		if (!item) {
			throw new Meteor.Error("item-does-not-exist", "The specified item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, item.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a rubric.");
		}
    return Rubrics.insert({
			courseId: item.courseId,
      itemId: itemId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
			log: [{what: "Created", when: now, who: this.userId}],
		});
  },
  createRubricCategory: function(rubricId) {
    var now = new Date();
		logMethodCall(now, "createRubricCategory", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var rubric = Rubrics.findOne({_id: rubricId});
		if (!rubric) {
			throw new Meteor.Error("rubric-does-not-exist", "The specified rubric does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, rubric.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a rubric category.");
		}
    return RubricCategories.insert({
			courseId: rubric.courseId,
      itemId: rubric.itemId,
      rubricId: rubricId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
			log: [{what: "Created", when: now, who: this.userId}],
      name: "",
      additive: true,
      maxPoints: 3,
      enableBonusMalus: false,
		});
  },
  createRubricItem: function(rubricCategoryId) {
    var now = new Date();
		logMethodCall(now, "createRubricItem", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var rubricCategory = RubricCategories.findOne({_id: rubricCategoryId});
		if (!rubricCategory) {
			throw new Meteor.Error("rubric-category-does-not-exist", "The specified rubric category does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, rubricCategory.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a rubric item.");
		}
    var index = RubricItems.find({rubricCategoryId: rubricCategoryId}).count()+1;
    return RubricItems.insert({
			courseId: rubricCategory.courseId,
      itemId: rubricCategory.itemId,
      rubricId: rubricCategory.rubricId,
      rubricCategoryId: rubricCategoryId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
			log: [{what: "Created", when: now, who: this.userId}],
      name: "Something",
      points: 1,
      index: index,
		});
  },
  updateRubricCategory: function(rubricCategoryId, name, maxPoints, additive, enableBonusMalus) {
    var now = new Date();
		logMethodCall(now, "updateRubricCategory", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var rubricCategory = RubricCategories.findOne({_id: rubricCategoryId});
		if (!rubricCategory) {
			throw new Meteor.Error("rubric-category-does-not-exist", "The specified rubric category does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, rubricCategory.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update a rubric category.");
		}
    return RubricCategories.update({
      _id: rubricCategoryId,
    }, {
      $set: {
        name: name,
        maxPoints: maxPoints,
        additive: additive,
        enableBonusMalus: enableBonusMalus,
      },
      $push: {
				log: {what: "Updated", when: now, who: this.userId, name: name, maxPoints: maxPoints, additive: additive, enableBonusMalus: enableBonusMalus},
			},
		});
  },
  removeRubricCategory: function(rubricCategoryId) {
    var now = new Date();
		logMethodCall(now, "removeRubricCategory", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var rubricCategory = RubricCategories.findOne({_id: rubricCategoryId});
		if (!rubricCategory) {
			throw new Meteor.Error("rubric-category-does-not-exist", "The specified rubric category does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, rubricCategory.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove a rubric category.");
		}
    return RubricCategories.update({
      _id: rubricCategoryId,
    }, {
      $set: {
        removed: true,
      },
      $push: {
				log: {what: "Removed", when: now, who: this.userId},
			},
		});
  },
  setRubricItemName: function(rubricItemId, name) {
    var now = new Date();
		logMethodCall(now, "setRubricItemName", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var rubricItem = RubricItems.findOne({_id: rubricItemId});
		if (!rubricItem) {
			throw new Meteor.Error("rubric-item-does-not-exist", "The specified rubric item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, rubricItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update a rubric item.");
		}
    return RubricItems.update({
      _id: rubricItemId,
    }, {
      $set: {
        name: name,
      },
      $push: {
				log: {what: "SetName", when: now, who: this.userId, name: name},
			},
		});
  },
  setRubricItemPoints: function(rubricItemId, points) {
    var now = new Date();
		logMethodCall(now, "setRubricItemPoints", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in.");
		}
		var rubricItem = RubricItems.findOne({_id: rubricItemId});
		if (!rubricItem) {
			throw new Meteor.Error("rubric-item-does-not-exist", "The specified rubric item does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, rubricItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update a rubric item.");
		}
    return RubricItems.update({
      _id: rubricItemId,
    }, {
      $set: {
        points: points,
      },
      $push: {
				log: {what: "SetPoints", when: now, who: this.userId, points: points},
			},
		});
  },
  removeRubricItem: function(rubricItemId) {
    var now = new Date();
    logMethodCall(now, "removeRubricItem", arguments);
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in.");
    }
    var rubricItem = RubricItems.findOne({_id: rubricItemId});
    if (!rubricItem) {
      throw new Meteor.Error("rubric-item-does-not-exist", "The specified rubric item does not exist.");
    }
    if (!isOnTeachingTeam(this.userId, rubricItem.courseId) && !isAdmin(this.userId)) {
      throw new Meteor.Error("no-permission", "You don't have permissions to remove a rubric item.");
    }
    return RubricItems.update({
      _id: rubricItemId,
    }, {
      $set: {
        removed: true,
      },
      $push: {
        log: {what: "Removed", when: now, who: this.userId, name: name},
      },
    });
  },
});
