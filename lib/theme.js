Meteor.methods({
	addTheme: function(courseId) {
		console.log("Meteor.methods.addTheme("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a course.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this course.");
		}
		var now = new Date();
		return Themes.insert({
			courseId: courseId,
			log: [{what: "Created", when: now, who: this.userId}],
		});
	},
	updateTheme: function(themeId, title, description, icon, color, why) {
		console.log("Meteor.methods.updateTheme("+themeId+", "+title+", "+description+", "+icon+", "+color+", "+why+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a theme.");
		}
		var theme = Themes.findOne({_id: themeId});
		if (!theme) {
			throw new Meteor.Error("theme-does-not-exist", "The specified theme does not exist.");
		}
		if (!isInstructor(this.userId, theme.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this theme.");
		}
		var now = new Date();
		return Themes.update({_id: themeId}, {
			$set: {
				title: title,
				description: description,
				icon: icon,
				color: color,
				why: why,
			},
			$push: {
				log: {what: "Updated", when: now, who: this.userId},
			},
		});
	},
	showTheme: function(themeId) {
		console.log("Meteor.methods.showTheme("+themeId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to show a theme.");
		}
		var theme = Themes.findOne({_id: themeId});
		if (!theme) {
			throw new Meteor.Error("theme-does-not-exist", "The specified theme does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, theme.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to show this theme.");
		}
		var now = new Date();
		return Themes.update({_id: themeId}, {
			$set: {
				status: "showing",
			},
			$push: {
				log: {what: "Shown", when: now, who: this.userId},
			},
		});
	},
	hideTheme: function(themeId) {
		console.log("Meteor.methods.hideTheme("+themeId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to hide a theme.");
		}
		var theme = Themes.findOne({_id: themeId});
		if (!theme) {
			throw new Meteor.Error("theme-does-not-exist", "The specified theme does not exist.");
		}
		if (!isOnTeachingTeam(this.userId, theme.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to hide this theme.");
		}
		var now = new Date();
		return Themes.update({_id: themeId}, {
			$set: {
				status: "hidden",
			},
			$push: {
				log: {what: "Hidden", when: now, who: this.userId},
			},
		});
	},
	removeTheme: function(themeId) {
		console.log("Meteor.methods.removeTheme("+themeId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to edit a course.");
		}
		var theme = Themes.findOne({_id: themeId});
		if (!theme) {
			throw new Meteor.Error("theme-does-not-exist", "The specified theme does not exist.");
		}
		if (!isInstructor(this.userId, theme.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to edit this course.");
		}
		var now = new Date();
		// don't delete, but mark as removed
		return Themes.update({_id: themeId}, {
			$set: {
				removed: true,
			},
			$push: {
				log: {what: "Removed", when: now, who: this.userId},
			},
		});
	},
});
