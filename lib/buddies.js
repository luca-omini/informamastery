Meteor.methods({
  addBuddyPreference: function(courseId, userId, buddyId) {
    var now = new Date();
		logMethodCall(now, "addBuddyPreference", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a buddy preference.");
		}
    var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
      if (this.userId!=userId) {
        throw new Meteor.Error("no-permission", "You don't have permissions to add a buddy for user "+userId);
      }
		}
    if (!isUserApprovedNotWithdrawnStudent(userId, courseId)) {
      throw new Meteor.Error("user-not-registered", "You cannot add a buddy for a user who is not registered, approved, and has not withdrawn from the course");
    }
    if (!isUserApprovedNotWithdrawnStudent(buddyId, courseId)) {
      throw new Meteor.Error("buddy-not-registered", "You cannot add a buddy who is not registered, approved, and has not withdrawn from the course");
    }
		return BuddyPreferences.update({courseId: courseId, userId: userId, buddyId: buddyId}, {
      $set: {
        courseId: courseId,
        userId: userId,
        buddyId: buddyId,
  			lastModificationDate: now,
  			active: true,
      },
      $push: {
        log: {what: "Added", when: now, who: this.userId},
      },
		}, {upsert: true});
  },
  removeBuddyPreference: function(courseId, userId, buddyId) {
    var now = new Date();
		logMethodCall(now, "removeBuddyPreference", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a buddy preference.");
		}
    var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, courseId) && !isAdmin(this.userId)) {
      if (this.userId!=userId) {
        throw new Meteor.Error("no-permission", "You don't have permissions to remove a buddy for user "+userId);
      }
		}
    var buddyPreference = BuddyPreferences.findOne({courseId: courseId, userId: userId, buddyId: buddyId});
    if (!buddyPreference) {
      throw new Meteor.Error("no-buddy-preference", "The buddy preference you specified does not exist");
    }
		return BuddyPreferences.update({courseId: courseId, userId: userId, buddyId: buddyId}, {
      $set: {
  			lastModificationDate: now,
  			active: false,
      },
      $push: {
        log: {what: "Removed", when: now, who: this.userId},
      },
		});
  },
});
