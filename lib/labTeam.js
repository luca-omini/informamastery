Meteor.methods({
  copyLabTeamsToLabTeams: function(priorLabId, labId) {
    console.log("Meteor.methods.copyLabTeamsToLabTeams("+priorLabId+", "+labId+")");
    if (!this.userId) {
      throw new Meteor.Error("logged-out", "You must be logged in to create lab teams.");
    }
    var lab = Labs.findOne({_id: labId});
    if (!lab) {
      throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
    }
    var priorLab = Labs.findOne({_id: priorLabId});
    if (!priorLab) {
      throw new Meteor.Error("lab-does-not-exist", "The specified prior lab does not exist.");
    }
    if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
      throw new Meteor.Error("no-permission", "You don't have permissions to create lab teams.");
    }
    var now = new Date();
    // determine unassignedStudentIds
    var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    var assignedStudentIds = [];
    LabTeams.find({labId: lab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      assignedStudentIds = assignedStudentIds.concat(labTeam.memberIds);
    });

    function createTeam(memberIds) {
      if (memberIds.length>0) {
        console.log("creating team with memberIds ", memberIds);
        var rightNow = new Date();
        var labTeamId = LabTeams.insert({
          courseId: lab.courseId,
          labId: labId,
          memberIds: memberIds,
          teamAssignmentComputationDate: now,
          creationDate: rightNow,
          creatorId: this.userId,
          creationApproach: "copy",
          removed: false,
          log: [{what: "CreatedThroughCopyFromPriorLab", when: now, who: this.userId}],
        });
        console.log("labTeamId:", labTeamId);
      }
    }

    LabTeams.find({labId: priorLab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      var unassignedMemberIds = _.difference(labTeam.memberIds, assignedStudentIds);
      createTeam(unassignedMemberIds);
    });
  },
  buddyAssignUnassignedStudentsToLabTeams: function(labId) {
    console.log("Meteor.methods.buddyAssignUnassignedStudentsToLabTeams("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create lab teams.");
		}
    var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create lab teams.");
		}
    var now = new Date();
    // determine unassignedStudentIds
    var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    var assignedStudentIds = [];
    LabTeams.find({labId: lab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      assignedStudentIds = assignedStudentIds.concat(labTeam.memberIds);
    });
    var unassignedStudentIds = _.difference(studentIds, assignedStudentIds);
    console.log("unassignedStudentIds:", unassignedStudentIds);
    var groupSize = Math.ceil(lab.groupSize);
    console.log("groupSize:", groupSize);
    var numberOfGroups = Math.ceil(studentIds.length/lab.groupSize);
    console.log("numberOfGroups:", numberOfGroups);

    // process all unassignedStudentIds, assigning them to (new) teams
    function createTeam(memberIds) {
      console.log("creating team with memberIds ", memberIds);
      var rightNow = new Date();
      var labTeamId = LabTeams.insert({
        courseId: lab.courseId,
        labId: labId,
        memberIds: memberIds,
        teamAssignmentComputationDate: now,
        creationDate: rightNow,
        creatorId: this.userId,
        creationApproach: "buddy",
        removed: false,
        log: [{what: "CreatedThroughBuddyAssignment", when: now, who: this.userId}],
      });
      console.log("labTeamId:", labTeamId);
    }

    // first, shuffle, so that those students who have NO quality ratings at all will be randomly permuted
    var shuffledUnassignedStudentIds = _.shuffle(unassignedStudentIds);
    console.log("shuffledUnassignedStudentIds:", shuffledUnassignedStudentIds);
    // now find the average rating (across published practice problems) for each student
    var unassignedStudentIdsAndRatings = _.map(shuffledUnassignedStudentIds, function(studentId) {
      var practiceProblemIds = PracticeProblems.find({userId: studentId}).map(function(practiceProblem) {return practiceProblem._id;});
      var qualityRating = _computeAverageQualityRatingAcrossProblems(practiceProblemIds);
      return {studentId: studentId, qualityRating: qualityRating};
    });
    console.log("unassignedStudentIdsAndRatings:", unassignedStudentIdsAndRatings);
    // sort by rating
    var sortedUnassignedStudentIdsAndRatings = _.sortBy(unassignedStudentIdsAndRatings, function(pair) {return -pair.qualityRating;});
    console.log("sortedUnassignedStudentIdsAndRatings:", sortedUnassignedStudentIdsAndRatings);
    // extract just the sorted list of studentIds
    var sortedUnassignedStudentIds = _.map(sortedUnassignedStudentIdsAndRatings, function(pair) {return pair.studentId});
    console.log("sortedUnassignedStudentIds:", sortedUnassignedStudentIds);

    var memberIds = [];
    while (sortedUnassignedStudentIds.length>0) {
      console.log("sortedUnassignedStudentIds:", sortedUnassignedStudentIds);
      var i = 0;
      var studentId = sortedUnassignedStudentIds[i];
      console.log("assigning student at index ", i, " with userId ", studentId);
      sortedUnassignedStudentIds.splice(i, 1);
      memberIds.push(studentId);

      var meLikingOthersIds = BuddyPreferences.find({courseId: lab.courseId, userId: studentId, buddyId: {$in: sortedUnassignedStudentIds}, active: true}).map(function(buddyPreference) {return buddyPreference.buddyId;});
      console.log("  meLikingOthersIds:", meLikingOthersIds);
      var othersLikingMeIds = BuddyPreferences.find({courseId: lab.courseId, buddyId: studentId, userId: {$in: sortedUnassignedStudentIds}, active: true}).map(function(buddyPreference) {return buddyPreference.userId;});
      console.log("  othersLikingMeIds:", othersLikingMeIds);
      var reciprocalIds = _.intersection(othersLikingMeIds, meLikingOthersIds);
      console.log("  reciprocalIds:", reciprocalIds);
      var disconnectedIds = _.difference(sortedUnassignedStudentIds, meLikingOthersIds, othersLikingMeIds);
      console.log("  disconnectedIds:", disconnectedIds);
      while (memberIds.length<groupSize && sortedUnassignedStudentIds.length>0) {
        var memberId = undefined;
        if (reciprocalIds.length>0) {
          memberId = reciprocalIds[0];
          console.log("    picking partner from reciprocalIds: "+memberId);
        } else if (meLikingOthersIds.length>0) {
          memberId = meLikingOthersIds[0];
          console.log("    picking partner from meLikingOthersIds: "+memberId);
        } else if (othersLikingMeIds.length>0) {
          memberId = othersLikingMeIds[0];
          console.log("    picking partner from othersLikingMeIds: "+memberId);
        } else {
          memberId = disconnectedIds[0];
          console.log("    picking partner from disconnectedIds: "+memberId);
        }
        memberIds.push(memberId);
        reciprocalIds = _.without(reciprocalIds, memberId);
        meLikingOthersIds = _.without(meLikingOthersIds, memberId);
        othersLikingMeIds = _.without(othersLikingMeIds, memberId);
        disconnectedIds = _.without(disconnectedIds, memberId);
        sortedUnassignedStudentIds = _.without(sortedUnassignedStudentIds, memberId);
      }
      createTeam(memberIds);
      memberIds = [];
    }
  },
  randomlyAssignUnassignedStudentsToLabTeams: function(labId) {
    console.log("Meteor.methods.randomlyAssignUnassignedStudentsToLabTeams("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create lab teams.");
		}
    var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create lab teams.");
		}
    var now = new Date();
    // determine unassignedStudentIds
    var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
    var assignedStudentIds = [];
    LabTeams.find({labId: lab._id, removed: {$ne: true}}).forEach(function(labTeam) {
      assignedStudentIds = assignedStudentIds.concat(labTeam.memberIds);
    });
    var unassignedStudentIds = _.difference(studentIds, assignedStudentIds);
    console.log("unassignedStudentIds:", unassignedStudentIds);
    var groupSize = Math.ceil(lab.groupSize);
    console.log("groupSize:", groupSize);
    var numberOfGroups = Math.ceil(studentIds.length/lab.groupSize);
    console.log("numberOfGroups:", numberOfGroups);

    // process all unassignedStudentIds, assigning them to (new) teams
    function createTeam(memberIds) {
      console.log("creating team with memberIds ", memberIds);
      var rightNow = new Date();
      var labTeamId = LabTeams.insert({
        courseId: lab.courseId,
        labId: labId,
        memberIds: memberIds,
        teamAssignmentComputationDate: now,
        creationDate: rightNow,
        creatorId: this.userId,
        creationApproach: "random",
        removed: false,
        log: [{what: "CreatedThroughRandomAssignment", when: now, who: this.userId}],
      });
      console.log("labTeamId:", labTeamId);
    }

    var memberIds = [];
    while (unassignedStudentIds.length>0) {
      var i = Math.floor(Math.random() * unassignedStudentIds.length);
      var studentId = unassignedStudentIds[i];
      console.log("assigning student at index ", i, " with userId ", studentId);
      unassignedStudentIds.splice(i, 1);
      memberIds.push(studentId);
      if (memberIds.length==groupSize) {
        createTeam(memberIds);
        memberIds = [];
      }
    }
    if (memberIds.length>0) {
      createTeam(memberIds);
    }
  },
  createRandomLabTeams: function(labId) {
    console.log("Meteor.methods.createRandomLabTeams("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create lab teams.");
		}
    var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create lab teams.");
		}
    var now = new Date();
    // first remove the existing teams for this lab
    LabTeams.remove({labId: labId}, function(error, count) {
      if (error) {
        throw new Meteor.Error("remove-failed", "Could not remove the existing lab teams");
      }
      // successfully removed the existing teams for this lab
      // only assign students that have been approved and have not withdrawn from the course
      var studentIds = StudentRegistrations.find({courseId: lab.courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
      console.log("studentIds:", studentIds);
      var groupSize = Math.ceil(lab.groupSize);
      console.log("groupSize:", groupSize);
      var numberOfGroups = Math.ceil(studentIds.length/lab.groupSize);
      console.log("numberOfGroups:", numberOfGroups);

      function createTeam(memberIds) {
        console.log("creating team with memberIds ", memberIds);
    		var labTeamId = LabTeams.insert({
    			courseId: lab.courseId,
          labId: labId,
          memberIds: memberIds,
    			creationDate: now,
    			creatorId: this.userId,
          creationApproach: "random",
    			removed: false,
    			log: [{what: "CreatedThroughRandomAssignment", when: now, who: this.userId}],
    		});
        console.log("labTeamId:", labTeamId);
      }

      var memberIds = [];
      while (studentIds.length>0) {
        var i = Math.floor(Math.random() * studentIds.length);
        var studentId = studentIds[i];
        console.log("assigning student at index ", i, " with userId ", studentId);
        studentIds.splice(i, 1);
        memberIds.push(studentId);
        if (memberIds.length==groupSize) {
          createTeam(memberIds);
          memberIds = [];
        }
      }
      if (memberIds.length>0) {
        createTeam(memberIds);
      }
    });
  },
	createLabTeam: function(labId) {
    console.log("Meteor.methods.createLabTeam("+labId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to create a lab team.");
		}
    var lab = Labs.findOne({_id: labId});
		if (!lab) {
			throw new Meteor.Error("lab-does-not-exist", "The specified lab does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, lab.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to create a lab team.");
		}
		var now = new Date();
		return LabTeams.insert({
			courseId: lab.courseId,
      labId: labId,
      memberIds: [],
			creationDate: now,
			creatorId: this.userId,
      creationApproach: "manual",
			removed: false,
			log: [{what: "Created", when: now, who: this.userId}],
		});
  },
  addLabTeamMember: function(labTeamId, userId) {
    console.log("Meteor.methods.addLabTeamMember("+labTeamId+", "+userId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a lab team member.");
		}
    var labTeam = LabTeams.findOne({_id: labTeamId});
		if (!labTeam) {
			throw new Meteor.Error("lab-team-does-not-exist", "The specified lab team does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, labTeam.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a member to this lab team.");
		}
		var now = new Date();
		return LabTeams.update({_id: labTeamId}, {
			$addToSet: {
        memberIds: userId,
      },
			$push: {
				log: {what: "AddedTeamMember", when: now, who: this.userId},
			},
    });
  },
  removeLabTeamMember: function(labTeamId, userId) {
    console.log("Meteor.methods.removeLabTeamMember("+labTeamId+", "+userId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a lab team member.");
		}
    var labTeam = LabTeams.findOne({_id: labTeamId});
		if (!labTeam) {
			throw new Meteor.Error("lab-team-does-not-exist", "The specified lab team does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, labTeam.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove a member from this lab team.");
		}
		var now = new Date();
		return LabTeams.update({_id: labTeamId}, {
			$pull: {
        memberIds: userId,
      },
			$push: {
				log: {what: "RemovedTeamMember", when: now, who: this.userId},
			},
    });
  },
  removeLabTeam: function(labTeamId) {
    console.log("Meteor.methods.removeLabTeam("+labTeamId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a lab team.");
		}
    var labTeam = LabTeams.findOne({_id: labTeamId});
		if (!labTeam) {
			throw new Meteor.Error("lab-team-does-not-exist", "The specified lab team does not exist.");
		}
    if (!isOnTeachingTeam(this.userId, labTeam.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this lab team.");
		}
		var now = new Date();
		return LabTeams.update({_id: labTeamId}, {
			$set: {
        removed: true,
      },
			$push: {
				log: {what: "Removed", when: now, who: this.userId},
			},
    });
  },
});
