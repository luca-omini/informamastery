Meteor.methods({
	addClassSession: function(courseId) {
		console.log("Meteor.methods.addClassSession("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a class session.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, course._id) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a class session.");
		}
		var now = new Date();
		return ClassSessions.insert({
			courseId: courseId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
		});
	},
	updateClassSession: function(classSessionId, title, description, date, duration, preparation, materials) {
    var now = new Date();
		logMethodCall(now, "updateClassSession", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this class session.");
		}
		return ClassSessions.update({_id: classSessionId}, {
			$set: {
				title: title,
				description: description,
        date: date,
        duration: duration,
        preparation: preparation,
        materials: materials,
			},
		});
	},
	addClassSessionTopic: function(classSessionId, topicId) {
		console.log("Meteor.methods.addClassSessionTopic("+classSessionId+", "+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this class session.");
		}
		var now = new Date();
		return ClassSessions.update({_id: classSessionId}, {
			$addToSet: {
				topicIds: topicId,
			},
		});
	},
	removeClassSessionTopic: function(classSessionId, topicId) {
		console.log("Meteor.methods.removeClassSessionTopic("+classSessionId+", "+topicId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this class session.");
		}
		var now = new Date();
		return ClassSessions.update({_id: classSessionId}, {
			$pullAll: {
				topicIds: [topicId],
			},
		});
	},
  addClassSessionStudyTask: function(classSessionId, studyTaskId) {
		console.log("Meteor.methods.addClassSessionStudyTask("+classSessionId+", "+studyTaskId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this class session.");
		}
		var now = new Date();
		return ClassSessions.update({_id: classSessionId}, {
			$addToSet: {
				studyTaskIds: studyTaskId,
			},
		});
	},
  removeClassSessionStudyTask: function(classSessionId, studyTaskId) {
		console.log("Meteor.methods.removeClassSessionStudyTask("+classSessionId+", "+studyTaskId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this class session.");
		}
		var now = new Date();
		return ClassSessions.update({_id: classSessionId}, {
			$pullAll: {
				studyTaskIds: [studyTaskId],
			},
		});
	},
	showClassSession: function(classSessionId) {
		console.log("Meteor.methods.showClassSession("+classSessionId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to show a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to show this class session.");
		}
		var now = new Date();
		ClassSessions.update({_id: classSessionId}, {
			$set: {
				status: "showing",
			},
			$push: {
				log: {what: "Shown", when: now, who: this.userId},
			},
		});
	},
	hideClassSession: function(classSessionId) {
		console.log("Meteor.methods.hideClassSession("+classSessionId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to hide a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to hide this class session.");
		}
		var now = new Date();
		ClassSessions.update({_id: classSessionId}, {
			$set: {
				status: "hidden",
			},
			$push: {
				log: {what: "Hidden", when: now, who: this.userId},
			},
		});
	},
	removeClassSession: function(classSessionId) {
		console.log("Meteor.methods.removeClassSession("+classSessionId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a class session.");
		}
		var classSession = ClassSessions.findOne({_id: classSessionId});
		if (!classSession) {
			throw new Meteor.Error("class-session-does-not-exist", "The specified class session does not exist.");
		}
		if (!isInstructor(this.userId, classSession.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this class session.");
		}
		var now = new Date();
		//TODO: if there is extra data in other tables for this classSession, "remove" that extra data
		ClassSessions.update({_id: classSessionId}, {
			$set: {
				removed: true,
			},
		});
	},
});
