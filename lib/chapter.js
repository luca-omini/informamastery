Meteor.methods({
	addChapter: function(bookId) {
		console.log("Meteor.methods.addChapter("+bookId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a chapter.");
		}
		var book = Books.findOne({_id: bookId});
		if (!book) {
			throw new Meteor.Error("book-does-not-exist", "The specified book does not exist.");
		}
		if (!isInstructor(this.userId, book.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a chapter.");
		}
		var now = new Date();
		return Chapters.insert({
			courseId: book.courseId,
			bookId: bookId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
		});
	},
	updateChapter: function(chapterId, title, number, sortKey) {
		console.log("Meteor.methods.updateChapter("+chapterId+", "+title+", "+number+", "+sortKey+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a chapter.");
		}
		var chapter = Chapters.findOne({_id: chapterId});
		if (!chapter) {
			throw new Meteor.Error("chapter-does-not-exist", "The specified chapter does not exist.");
		}
		if (!isInstructor(this.userId, chapter.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this chapter.");
		}
		var now = new Date();
		return Chapters.update({_id: chapterId}, {
			$set: {
				title: title,
				number: number,
				sortKey: sortKey,
			},
		});
	},
	removeChapter: function(chapterId) {
		console.log("Meteor.methods.removeChapter("+chapterId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a chapter.");
		}
		var chapter = Chapters.findOne({_id: chapterId});
		if (!chapter) {
			throw new Meteor.Error("chapter-does-not-exist", "The specified chapter does not exist.");
		}
		if (!isInstructor(this.userId, chapter.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this chapter.");
		}
		var now = new Date();
		Sections.update({chapterId: chapterId}, {
			$set: {
				removed: true,
			},
		});
		Chapters.update({_id: chapterId}, {
			$set: {
				removed: true,
			},
		});
	},
});
