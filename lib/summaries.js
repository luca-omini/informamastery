Meteor.methods({
	computeQuizScoreSummary: function(courseId) {
		var now = new Date();
		logMethodCall(now, "computeQuizScoreSummary", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to compute summaries.");
		}
    var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to compute summaries.");
		}

    if (!this.isSimulation) {
      console.log("computeQuizScoreSummary on the server: starting");
			var data = {};
			var quizzes = getPastQuizzes(courseId);
			//console.log("quizzes: ", quizzes);
			var studentIds = StudentRegistrations.find({courseId: courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
			var users = Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
			//console.log("users: ", users);
			quizzes.forEach(function(quiz) {
				var quizId = quiz._id;
				data[quizId] = {};
				var maxScore = computeMaxOverallQuizScore(quizId);
				data[quizId]["maxScore"] = maxScore;
				data[quizId]["userScores"] = {};
				users.forEach(function(user) {
					var userId = user._id;
					var quizAttempt = getQuizAttempt(quizId, userId);
					if (quizAttempt) {
						data[quizId]["userScores"][userId] = computeOverallQuizScore(quizAttempt);
					}
				});
			});
			var result = Summaries.insert({
				courseId: courseId,
				date: now,
				createdBy: this.userId,
				kind: "quizScoreSummary",
				data: data,
			});
			console.log("computeQuizScoreSummary on the server: done");
			return result;
		}
	},
	computeLabScoreSummary: function(courseId) {
		var now = new Date();
		logMethodCall(now, "computeLabScoreSummary", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to compute summaries.");
		}
    var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to compute summaries.");
		}

    if (!this.isSimulation) {
      console.log("computeLabScoreSummary on the server: starting");
			var data = {};
			var labs = getPastLabs(courseId);
			console.log("labs: ", labs);
			var studentIds = StudentRegistrations.find({courseId: courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
			var users = Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
			console.log("users: ", users);
			labs.forEach(function(lab) {
				var labId = lab._id;
				data[labId] = {};
				var maxScore = computeMaxOverallScore(labId);
				data[labId]["maxScore"] = maxScore;
				data[labId]["userScores"] = {};
				users.forEach(function(user) {
					var userId = user._id;
					var team = LabTeams.findOne({labId: labId, memberIds: userId});
					if (team) {
						var labTeamId = team._id;
						data[labId]["userScores"][userId] = computeOverallScore(labId, labTeamId);
					}
				});
			});
			var result = Summaries.insert({
				courseId: courseId,
				date: now,
				createdBy: this.userId,
				kind: "labScoreSummary",
				data: data,
			});
			console.log("computeLabScoreSummary on the server: done");
			return result;
    }
  },
});
