Meteor.methods({
	addContentItem: function(containerId, containerCollection, contentItemKind, position) {
		console.log("Meteor.methods.addContentItem("+containerId+", "+containerCollection+", "+contentItemKind+", "+position+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a content item.");
		}
		var container;
		if (containerCollection==="Labs") {
			container = Labs.findOne({_id: containerId});
		} else {
			throw new Meteor.Error("unsupported-container-collection", "Currently content items cannot be placed into that kind of container!");
		}
		if (!container) {
			throw new Meteor.Error("container-does-not-exist", "The specified container does not exist.");
		}
		if (!isInstructor(this.userId, container.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a content item to this container.");
		}
		var now = new Date();
		return ContentItems.insert({
			courseId: container.courseId,
			containerCollection: containerCollection,
			containerId: containerId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
			kind: contentItemKind,
			log: {what: "Created", when: now, who: this.userId},
			index: position,
			isNewlyCreated: true,
		});
	},
	updateContentItemPosition: function(contentItemId, newPosition) {
		console.log("Meteor.methods.updateContentItemPosition("+contentItemId+", "+newPosition+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a contentItem.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this content item.");
		}
		var now = new Date();
		return ContentItems.update({_id: contentItemId}, {
			$set: {
				index: newPosition,
			},
		});
	},
	updateContentItemTitle: function(contentItemId, title) {
		console.log("Meteor.methods.updateContentItemTitle("+contentItemId+", "+title+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a contentItem.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this content item.");
		}
		var now = new Date();
		return ContentItems.update({_id: contentItemId}, {
			$set: {
				title: title,
				isNewlyCreated: false,
			},
		});
	},
	updateContentItemText: function(contentItemId, text) {
		console.log("Meteor.methods.updateContentItemText("+contentItemId+", "+text+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a contentItem.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this content item.");
		}
		var now = new Date();
		return ContentItems.update({_id: contentItemId}, {
			$set: {
				text: text,
				isNewlyCreated: false,
			},
		});
	},
	updateContentItemLangAndText: function(contentItemId, lang, text) {
		console.log("Meteor.methods.updateContentItemLangAndText("+contentItemId+", "+lang+", "+text+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a contentItem.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this content item.");
		}
		var now = new Date();
		return ContentItems.update({_id: contentItemId}, {
			$set: {
				lang: lang,
				text: text,
				isNewlyCreated: false,
			},
		});
	},
	updateQuestionContentItem: function(contentItemId, type, text, contents) {
		console.log("Meteor.methods.updateContentItemTextAndContents("+contentItemId+", "+type+", "+text+", "+contents+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a contentItem.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this content item.");
		}
		var now = new Date();
		return ContentItems.update({_id: contentItemId}, {
			$set: {
				type: type,
				text: text,
				contents: contents,
				isNewlyCreated: false,
			},
		});
	},
	updateContentItemUrl: function(contentItemId, url) {
		console.log("Meteor.methods.updateContentItemUrl("+contentItemId+", "+url+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a contentItem.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this content item.");
		}
		var now = new Date();
		return ContentItems.update({_id: contentItemId}, {
			$set: {
				url: url,
				isNewlyCreated: false,
			},
		});
	},
	removeContentItem: function(contentItemId) {
		console.log("Meteor.methods.removeContentItem("+contentItemId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a content item.");
		}
		var contentItem = ContentItems.findOne({_id: contentItemId});
		if (!contentItem) {
			throw new Meteor.Error("content-item-does-not-exist", "The specified content item does not exist.");
		}
		if (!isInstructor(this.userId, contentItem.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this content item.");
		}
		var now = new Date();
		ContentItems.update({_id: contentItemId}, {
			$set: {
				removed: true,
			},
		});
	},
});
