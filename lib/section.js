Meteor.methods({
	addSection: function(chapterId) {
		console.log("Meteor.methods.addSection("+chapterId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a section.");
		}
		var chapter = Chapters.findOne({_id: chapterId});
		if (!chapter) {
			throw new Meteor.Error("chapter-does-not-exist", "The specified chapter does not exist.");
		}
		if (!isInstructor(this.userId, chapter.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a section.");
		}
		var now = new Date();
		return Sections.insert({
			courseId: chapter.courseId,
			bookId: chapter.bookId,
			chapterId: chapterId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
		});
	},
	updateSection: function(sectionId, title, number, sortKey, duration, contents, embedUrl, externalUrl) {
		console.log("Meteor.methods.updateSection("+sectionId+", "+title+", "+number+", "+sortKey+", "+duration+", "+contents+", "+embedUrl+", "+externalUrl+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a section.");
		}
		var section = Sections.findOne({_id: sectionId});
		if (!section) {
			throw new Meteor.Error("section-does-not-exist", "The specified section does not exist.");
		}
		if (!isInstructor(this.userId, section.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this section.");
		}
		var now = new Date();
		return Sections.update({_id: sectionId}, {
			$set: {
				title: title,
				number: number,
				sortKey: sortKey,
				duration: duration,
				contents: contents,
				embedUrl: embedUrl,
				externalUrl: externalUrl,
			},
		});
	},
	removeSection: function(sectionId) {
		console.log("Meteor.methods.removeSection("+sectionId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a section.");
		}
		var section = Sections.findOne({_id: sectionId});
		if (!section) {
			throw new Meteor.Error("section-does-not-exist", "The specified section does not exist.");
		}
		if (!isInstructor(this.userId, section.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this section.");
		}
		var now = new Date();
		return Sections.update({_id: sectionId}, {
			$set: {
				removed: true,
			},
		});
	},
});
