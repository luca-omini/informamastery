Meteor.methods({
  addMisconception: function(courseId) {
    var now = new Date();
    logMethodCall(now, "addMisconception", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a misconception.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, course._id) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a misconception.");
		}
		return Misconceptions.insert({
			courseId: courseId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
		});
	},
	updateMisconception: function(misconceptionId, title, description, correction) {
    var now = new Date();
    logMethodCall(now, "updateMisconception", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$set: {
				title: title,
				description: description,
        correction: correction,
			},
		});
	},
  removeMisconception: function(misconceptionId) {
    var now = new Date();
    logMethodCall(now, "removeMisconception", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$set: {
        removed: true,
        removedDate: now,
        removedBy: this.userId,
			},
		});
	},
  addMisconceptionTopic: function(misconceptionId, topicId) {
    var now = new Date();
    logMethodCall(now, "addMisconceptionTopic", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$addToSet: {
				topicIds: topicId,
			},
		});
	},
	removeMisconceptionTopic: function(misconceptionId, topicId) {
    var now = new Date();
    logMethodCall(now, "removeMisconceptionTopic", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$pullAll: {
				topicIds: [topicId],
			},
		});
	},
	addMisconceptionSkill: function(misconceptionId, skillId) {
    var now = new Date();
    logMethodCall(now, "addMisconceptionSkill", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$addToSet: {
				skillIds: skillId,
			},
		});
	},
	removeMisconceptionSkill: function(misconceptionId, skillId) {
    var now = new Date();
    logMethodCall(now, "removeMisconceptionSkill", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$pullAll: {
				skillIds: [skillId],
			},
		});
	},
  addMisconceptionStudyTask: function(misconceptionId, studyTaskId) {
    var now = new Date();
    logMethodCall(now, "addMisconceptionStudyTask", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$addToSet: {
				studyTaskIds: studyTaskId,
			},
		});
	},
	removeMisconceptionStudyTask: function(misconceptionId, studyTaskId) {
    var now = new Date();
    logMethodCall(now, "removeMisconceptionStudyTask", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$pullAll: {
				studyTaskIds: [studyTaskId],
			},
		});
	},
  addMisconceptionPracticeProblem: function(misconceptionId, practiceProblemId) {
    var now = new Date();
    logMethodCall(now, "addMisconceptionPracticeProblem", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$addToSet: {
        practiceProblemIds: practiceProblemId
      },
		});
	},
	removeMisconceptionPracticeProblem: function(misconceptionId, practiceProblemId) {
    var now = new Date();
    logMethodCall(now, "removeMisconceptionPracticeProblem", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$pullAll: {
				practiceProblemIds: [practiceProblemId],
			},
		});
	},
  addMisconceptionPracticeProblemChoice: function(misconceptionId, practiceProblemId, choiceLabel) {
    var now = new Date();
    logMethodCall(now, "addMisconceptionPracticeProblemChoice", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$addToSet: {
				practiceProblemChoices: {
          practiceProblemId: practiceProblemId,
          choiceLabel: choiceLabel
        },
			},
		});
	},
	removeMisconceptionPracticeProblemChoice: function(misconceptionId, practiceProblemId, choiceLabel) {
    var now = new Date();
    logMethodCall(now, "removeMisconceptionPracticeProblemChoice", arguments);
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a misconception.");
		}
		var misconception = Misconceptions.findOne({_id: misconceptionId});
		if (!misconception) {
			throw new Meteor.Error("misconception-does-not-exist", "The specified misconception does not exist.");
		}
		if (!isInstructor(this.userId, misconception.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this misconception.");
		}
		return Misconceptions.update({_id: misconceptionId}, {
			$pullAll: {
				practiceProblemChoices: [{
          practiceProblemId: practiceProblemId,
          choiceLabel: choiceLabel
        }],
			},
		});
	},
});
