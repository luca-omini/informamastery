Meteor.methods({
	addBook: function(courseId) {
		console.log("Meteor.methods.addBook("+courseId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to add a book.");
		}
		var course = Courses.findOne({_id: courseId});
		if (!course) {
			throw new Meteor.Error("course-does-not-exist", "The specified course does not exist.");
		}
		if (!isInstructor(this.userId, course._id) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to add a book.");
		}
		var now = new Date();
		return Books.insert({
			courseId: courseId,
			creationDate: now,
			creatorId: this.userId,
			removed: false,
		});
	},
	updateBook: function(bookId, abbreviation, title, subtitle, author, url, imageUrl, description) {
		console.log("Meteor.methods.updateBook("+bookId+", "+abbreviation+", "+title+", "+subtitle+", "+author+", "+url+", "+imageUrl+", "+description+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a book.");
		}
		var book = Books.findOne({_id: bookId});
		if (!book) {
			throw new Meteor.Error("book-does-not-exist", "The specified book does not exist.");
		}
		if (!isInstructor(this.userId, book.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this book.");
		}
		var now = new Date();
		return Books.update({_id: bookId}, {
			$set: {
				abbreviation: abbreviation,
				title: title,
				subtitle: subtitle,
				author: author,
				url: url,
				imageUrl: imageUrl,
				description: description,
			},
		});
	},
	removeBook: function(bookId) {
		console.log("Meteor.methods.removeBook("+bookId+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to remove a book.");
		}
		var book = Books.findOne({_id: bookId});
		if (!book) {
			throw new Meteor.Error("book-does-not-exist", "The specified book does not exist.");
		}
		if (!isInstructor(this.userId, book.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to remove this book.");
		}
		var now = new Date();
		Sections.update({bookId: bookId}, {
			$set: {
				removed: true,
			},
		});
		Chapters.update({bookId: bookId}, {
			$set: {
				removed: true,
			},
		});
		Books.update({_id: bookId}, {
			$set: {
				removed: true,
			},
		});
	},
	bulkCreateBookStructure: function(bookId, structure) {
		console.log("Meteor.methods.bulkCreateBookStructure("+bookId+", "+structure+")");
		if (!this.userId) {
			throw new Meteor.Error("logged-out", "You must be logged in to update a book.");
		}
		var book = Books.findOne({_id: bookId});
		if (!book) {
			throw new Meteor.Error("book-does-not-exist", "The specified book does not exist.");
		}
		if (!isInstructor(this.userId, book.courseId) && !isAdmin(this.userId)) {
			throw new Meteor.Error("no-permission", "You don't have permissions to update this book.");
		}
		var now = new Date();
		_.each(structure, function(chapter, index, list) {
			console.log("creating Chapter "+chapter.number+" "+chapter.title);
			var chapterId = Chapters.insert({
				courseId: book.courseId,
				bookId: bookId,
				creationDate: now,
				creatorId: this.userId,
				removed: false,
				title: chapter.title,
				number: chapter.number,
				// note that chapter.number might also be alphabetical (e.g., "A")
				// and this will only prefix numbers.
				sortKey: chapter.number<10?"0"+chapter.number:""+chapter.number,
			});
			_.each(chapter.sections, function(section, index, list) {
				console.log("creating Section "+chapter.number+"."+section.number+" "+section.title);
				var sectionId = Sections.insert({
					courseId: book.courseId,
					bookId: bookId,
					chapterId: chapterId,
					creationDate: now,
					creatorId: this.userId,
					removed: false,
					title: section.title,
					number: chapter.number+"."+section.number,
					// note that chapter.number might also be alphabetical (e.g., "A")
					// and this will only prefix numbers.
					sortKey: (chapter.number<10?"0"+chapter.number:""+chapter.number)+"."+(section.number<10?"0"+section.number:""+section.number),
				});
			});
		});
	},
});
