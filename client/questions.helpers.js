Template.courseQuestionsPage.helpers({
	getOwnAnnotationsForCourse: function(courseId) {
		return Annotations.find({courseId: courseId, userId: Meteor.userId(), removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		console.log("Template.courseQuestionsPage.helpers.containerIs("+itemId+", "+collectionName+")")
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
});
