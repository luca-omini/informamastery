var pickadateSubmitFormat = "mmmm d, yyyy";
var momentParseFormat = "MMMM D, YYYY";
var pickadateDisplayFormat = "dddd mmmm d, yyyy"


Template.themeListEditPage.rendered = function() {
	var computeDuration = function() {
		var start = $("[name='startDate_submit']").val();
		console.log("start: "+start);
		var end = $("[name='endDate_submit']").val();
		console.log("end: "+end);
		var startMoment = moment(start, momentParseFormat);
		console.log("startMoment: ", startMoment);
		var endMoment = moment(end, momentParseFormat);
		console.log("endMoment: ", endMoment);
		var days = endMoment.diff(startMoment, "days");
		var weeks = Math.floor(days/7);
		var remainingDays = days%7;
		var text = remainingDays?(weeks+" weeks and "+remainingDays+" days"):(weeks+" weeks");
		$(".duration").val(text);
	};
	$('.ui.checkbox').checkbox();
	$('.datepicker').pickadate({
		formatSubmit: pickadateSubmitFormat,
		format: pickadateDisplayFormat,
		onSet: function(thingSet) {
			//console.log('Set stuff:', thingSet);
			computeDuration();
		},
	});
	computeDuration();
};


Template.themeListEditPage.events({
	'submit .form': function(event, template) {
		event.preventDefault();
		var courseId = this.course._id;
		var abbreviation = event.target.abbreviation.value;
		var instance = event.target.instance.value;
		var title = event.target.title.value;
		var description = event.target.description.value;
		var maxTopicsPerMasteryCheck = event.target.maxTopicsPerMasteryCheck.value;
		var masteryChecksDetermineGrade = $(".masteryChecksDetermineGrade-checkbox").checkbox("is checked");
		var useMasteryChecks = $(".useMasteryChecks-checkbox").checkbox("is checked");
		var useQuizzes = $(".useQuizzes-checkbox").checkbox("is checked");
		var visible = $(".visible-checkbox").checkbox("is checked");
		//var visible = event.target.visible.value;
		var openForRegistration = $(".openForRegistration-checkbox").checkbox("is checked");
		//var openForRegistration = event.target.openForRegistration.value;
		var ended = $(".ended-checkbox").checkbox("is checked");
		var logoUrl = event.target.logoUrl.value;
		var startMoment = moment(event.target.startDate_submit.value, momentParseFormat);
		var startDate = startMoment.toDate();
		var endMoment = moment(event.target.endDate_submit.value, momentParseFormat);
		var endDate = endMoment.toDate();
		var overview = event.target.overview.value;
		Meteor.call('updateCourse', courseId, abbreviation, instance, title, description, useMasteryChecks, masteryChecksDetermineGrade, maxTopicsPerMasteryCheck, useQuizzes, visible, openForRegistration, ended, logoUrl, startDate, endDate, overview, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("themeListPage", {courseId: courseId});
			}
		});
		// Prevent default form submit
		//return false;
	},
	'click .add-theme-button': function(event, template) {
		var courseId = this.course._id;
		Meteor.call('addTheme', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("themeEditPage", {courseId: courseId, themeId: result});
			}
		});
	},
	'click .remove-theme-button': function(event, template) {
		var themeId = this._id;
		Meteor.call('removeTheme', themeId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
