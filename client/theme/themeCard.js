Template.themeCard.helpers({
	getTopicsMasteredCountForTheme: function(themeId) {
		//console.log("Template.themeCard.helpers.getTopicsMasteredCountForTheme("+themeId+")");
		var topicIdsForTheme = Topics.find({themeId: themeId}).map(function(topic) {
			return topic._id;
		});
		return TopicStatus.find({
			studentId: Meteor.userId(),
			topicId: {$in: topicIdsForTheme},
			mastered: true
		}).count();
	},
});
