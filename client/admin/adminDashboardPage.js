Template.adminDashboardPage.helpers({
	MeteorUsersCount: function() {
		return Meteor.users.find({}).count();
	},
  CoursesCount: function() {
		return Courses.find({}).count();
	},
  LabSubmissionFilesCount: function() {
    return LabSubmissionFiles.find().count();
  },
	release: function() {
		return Meteor.release;
	},
	publicSettings: function() {
		return JSON.stringify(Meteor.settings.public);
	},
	absoluteUrl: function() {
		return Meteor.absoluteUrl();
	},
  location: function() {
		return window.location.href;
	},
	protocol: function() {
		return window.location.protocol;
	},
	secure: function() {
		return window.location.protocol==="https:"?"true":"false";
	},
});
