Template.fileSizeCell.helpers({
  formattedLength: function() {
    return numeral(this.length).format('0.0b');
  },
});
