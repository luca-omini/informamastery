Template.adminLabSubmissionFilesPage.helpers({
  getLabSubmissionFiles: function() {
    return LabSubmissionFiles.find({});
  },
  getLabSubmissionFilesCount: function() {
    return LabSubmissionFiles.find({}).count();
  },
  getLabSubmissionFilesSize: function() {
    var sizes = LabSubmissionFiles.find({}).map(function(file) {return file.length;});
    var totalSize = _.reduce(sizes, function(memo, num){ return memo + num; }, 0);
    return numeral(totalSize).format('0.0b')+" ("+totalSize+" bytes)";
  },
  settings: {
    class: "ui compact table",
    filters: ['myFilter'],
    showRowCount: true,
    enableRegex: true,
    fields: [
      {
        key: "course",
        label: "Course",
        headerClass: "collapsing",
        tmpl: Template.fileCourseCell,
        fn: function(value, object) {
          var course = Courses.findOne({_id: object.metadata.courseId});
          return course.abbreviation+" "+course.instance;
        },
      },
      {
        key: "lab",
        label: "Lab",
        headerClass: "collapsing",
        tmpl: Template.fileLabCell,
        fn: function(value, object) {
          var lab = Labs.findOne({_id: object.metadata.labId});
          return lab.title;
        },
      },
      {
        key: "labItem",
        label: "Item",
        headerClass: "collapsing",
        tmpl: Template.fileLabItemCell,
        fn: function(value, object) {
          var labItem = ContentItems.findOne({_id: object.metadata.labItemId});
          return labItem.index;
        },
      },
      {
        key: "filename",
        label: "File Name",
        headerClass: "collapsing",
        tmpl: Template.fileNameCell,
        fn: function(value, object) {
          return object.filename;
        },
      },
      {
        key: "owner",
        label: "Owner",
        headerClass: "collapsing",
        tmpl: Template.fileOwnerCell,
      },
      {
        key: "uploadDate",
        label: "Uploaded",
        headerClass: "collapsing",
        sort: "descending",
        tmpl: Template.fileUploadedCell,
      },
      {
        key: "length",
        label: "Size",
        headerClass: "collapsing",
        tmpl: Template.fileSizeCell,
        fn: function(value, object) {
          return object.length;
        },
      },
    ],
  },
});
