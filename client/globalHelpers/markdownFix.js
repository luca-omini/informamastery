//import xss from 'xss';
const xss = require('xss');
//import moment from 'moment';
//const moment = require('moment');

// markdown throws exceptions (maybe only in combination with simple:highlight.js)
// when rendering markdown containing a code block with an unknown language
// such as ```blabla
// Because in Informa a user can enter markdown code,
// it actually happens that they enter this kind of illegal code,
// and that breaks the pages that render that code.
// Thus herewith we want to provide a helper that catches such exceptions
// and handles the problem gracefully.
Template.registerHelper("markdownFix", new Template('markdownFix', function () {
	//console.log("markdownFix");
	var view = this;
	var content = '';
	if (view.templateContentBlock) {
		content = Blaze._toText(view.templateContentBlock, HTML.TEXTMODE.STRING);
	}
	var converter = new Showdown.converter();
	//console.log("content:", content);
	var html;
	try {
		html = converter.makeHtml(content);
		console.log("Original HTML:", html);

		// This does not filter out enough
		var regexArr = Array.of(
			/(?:<style>((.|\n)*)<\/style>)+/gm,
			/(?:<script.*>((.|\n)*)<\/script>)+/gm,
			/(?:<button.*>((.|\n)*)<\/button>)+/gm
		);
		regexArr.forEach(exp => { html = html.replace(exp); });

		// This filters out too much (e.g., syntax highlighting from ```java)
		////html = xss(html);

		console.log("Filtered HTML:", html);
	} catch (error) {
		html = "<div class='ui error message'><div class='header'>Markdown Error</div>"+error+"</div>\n<pre>"+content+"</pre>";
	}
	return HTML.Raw(html);
}));
