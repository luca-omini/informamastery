Template.registerHelper("truncateId", function(id, length) {
  //console.log("truncateId("+id+", "+length+")");
  if (!length) {
    length = 6;
  }
  if (id) {
    if (typeof id === 'object') {
      id = ""+id.valueOf();
    }
    return id.substr(0, length);
  } else {
    return "";
  }
});

Template.registerHelper("truncate", function(text, length) {
  if (!length) {
    length = 20;
  }
  if (text) {
    text = ""+text.valueOf();
    return text.substr(0, length);
  } else {
    return "";
  }
});
