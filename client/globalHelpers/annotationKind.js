Template.registerHelper("iconForAnnotationKind", function(kind) {
	if (kind==="tag") {
		return "tag";
	} else if (kind==="note") {
		return "write";
	} else if (kind==="bookmark") {
		return "bookmark";
	} else if (kind==="question") {
		return "help";
	}
	return "";
});
