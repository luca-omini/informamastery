Template.registerHelper('equals', (a, b) => a == b);

Template.registerHelper('greaterThan', (a, b) => a > b);
