Template.syllabusEditPage.events({
	'submit .form': function (event) {
		var courseId = this._id;
		var syllabus = event.target.syllabus.value;
		Meteor.call('updateSyllabus', courseId, syllabus, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("syllabusPage", {courseId: courseId});
			}
		});
		// Prevent default form submit
		return false;
	},
});
