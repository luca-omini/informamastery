Template.classSessionListEditPage.helpers({
  getClassSessions: function(courseId) {
    return ClassSessions.find({courseId: courseId, removed: false});
  },
});


Template.classSessionListEditPage.events({
	'submit .form': function(event, template) {
		var courseId = this.course._id;
		var introduction = event.target.introduction.value;
		Meteor.call('updateClassSessionListIntroduction', courseId, introduction, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("classSessionListPage", {courseId: courseId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .add-class-session-button': function(ev, template) {
		var courseId = this.course._id;
		Meteor.call('addClassSession', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("classSessionEditPage", {courseId: courseId, classSessionId: result});
			}
		});
	},
	'click .remove-class-session-button': function(ev, template) {
		var classSessionId = this._id;
		Meteor.call('removeClassSession', classSessionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
