Template.classSessionStatusPanel.helpers({
});


Template.classSessionStatusPanel.events({
  'click .show-button': function(event, template) {
		console.log("click .show-button");
		var classSessionId = this.classSession._id;
		Meteor.call("showClassSession", classSessionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .hide-button': function(event, template) {
		console.log("click .hide-button");
		var classSessionId = this.classSession._id;
		Meteor.call("hideClassSession", classSessionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
