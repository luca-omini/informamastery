function _getTodaysClassSessions(courseId) {
  var start = moment().startOf('day').toDate();
  var end = moment().endOf('day').toDate();
  console.log("start: ", start);
  console.log("end: ", end);
  return ClassSessions.find({courseId: courseId, removed: false, date: {$gte: start, $lte: end}}, {sort: {date: 1}});
}

function _getPastClassSessions(courseId) {
  var start = moment().startOf('day').toDate();
  return ClassSessions.find({courseId: courseId, removed: false, date: {$lt: start}}, {sort: {date: 1}});
}

function _getFutureClassSessions(courseId) {
  var end = moment().endOf('day').toDate();
  return ClassSessions.find({courseId: courseId, removed: false, date: {$gt: end}}, {sort: {date: 1}});
}


Template.classSessionListPage.helpers({
  getClassSessions: function(courseId) {
    return ClassSessions.find({courseId: courseId, removed: false});
  },
  getTodaysClassSessions: function(courseId) {
    return _getTodaysClassSessions(courseId);
  },
  countTodaysClassSessions: function(courseId) {
    return _getTodaysClassSessions(courseId).count();
  },
  getPastClassSessions: function(courseId) {
    return _getPastClassSessions(courseId);
  },
  countPastClassSessions: function(courseId) {
    return _getPastClassSessions(courseId).count();
  },
  getFutureClassSessions: function(courseId) {
    return _getFutureClassSessions(courseId);
  },
  countFutureClassSessions: function(courseId) {
    return _getFutureClassSessions(courseId).count();
  },
});
