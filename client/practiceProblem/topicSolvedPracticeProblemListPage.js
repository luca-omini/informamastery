_findPracticeProblemSolutions = function(topicId) {
	var pss = PracticeProblemSolutions.find({topicId: topicId, userId: Meteor.userId()});
	return pss;
},


Template.topicSolvedPracticeProblemListPage.helpers({
	findPracticeProblemSolutions: function(topicId) {
		return _findPracticeProblemSolutions(topicId);
	},
	countPracticeProblemSolutions: function(topicId) {
		return _findPracticeProblemSolutions(topicId).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "problemPublishedDate",
					label: "Published",
					tmpl: Template.practiceProblemSolutionProblemPublishedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.practiceProblemSolutionClickablePracticeProblemTitle,
				},
				{
					key: "createdDate",
					label: "Solved",
					sort: "descending",
					tmpl: Template.practiceProblemSolutionCreatedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
			],
		};
	},
});
