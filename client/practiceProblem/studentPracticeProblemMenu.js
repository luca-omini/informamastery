Template.studentPracticeProblemMenu.helpers({
	countStudentUnsolvedPracticeProblems: function(studentId) {
		return _findStudentUnsolvedPracticeProblems(studentId).count();
	},
	countStudentSolvedPracticeProblems: function(studentId) {
		return PracticeProblemSolutions.find({userId: studentId}).count();
	},
	countStudentOwnPublishedUnarchivedPracticeProblems: function(studentId) {
		return PracticeProblems.find({userId: studentId, published: true, archived: false}).count();
	},
});
