_findUnsolvedPracticeProblems = function(topicId) {
	var solvedPracticeProblemIds = PracticeProblemSolutions.find({topicId: topicId, userId: Meteor.userId()}).map(function(practiceProblemSolution) {return practiceProblemSolution.practiceProblemId;});
	var ps = PracticeProblems.find({topicId: topicId, userId: {$ne: Meteor.userId()}, _id: {$nin: solvedPracticeProblemIds}, published:true, archived:false});
	return ps;
}

Template.topicUnsolvedPracticeProblemListPage.helpers({
	findUnsolvedPracticeProblems: function(topicId) {
		return _findUnsolvedPracticeProblems(topicId);
	},
	countUnsolvedPracticeProblems: function(topicId) {
		return _findUnsolvedPracticeProblems(topicId).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "publishedDate",
					label: "Published",
					sort: "descending",
					tmpl: Template.practiceProblemPublishedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.clickablePracticeProblemTitle,
				},
				{
					key: "solutions",
					label: "Solutions",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _countPracticeProblemSolutions(object._id)},
					tmpl: Template.practiceProblemSolutionCount,
				},
				{
					key: "difficulty",
					label: "Difficulty",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function (value, object) {return _computeAverageDifficultyRating(object._id)},
					tmpl: Template.practiceProblemDifficultyRating,
				},
				{
					key: "quality",
					label: "Quality",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function (value, object) {return _computeAverageQualityRating(object._id)},
					tmpl: Template.practiceProblemQualityRating,
				},
			],
		};
	},
});
