Template.practiceProblemQualityRating.helpers({
	getQualityRating: function(practiceProblemId) {
		var average = _computeAverageQualityRating(practiceProblemId);
		return _convertQualityNumberToText(average);
	},
});
