_countPracticeProblemComments = function(practiceProblemId) {
	return PracticeProblemSolutions.find({practiceProblemId: practiceProblemId, comment: {$exists: true, $ne: ""}}).count();
};

Template.practiceProblemCommentCount.helpers({
	countPracticeProblemComments: function(practiceProblemId) {
		return _countPracticeProblemComments(practiceProblemId);
	},
});
