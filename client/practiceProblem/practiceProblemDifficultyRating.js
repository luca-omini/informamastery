_computeAverageDifficultyRating = function(practiceProblemId) {
	// easy, medium, hard
	var count = 0;
	var sum = 0;
	PracticeProblemSolutions.find({practiceProblemId: practiceProblemId}).forEach(function(practiceProblemSolution) {
		if (practiceProblemSolution.difficulty) {
			count++;
			sum+=Math.floor(practiceProblemSolution.difficulty);
		}
	});
	if (count>0) {
		var average = sum/count;
		return average;
	} else {
		return -1;
	}
};


_computeAverageDifficultyRatingAcrossProblems = function(practiceProblemIds) {
	var count = 0;
	var sum = 0;
	PracticeProblemSolutions.find({practiceProblemId: {$in: practiceProblemIds}}).forEach(function(practiceProblemSolution) {
		if (practiceProblemSolution.difficulty) {
			count++;
			sum+=Math.floor(practiceProblemSolution.difficulty);
		}
	});
	if (count>0) {
		var average = sum/count;
		return average;
	} else {
		return -1;
	}
};


_convertDifficultyNumberToText = function(difficulty) {
	if (difficulty>=0) {
		var roundedDifficulty = Math.round(difficulty);
		var labels = ["easy", "medium", "hard"];
		if (roundedDifficulty>=0 && roundedDifficulty<=2) {
			return labels[roundedDifficulty];
		} else {
			// should not happen
			return roundedDifficulty;
		}
	}
	return "";
};


Template.practiceProblemDifficultyRating.helpers({
	getDifficultyRating: function(practiceProblemId) {
		var average = _computeAverageDifficultyRating(practiceProblemId);
		return _convertDifficultyNumberToText(average);
	},
});
