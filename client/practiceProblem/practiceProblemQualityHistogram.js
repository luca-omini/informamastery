_computeQualityHistogram = function(practiceProblemId) {
	console.log("_computeQualityHistogram("+practiceProblemId+")");
	var histogram = [
		{label: "very poor", count: 0, percentage: 0},
		{label: "poor", count: 0, percentage: 0},
		{label: "fair", count: 0, percentage: 0},
		{label: "good", count: 0, percentage: 0},
		{label: "very good", count: 0, percentage: 0},
		{label: "excellent", count: 0, percentage: 0},
	];
	var count = 0;
	PracticeProblemSolutions.find({practiceProblemId: practiceProblemId}).forEach(function(practiceProblemSolution) {
		if (practiceProblemSolution.quality) {
			console.log("practiceProblemSolution.quality="+practiceProblemSolution.quality);
			histogram[practiceProblemSolution.quality].count++;
			count++;
		}
	});
	if (count) {
		histogram = _.map(histogram, function(element) {
			var percentage = Math.floor(100*element.count/count);
			console.log("histogram: "+element.label+" "+element.count+" "+percentage);
			return {
				label: element.label,
				count: element.count,
				percentage: percentage,
			};
		});
	}
	return histogram.reverse();
};


Template.practiceProblemQualityHistogram.helpers({
	getQualityHistogram: function(practiceProblemId) {
		return _computeQualityHistogram(practiceProblemId);
	},
});
