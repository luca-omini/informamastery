function _findUnarchivedTeachingTeamPracticeProblems(courseId) {
	//TODO only problems by the instructor and TAs
	var assistantIds = AssistantRegistrations.find({courseId: courseId}).map(function(assistantRegistration) {return assistantRegistration.assistantId;});
	console.log("assistantIds: ", assistantIds);
	var instructorIds = InstructorRegistrations.find({courseId: courseId}).map(function(instructorRegistration) {return instructorRegistration.instructorId;});
	console.log("instructorIds: ", instructorIds);
	var ttIds = _.flatten([assistantIds, instructorIds]);
	console.log("ttIds: ", ttIds);
	return PracticeProblems.find({courseId: courseId, archived:false, userId: {$in: ttIds}});
}

Template.ttPracticeProblemsPage.helpers({
	findUnarchivedTeachingTeamPracticeProblems: function(courseId) {
		return _findUnarchivedTeachingTeamPracticeProblems(courseId);
	},
	countUnarchivedTeachingTeamPracticeProblems: function(courseId) {
		return _findUnarchivedTeachingTeamPracticeProblems(courseId).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "publishedDate",
					label: "Published",
					sort: "descending",
					tmpl: Template.practiceProblemPublishedDateIfPublished,
					headerClass: "collapsing",
					cellClass: "left aligned",
					fn: function(value, object) {return object.publishedDate?object.publishedDate:new Date(0)},
				},
				{
					key: "userId",
					label: "Author",
					tmpl: Template.practiceProblemAuthorCell,
				},
				{
					key: "topic",
					label: "Topic",
					cellClass: "left aligned",
					fn: function(value, object) {
						if (object) {
							var topic = Topics.findOne({_id: object.topicId});
							return topic?topic.title:"";
						} else {
							return "";
						}
					},
					tmpl: Template.practiceProblemTopic,
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.clickablePracticeProblemTitle,
				},
				{
					key: "solutions",
					label: "Solutions",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _countPracticeProblemSolutions(object._id)},
					tmpl: Template.practiceProblemSolutionCount,
				},
				{
					key: "difficulty",
					label: "Difficulty",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function (value, object) {return _computeAverageDifficultyRating(object._id)},
					tmpl: Template.practiceProblemDifficultyRating,
				},
				{
					key: "quality",
					label: "Quality",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function (value, object) {return _computeAverageQualityRating(object._id)},
					tmpl: Template.practiceProblemQualityRating,
				},
			],
		};
	},
});
