Template.topicPracticeProblemMenu.helpers({
	countUnsolvedPracticeProblems: function(topicId) {
		return _findUnsolvedPracticeProblems(topicId).count();
	},
	countSolvedPracticeProblems: function(topicId) {
		return PracticeProblemSolutions.find({topicId: topicId, userId: Meteor.userId()}).count();
	},
	countOwnPublishedUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), published: true, archived: false}).count();
	},
	countOwnUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), archived: false}).count();
	},
});
