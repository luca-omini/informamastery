_countPracticeProblemSolutions = function(practiceProblemId) {
	return PracticeProblemSolutions.find({practiceProblemId: practiceProblemId}).count();
};

Template.practiceProblemSolutionCount.helpers({
	countPracticeProblemSolutions: function(practiceProblemId) {
		return _countPracticeProblemSolutions(practiceProblemId);
	},
});
