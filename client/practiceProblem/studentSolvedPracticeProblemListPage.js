_findStudentPracticeProblemSolutions = function(userId) {
	return PracticeProblemSolutions.find({userId: userId});
},


Template.studentSolvedPracticeProblemListPage.helpers({
	findStudentPracticeProblemSolutions: function(userId) {
		return _findStudentPracticeProblemSolutions(userId);
	},
	countStudentPracticeProblemSolutions: function(userId) {
		return _findStudentPracticeProblemSolutions(userId).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "problemPublishedDate",
					label: "Published",
					tmpl: Template.practiceProblemSolutionProblemPublishedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "topic",
					label: "Topic",
					cellClass: "left aligned",
					fn: function(value, object) {
						if (object) {
							var topic = Topics.findOne({_id: object.topicId});
							return topic?topic.title:"";
						} else {
							return "";
						}
					},
					tmpl: Template.practiceProblemTopic,
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.practiceProblemSolutionClickablePracticeProblemTitle,
				},
				{
					key: "createdDate",
					label: "Solved",
					sort: "descending",
					tmpl: Template.practiceProblemSolutionCreatedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
			],
		};
	},
});
