Template.practiceProblemArchived.events({
	'click .archive-practice-problem-button': function (event) {
		var practiceProblemId = this._id;
		Meteor.call('archivePracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
