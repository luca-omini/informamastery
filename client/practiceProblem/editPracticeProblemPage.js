Template.editPracticeProblemPage.helpers({
	activeIfTrue: function(v) {
		return v?"active":"";
	},
	containsElement: function(array, element) {
		console.log("containsElement("+array+", "+element+")");
		console.log(array);
		return _.contains(array, element);
	},
});


Template.editPracticeProblemPage.events({
	'click .skill-toggle-button': function (event) {
		console.log(event);
		console.log(event.target);
		console.log(event.currentTarget);
		if ($(event.currentTarget).hasClass("active")) {
			$(event.currentTarget).removeClass("active");
			$(event.currentTarget).removeClass("green");
			//$(event.target).html("<i class='minus icon'></i> Wrong");
		} else {
			$(event.currentTarget).addClass("active");
			$(event.currentTarget).addClass("green");
			//$(event.target).html("<i class='checkmark icon'></i> Correct");
		}
	},
	'click .choice-toggle-button': function (event) {
		console.log(event);
		console.log(event.currentTarget);
		if ($(event.currentTarget).hasClass("active")) {
			$(event.currentTarget).removeClass("active");
			$(event.currentTarget).html("<i class='minus icon'></i> Wrong");
		} else {
			$(event.currentTarget).addClass("active");
			$(event.currentTarget).html("<i class='checkmark icon'></i> Correct");
		}
	},
	'submit .form': function (event) {
		var courseId = this.course._id;
		var topicId = this.topic._id;
		var practiceProblemId = this.practiceProblem._id;
		var title = event.target.title.value;
		var question = event.target.question.value;

		console.log("finding DOM nodes with class choice-container...");
		var choices = $(event.target).find(".choice-container").map(function(index, element) {
			console.log("found a DOM node with class choice-container");
			console.log(element);
			return {
				label: $(element).find(".choice-toggle-button").attr("name"),
				text: $(element).find(".choice-text").val(),
				isCorrect: $(element).find(".choice-toggle-button").hasClass("active"),
			};
		}).get();
		console.log(choices);

		console.log("finding DOM nodes with classes skill-toggle-button and active");
		var skillIds = [];
		var skills = $(".skill-toggle-button.active").each(function(index, element) {
			console.log("found a DOM node");
			console.log(element);
			var skillId = $(element).attr("name");
			skillIds.push(skillId);
		});
		console.log(skillIds);

		var explanation = event.target.explanation.value;
		Meteor.call('updatePracticeProblem', practiceProblemId, skillIds, title, question, choices, explanation, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("viewPracticeProblemPage", {courseId: courseId, topicId: topicId, practiceProblemId: practiceProblemId});
			}
		});
		// Prevent default form submit
		return false;
	},
});
