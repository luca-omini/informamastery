Template.topicOwnPracticeProblemListPage.helpers({
	countOwnArchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, archived: true, userId: Meteor.userId()}).count();
	},
	findOwnArchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, archived: true, userId: Meteor.userId()});
	},
	countOwnUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, archived: false, userId: Meteor.userId()}).count();
	},
	findOwnUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, archived: false, userId: Meteor.userId()});
	},
	countOwnPublishedUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, published: true, archived: false, userId: Meteor.userId()}).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "createdDate",
					label: "Created",
					sort: "descending",
					tmpl: Template.practiceProblemCreatedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.clickablePracticeProblemTitle,
				},
				{
					key: "solutions",
					label: "Solutions",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _countPracticeProblemSolutions(object._id)},
					tmpl: Template.practiceProblemSolutionCount,
				},
				{
					key: "difficulty",
					label: "Difficulty",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _computeAverageDifficultyRating(object._id)},
					tmpl: Template.practiceProblemDifficultyRating,
				},
				{
					key: "quality",
					label: "Quality",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _computeAverageQualityRating(object._id)},
					tmpl: Template.practiceProblemQualityRating,
				},
				{
					key: "EDIT",
					label: "Edit",
					sortable: false,
					tmpl: Template.practiceProblemEditButton,
					headerClass: "collapsing",
					cellClass: "center algined",
				},
				{
					key: "published",
					label: "P",
					tmpl: Template.practiceProblemPublished,
					headerClass: "center aligned collapsing",
					cellClass: "center aligned",
				},
				{
					key: "archived",
					label: "A",
					tmpl: Template.practiceProblemArchived,
					headerClass: "center aligned collapsing",
					cellClass: "center aligned",
				},
			],
		};
	},
});


Template.topicOwnPracticeProblemListPage.events({
	'click .create-practice-problem-button': function(ev, template) {
		var topicId = this.topic._id;
		console.log("calling createPracticeProblem("+topicId+")");
		Meteor.call('createPracticeProblem', topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("success. result="+result);
				var practiceProblem = PracticeProblems.findOne({_id: result});
				Router.go("editPracticeProblemPage", {courseId: practiceProblem.courseId, topicId: practiceProblem.topicId, practiceProblemId: practiceProblem._id});
			}
		});
	},
});
