Template.practiceProblemPublished.events({
	'click .publish-practice-problem-button': function (event) {
		var practiceProblemId = this._id;
		Meteor.call('publishPracticeProblem', practiceProblemId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
		// Prevent default form submit
		return false;
	},
});
