Template.studentReadingPage.helpers({
	getBooks: function(courseId) {
		return Books.find({courseId: courseId, removed: {$ne: true}});
	},
  getChapters: function(bookId) {
    return Chapters.find({bookId: bookId, removed: {$ne: true}}, {sort: {sortKey: 1}});
	},
  getSections: function(chapterId) {
    return Sections.find({chapterId: chapterId, removed: {$ne: true}}, {sort: {sortKey: 1}});
	},
  countBookSections: function(bookId) {
		//console.log("countBookSections("+bookId+")");
		return Sections.find({bookId: bookId}).count();
	},
  countChapterSections: function(chapterId) {
    //console.log("countChapterSections("+chapterId+")");
    return Sections.find({chapterId: chapterId}).count();
  },
  hasStudentStatus: function(sectionId, userId) {
		//console.log("hasStudentStatus("+sectionId+", "+userId+")");
		var result = SectionStatus.find({sectionId: sectionId, userId: userId}).count()>0;
    //console.log(result);
    return result;
	},
	getStudentStatus: function(sectionId, userId) {
		//console.log("getStudentStatus("+sectionId+", "+userId+")");
		var result = SectionStatus.findOne({sectionId: sectionId, userId: userId});
    //console.log(result);
    return result;
	},
  countStudentChapterSectionStatuses: function(chapterId, userId) {
		//console.log("countStudentChapterSectionStatuses("+chapterId+", "+userId+")");
		return SectionStatus.find({chapterId: chapterId, userId: userId}).count();
	},
  countStudentBookSectionStatuses: function(bookId, userId) {
    //console.log("countStudentBookSectionStatuses("+bookId+", "+userId+")");
    return SectionStatus.find({bookId: bookId, userId: userId}).count();
  },
  countStudentCourseSectionStatuses: function(courseId, userId) {
    //console.log("countStudentCourseSectionStatuses("+courseId+", "+userId+")");
    return SectionStatus.find({courseId: courseId, userId: userId}).count();
  },
});
