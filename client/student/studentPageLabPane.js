function _getOngoingLabs(courseId) {
	var now = new Date();
  return Labs.find({courseId: courseId, removed: false, deadlineDate: {$gt: now}, status: "open"}, {sort: {deadlineDate: 1}});
}

function _getNextLab(courseId) {
	var now = new Date();
  return Labs.findOne({courseId: courseId, removed: false, deadlineDate: {$gt: now}, status: "preview"}, {sort: {deadlineDate: 1}, count: 1});
}

Template.studentPageLabPane.helpers({
	countOngoingLabs: function(courseId) {
		return _getOngoingLabs(courseId).count();
	},
  getOngoingLabs: function(courseId) {
    return _getOngoingLabs(courseId);
	},
  getNextLab: function(courseId) {
		return _getNextLab(courseId);
	},
});
