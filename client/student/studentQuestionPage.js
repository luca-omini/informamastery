Template.studentQuestionPage.helpers({
	getUserAnnotationsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
});
