Template.studentGradePage.helpers({
	countTopicsInCourse: function(courseId) {
		return Topics.find({courseId: courseId}).count();
	},
	countTopicsMastered: function(courseId, studentId) {
		return TopicStatus.find({courseId: courseId, studentId: studentId, mastered: true}).count();
	},
	countTopicsNotYetMastered: function(courseId, studentId) {
		return Topics.find({courseId: courseId}).count() - TopicStatus.find({courseId: courseId, studentId: studentId, mastered: true}).count();
	},
	computeWorstCaseGrade: function(courseId, studentId) {
		var courseTopicIds = Topics.find({courseId: courseId}).map(function(topic) {return topic._id;});
		var masteredTopicIds = TopicStatus.find({courseId: courseId, studentId: studentId, mastered: true}).map(function(topicStatus) {return topicStatus.topicId;});
		var notYetMasteredTopicIds = _.difference(courseTopicIds, masteredTopicIds);
		console.log(notYetMasteredTopicIds);
		if (notYetMasteredTopicIds.length>0) {
			var topicsWithMinimumNotYetAchievedGrade = Topics.find({_id: {$in: notYetMasteredTopicIds}}, {sort: {masteryRequiredForGrade: 1}, limit: 1});
			console.log(topicsWithMinimumNotYetAchievedGrade);
			return topicsWithMinimumNotYetAchievedGrade.fetch()[0].masteryRequiredForGrade-1;
		} else {
			return 10; //TODO: course needs to have some settings defining the grading system (incl. max grade).
		}
	},
	getTopicsToMasterForNextGradeLevel: function(courseId, studentId) {
		var courseTopicIds = Topics.find({courseId: courseId}).map(function(topic) {return topic._id;});
		var masteredTopicIds = TopicStatus.find({courseId: courseId, studentId: studentId, mastered: true}).map(function(topicStatus) {return topicStatus.topicId;});
		var notYetMasteredTopicIds = _.difference(courseTopicIds, masteredTopicIds);
		console.log(notYetMasteredTopicIds);
		if (notYetMasteredTopicIds.length>0) {
			var topicsWithMinimumNotYetAchievedGrade = Topics.find({_id: {$in: notYetMasteredTopicIds}}, {sort: {masteryRequiredForGrade: 1}, limit: 1});
			console.log(topicsWithMinimumNotYetAchievedGrade);
			var nextGradeLevel = topicsWithMinimumNotYetAchievedGrade.fetch()[0].masteryRequiredForGrade;
			console.log("nextGradeLevel: "+nextGradeLevel)
			return Topics.find({courseId: courseId, masteryRequiredForGrade: nextGradeLevel, _id: {$in: notYetMasteredTopicIds}});
		} else {
			return null;
		}
	},
});
