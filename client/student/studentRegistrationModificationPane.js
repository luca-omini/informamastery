Template.studentRegistrationModificationPane.events({
	'click .approve-user-as-student-button': function(ev, template) {
		console.log("approve-user-as-student-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('approveUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .undo-approval-user-as-student-button': function(ev, template) {
		console.log("undo-approval-user-as-student-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('undoApprovalOfUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .withdraw-button': function(ev, template) {
		console.log("withdraw-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('withdrawUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .undo-withdrawal-button': function(ev, template) {
		console.log("undo-withdrawal-button clicked");
		var studentRegistration = Template.currentData();
		var courseId = studentRegistration.courseId;
		var studentId = studentRegistration.studentId;
		Meteor.call('undoWithdrawalOfUserAsStudent', studentId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
