Template.studentListPage1.helpers({
	findStudentRegistrationsForCourse: function(courseId) {
		return StudentRegistrations.find({courseId: courseId}, {sort: {creationDate: 1}});
	},
	getTopicsMasteredCount: function(courseId, studentId) {
		return TopicStatus.find({courseId: courseId, studentId: studentId, mastered: true}).count();
	},
	getTopicsReadyCount: function(courseId, studentId) {
		return TopicStatus.find({courseId: courseId, studentId: studentId, toBeChecked: true}).count();
	},
	getSkillsReadyCount: function(courseId, studentId) {
		var topicIds = Topics.find({courseId: courseId}).map(function(topic) {return topic._id;});
		return SkillStatus.find({topicId: {$in: topicIds}, studentId: studentId, ready: true}).count();
	},
	getMasteryChecksCount: function(courseId, studentId) {
		return MasteryChecks.find({courseId: courseId, studentId: studentId}).count();
	},

	getUsers: function() {
		return Meteor.users.find({}, {sort: {"profile.firstName": 1}});
	},
});
