Template.studentPageMenu.helpers({
	countAllStudentsPassedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: true}).count();
	},
	countAllStudentsFailedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: false}).count();
	},
	countAllAnnotationsForUserAndCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, removed: {$ne: true}}).count();
	},
	countMasteredTopicChecksForUserAndCourse: function(userId, courseId) {
		return TopicChecks.find({courseId: courseId, studentId: userId, mastered: true}, {sort: {endDate: -1}}).count();
	},
	countFailedTopicChecksForUserAndCourse: function(userId, courseId) {
		return TopicChecks.find({courseId: courseId, studentId: userId, mastered: false}, {sort: {endDate: -1}}).count();
	},
	countPendingBugsForCourseAndStudent: function(userId, courseId) {
		var masteredTopicIds = TopicChecks.find({courseId: courseId, studentId: userId, mastered: true}).map(function(topicCheck) {return topicCheck.topicId;});
		return TopicChecks.find({courseId: courseId, studentId: userId, topicId: {$nin: masteredTopicIds}, mastered: false}, {sort: {endDate: -1}}).count();
	},
	countStudentUnsolvedPracticeProblems: function(studentId) {
		return _findStudentUnsolvedPracticeProblems(studentId).count();
	},
	countStudentSolvedPracticeProblems: function(studentId) {
		return PracticeProblemSolutions.find({userId: studentId}).count();
	},
	countStudentOwnPublishedUnarchivedPracticeProblems: function(studentId) {
		return PracticeProblems.find({userId: studentId, published: true, archived: false}).count();
	},
});
