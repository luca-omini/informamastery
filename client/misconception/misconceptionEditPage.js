Template.misconceptionEditPage.rendered = function() {
	var self = this;
	console.log("Template.misconceptionEditPage.rendered", self);
	this.$('.add-topic-dropdown').dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("onChange value:", value, "text:", text, "selectedItem:", selectedItem, "this:", this, "self:", self);
			var misconceptionId = self.data.misconception._id;
			var topicId = value;
			Meteor.call('addMisconceptionTopic', misconceptionId, topicId, function(error, result) {
				$('.add-topic-dropdown').dropdown("restore defaults");
				// for some reason the above attaches the default class, making the text grey
				$('.add-topic-dropdown span.text.default').removeClass("default");
				if (error) {
					throwError(error.reason);
				}
			});
		},
	});
};

Template.misconceptionEditPage.events({
	'submit .update-form': function(event, template) {
		var courseId = this.course._id;
		var misconceptionId = this.misconception._id;
		var title = event.target.title.value;
		var description = event.target.description.value;
    var correction = event.target.correction.value;

		Meteor.call('updateMisconception', misconceptionId, title, description, correction, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("misconceptionPage", {courseId: courseId, misconceptionId: misconceptionId});
			}
		});
		// Prevent default form submit
		return false;
	},
  'click .delete-misconception-button': function(event, template) {
		event.preventDefault();
		console.log("click .delete-misconception-button");
    console.log("this:", this);
    var misconceptionId = this.misconception._id;
    var courseId = this.course._id;
    Meteor.call('removeMisconception', misconceptionId, function(error, result) {
      if (error) {
        throwError(error.reason);
      } else {
				Router.go("misconceptionListPage", {courseId: courseId});
			}
    });
  },
	'click .remove-topic-button': function(event, template) {
		event.preventDefault();
		console.log("click .remove-topic-button");
		var topicId = this._id;
		//console.log("this: ", this);
		//console.log("Template.instance(): ", Template.instance());
		//console.log("Template.currentData(): ", Template.currentData());
		//console.log("Template.parentData(): ", Template.parentData());
		//console.log("Template.parentData(2): ", Template.parentData(2));
		var misconceptionId = Template.currentData().misconception._id;
		Meteor.call("removeMisconceptionTopic", misconceptionId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-skill-button': function(event, template) {
		event.preventDefault();
		console.log("click .set-skill-button");
    // this seems to point to the skill (but template.currentData() doesn't!)
		var skill = this;
		console.log("skill: ", skill);
    //HACK: find misconception
    var misconception = Template.currentData().misconception;
    console.log("misconception: ", misconception);
		Meteor.call("addMisconceptionSkill", misconception._id, skill._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-skill-button': function(event, template) {
		event.preventDefault();
		console.log("click .clear-skill-button");
    // this seems to point to the skill (but template.currentData() doesn't!)
		var skill = this;
		console.log("skill: ", skill);
    //HACK: find misconception
    var misconception = Template.currentData().misconception;
    console.log("misconception: ", misconception);
		Meteor.call("removeMisconceptionSkill", misconception._id, skill._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-study-task-button': function(event, template) {
		event.preventDefault();
		console.log("click .set-study-task-button");
    // this seems to point to the studyTask (but template.currentData() doesn't!)
		var studyTask = this;
		console.log("studyTask: ", studyTask);
    //HACK: find misconception
    var misconception = Template.currentData().misconception;
    console.log("misconception: ", misconception);
		Meteor.call("addMisconceptionStudyTask", misconception._id, studyTask._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-study-task-button': function(event, template) {
		event.preventDefault();
		console.log("click .clear-study-task-button");
    // this seems to point to the studyTask (but template.currentData() doesn't!)
		var studyTask = this;
		console.log("studyTask: ", studyTask);
    //HACK: find misconception
    var misconception = Template.currentData().misconception;
    console.log("misconception: ", misconception);
		Meteor.call("removeMisconceptionStudyTask", misconception._id, studyTask._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-practice-problem-button': function(event, template) {
		event.preventDefault();
		console.log("click .set-practice-problem-button");
    // this seems to point to the practiceProblem (but template.currentData() doesn't!)
		var practiceProblem = this;
		console.log("practiceProblem: ", practiceProblem);
    //HACK: find misconception
    var misconception = Template.currentData().misconception;
    console.log("misconception: ", misconception);
		Meteor.call("addMisconceptionPracticeProblem", misconception._id, practiceProblem._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-practice-problem-button': function(event, template) {
		event.preventDefault();
		console.log("click .clear-practice-problem-button");
    // this seems to point to the practiceProblem (but template.currentData() doesn't!)
		var practiceProblem = this;
		console.log("practiceProblem: ", practiceProblem);
    //HACK: find misconception
    var misconception = Template.currentData().misconception;
    console.log("misconception: ", misconception);
		Meteor.call("removeMisconceptionPracticeProblem", misconception._id, practiceProblem._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});

function getTopicsForMisconception(misconception) {
  var topicIds = misconception.topicIds;
  if (!topicIds) {
    topicIds = [];
  }
  return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
}

Template.misconceptionEditPage.helpers({
	getCourseTopics: function(courseId) {
    console.log("getCourseTopics("+courseId+")");
		return Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}});
	},

  getTopicCountForMisconception: function(misconception) {
    console.log("getTopicCountForMisconception(misconception:", misconception, ")");
    return getTopicsForMisconception(misconception).count();
	},
  getTopicsForMisconception: function(misconception) {
    console.log("getTopicsForMisconception(misconception:", misconception, ")");
    return getTopicsForMisconception(misconception);
	},
  getSkillsForTopic: function(topicId) {
    console.log("getSkillsForTopic("+topicId+")");
    return Skills.find({topicId: topicId, removed: {$ne: true}});
  },
  getStudyTasksForTopic: function(topicId) {
    console.log("getStudyTasksForTopic("+topicId+")");
    return StudyTasks.find({topicId: topicId, removed: {$ne: true}});
  },
  getPracticeProblemsForTopic: function(topicId) {
    console.log("getPracticeProblemsForTopic("+topicId+")");
    return PracticeProblems.find({topicId: topicId, published: true, archived: false, removed: {$ne: true}});
  },

  isSkillCoveredInMisconception: function(skillId, misconception) {
    console.log("isSkillCoveredInMisconception("+skillId+", "+misconception+")");
    var skillIds = misconception.skillIds;
    if (!skillIds) {
      skillIds = [];
    }
    return _.contains(skillIds, skillId);
  },
  isStudyTaskCoveredInMisconception: function(studyTaskId, misconception) {
    console.log("isStudyTaskCoveredInMisconception("+studyTaskId+", "+misconception+")");
    var studyTaskIds = misconception.studyTaskIds;
    if (!studyTaskIds) {
      studyTaskIds = [];
    }
    return _.contains(studyTaskIds, studyTaskId);
  },
  isPracticeProblemCoveredInMisconception: function(practiceProblemId, misconception) {
    console.log("isPracticeProblemCoveredInMisconception("+practiceProblemId+", "+misconception+")");
    var practiceProblemIds = misconception.practiceProblemIds;
    if (!practiceProblemIds) {
      practiceProblemIds = [];
    }
    return _.contains(practiceProblemIds, practiceProblemId);
  },
});
