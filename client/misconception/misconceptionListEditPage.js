Template.misconceptionListEditPage.helpers({
  getMisconceptionsForCourse: function(courseId) {
    return Misconceptions.find({courseId: courseId, removed: {$ne: true}});
  },
});

Template.misconceptionListEditPage.events({
	'click .add-misconception-button': function(ev, template) {
		var courseId = this._id;
		Meteor.call('addMisconception', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("misconceptionEditPage", {courseId: courseId, misconceptionId: result});
			}
		});
	},
});
