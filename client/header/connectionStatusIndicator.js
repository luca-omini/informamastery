Template.connectionStatusIndicator.helpers({
  getStatus: function() {
    console.log("Template.connectionStatusIndicator.helpers.getStatus()");
    // call popup on elements that need it
    var template = Template.instance();
    console.log("template=", template);
    if (template.firstNode) {
      template.$('.element-with-popup').popup();
    }
    var status = Meteor.status();
    console.log("status:", status);
    return status;
  },
  seconds: function() {
    var ms = Meteor.status().retryTime - (new Date()).getTime();
    return Math.round(ms/1000);
  },
});

Template.connectionStatusIndicator.events({
  'click .retryNowButton': function(event) {
    console.log("retrying now to connect...");
    Meteor.reconnect();
  },
});
