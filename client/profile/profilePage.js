Template.profilePage.helpers({
});

Template.profilePage.events({
  "submit .form": function (event) {
    console.log("Template.profilePage.events:submit .form");
    var firstName = event.target.firstName.value;
    var lastName = event.target.lastName.value;
    var skypeName = event.target.skypeName.value;
    var faceTimeId = event.target.faceTimeId.value;
    var gmailAddress = event.target.gmailAddress.value;
    var biography = event.target.biography.value;
    var howDoYouKnowYouAreLearning = event.target.howDoYouKnowYouAreLearning.value;

    Meteor.call("updateProfile", firstName, lastName, skypeName, faceTimeId, gmailAddress, biography, howDoYouKnowYouAreLearning);

    // Prevent default form submit
    return false;
  },
  "click .resend-verification-email-button": function(event) {
  	console.log("this");
  	console.log(this);
  	console.log("Template.profilePage.resend-verification-email-button clicked");
  	Meteor.call("resendVerificationEmail", this.address);
  },
});
