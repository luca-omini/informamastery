Template.courseFilesPage.helpers({
  getCourseFiles: function() {
    console.log("Template.courseFilesPage.getCourseFiles()");
    console.log(this);
    var courseId = this._id;
    console.log(courseId);
    return CourseFiles.find({"metadata.courseId": courseId});
  },
  getCourseFilesCount: function(courseId) {
    console.log("Template.courseFilesPage.getCourseFilesCount()");
    console.log(courseId);
    return CourseFiles.find({"metadata.courseId": courseId}).count();
  },
  getCourseFilesSize: function(courseId) {
    console.log("Template.courseFilesPage.getCourseFilesSize()");
    console.log(courseId);
    var sizes = CourseFiles.find({"metadata.courseId": courseId}).map(function(file) {return file.length;});
    var totalSize = _.reduce(sizes, function(memo, num){ return memo + num; }, 0);
    return numeral(totalSize).format('0.0b')+" ("+totalSize+" bytes)";
  },
  settings: {
    class: "ui compact table",
    filters: ['myFilter'],
    showRowCount: true,
    enableRegex: true,
    fields: [
      {
        key: "filename",
        label: "File Name",
        headerClass: "collapsing",
        tmpl: Template.courseFileNameCell,
        fn: function(value, object) {
          return object.filename;
        },
      },
      {
        key: "owner",
        label: "Owner",
        headerClass: "collapsing",
        tmpl: Template.fileOwnerCell,
      },
      {
        key: "uploadDate",
        label: "Uploaded",
        headerClass: "collapsing",
        sort: "descending",
        tmpl: Template.fileUploadedCell,
      },
      {
        key: "length",
        label: "Size",
        headerClass: "collapsing",
        tmpl: Template.fileSizeCell,
        fn: function(value, object) {
          return object.length;
        },
      },
      {
        key: "delete",
        label: "",
        headerClass: "collapsing",
        tmpl: Template.courseFileDeleteCell,
      },
    ],
  },
});
