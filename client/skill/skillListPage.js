Template.skillListPage.helpers({
  getTopicsForCourse: function(courseId) {
    //console.log("courseTopicsShow::getTopicsForCourse("+courseId+")");
    var ts = Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}} );
    //console.log(ts.fetch());
    return ts;
  },
  getSkillsForTopic: function(topicId) {
    var ss = Skills.find({topicId: topicId, removed: {$ne: true}} );
    return ss;
  },
  countSkillsForTopic: function(topicId) {
    var ssc = Skills.find({topicId: topicId, removed: {$ne: true}} ).count();
    return ssc;
  },
  isSkillRed: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
		return skillStatus?skillStatus.status=="Red":false;
	},
  isSkillAmber: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
		return skillStatus?skillStatus.status=="Amber":false;
	},
  isSkillGreen: function(skillId) {
		var skillStatus = SkillStatus.findOne({studentId: Meteor.userId(), skillId: skillId});
    // skillStatus.ready implies skillStatus.status=="Green"
    return skillStatus?skillStatus.ready:false;
	},
});

Template.skillListPage.events({
	'click .set-red-button': function(ev, template) {
		var skillId = this._id;
		console.log(".set-red-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusRed', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-amber-button': function(ev, template) {
		var skillId = this._id;
		console.log(".set-amber-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusAmber', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-green-button': function(ev, template) {
		var skillId = this._id;
		console.log(".set-green-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusGreen', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-button': function(ev, template) {
		var skillId = this._id;
		console.log(".clear-button clicked. Skill's _id: "+skillId);
		Meteor.call('setSkillStatusCleared', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
