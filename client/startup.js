Meteor.startup(function () {
  // Setup resumable.js in the UI

  // Prevent default drop behavior (loading a file) outside of the drop zone
  window.addEventListener('dragover', function(e) {e.preventDefault();}, false);
  window.addEventListener('drop', function(e) {e.preventDefault();}, false);

  // Note: We use one Meteor session variable for each file we're uploading.
  // The name of that variable is the unique identifier of the file.
  // The value of that variable is the upload progress percentage (0..100).

  //=== LabSubmissionFiles ===

  // When a file is added
  LabSubmissionFiles.resumable.on('fileAdded', function(file) {
    console.log("LabSubmissionFiles:fileAdded for file ", file);
    // Keep track of its progress reactively in a session variable
    Session.set(file.uniqueIdentifier, 0);
    // Create a new file in the file collection to upload to
    LabSubmissionFiles.insert({
      _id: file.uniqueIdentifier, // This is the ID resumable will use
      filename: file.fileName,
      contentType: file.file.type,
      metadata: {
        courseId: file.file.courseId,
        labId: file.file.labId,
        labItemId: file.file.labItemId,
        labTeamId: file.file.labTeamId,
        userIds: file.file.userIds,
      },
    }, function(err, _id) {
      if (err) {
        console.warn("File creation failed!", err);
        return;
      }
      // Once the file exists on the server, start uploading
      console.log("starting to upload file ", file);
      LabSubmissionFiles.resumable.upload();
    });
  });

  // Update the upload progress session variable
  LabSubmissionFiles.resumable.on('fileProgress', function(file) {
    console.log("LabSubmissionFiles:fileProgress for file ", file);
    Session.set(file.uniqueIdentifier, Math.floor(100*file.progress()));
  });

  // Finish the upload progress in the session variable
  LabSubmissionFiles.resumable.on('fileSuccess', function(file) {
    console.log("LabSubmissionFiles:fileSuccess for file ", file);
    Session.set(file.uniqueIdentifier, undefined);
    // notify server so it can respond to this
    // NOTE: I think there might be a better option for this.
    //       The server might just observe LabSubmissionFiles
    //       to see whenever files get added.
    //var labSubmissionFile = LabSubmissionFiles.findOne({_id: file.uniqueIdentifier});
    Meteor.call('labSubmissionFileUploadFinished', file.uniqueIdentifier);
  });

  // More robust error handling needed!
  LabSubmissionFiles.resumable.on('fileError', function(file) {
    console.log("LabSubmissionFiles:fileError for file ", file);
    Session.set(file.uniqueIdentifier, undefined);
  });


  //=== StudentPhotoFiles ===

  // When a file is added
  StudentPhotoFiles.resumable.on('fileAdded', function(file) {
    console.log("StudentPhotoFiles:fileAdded for file ", file);
    // Keep track of its progress reactively in a session variable
    Session.set(file.uniqueIdentifier, 0);
    // Create a new file in the file collection to upload to
    StudentPhotoFiles.insert({
      _id: file.uniqueIdentifier, // This is the ID resumable will use
      filename: file.fileName,
      contentType: file.file.type,
      metadata: {
        courseId: file.file.courseId,
        userId: file.file.userId,
        studentRegistrationId: file.file.studentRegistrationId,
      },
    }, function(err, _id) {
      if (err) {
        console.warn("File creation failed!", err);
        return;
      }
      // Once the file exists on the server, start uploading
      console.log("starting to upload file ", file);
      StudentPhotoFiles.resumable.upload();
    });
  });

  // Update the upload progress session variable
  StudentPhotoFiles.resumable.on('fileProgress', function(file) {
    console.log("StudentPhotoFiles:fileProgress for file ", file);
    Session.set(file.uniqueIdentifier, Math.floor(100*file.progress()));
  });

  // Finish the upload progress in the session variable
  StudentPhotoFiles.resumable.on('fileSuccess', function(file) {
    console.log("StudentPhotoFiles:fileSuccess for file ", file);
    Session.set(file.uniqueIdentifier, undefined);
    // notify server so it can respond to this
    // NOTE: I think there might be a better option for this.
    //       The server might just observe StudentPhotoFiles
    //       to see whenever files get added.
    //var studentPhotoFile = StudentPhotoFiles.findOne({_id: file.uniqueIdentifier});
    Meteor.call('studentPhotoFileUploadFinished', file.uniqueIdentifier);
  });

  // More robust error handling needed!
  StudentPhotoFiles.resumable.on('fileError', function(file) {
    console.log("StudentPhotoFiles:fileError for file ", file);
    Session.set(file.uniqueIdentifier, undefined);
  });


  //=== CourseFiles ===

  // When a file is added
  CourseFiles.resumable.on('fileAdded', function(file) {
    console.log("CourseFiles:fileAdded for file ", file);
    // Keep track of its progress reactively in a session variable
    Session.set(file.uniqueIdentifier, 0);
    // Create a new file in the file collection to upload to
    CourseFiles.insert({
      _id: file.uniqueIdentifier, // This is the ID resumable will use
      filename: file.fileName,
      contentType: file.file.type,
      metadata: {
        courseId: file.file.courseId,
        userId: file.file.userId,
      },
    }, function(err, _id) {
      if (err) {
        console.warn("File creation failed!", err);
        return;
      }
      // Once the file exists on the server, start uploading
      console.log("starting to upload file ", file);
      CourseFiles.resumable.upload();
    });
  });

  // Update the upload progress session variable
  CourseFiles.resumable.on('fileProgress', function(file) {
    console.log("CourseFiles:fileProgress for file ", file);
    Session.set(file.uniqueIdentifier, Math.floor(100*file.progress()));
  });

  // Finish the upload progress in the session variable
  CourseFiles.resumable.on('fileSuccess', function(file) {
    console.log("CourseFiles:fileSuccess for file ", file);
    Session.set(file.uniqueIdentifier, undefined);
    // notify server so it can respond to this
    // NOTE: I think there might be a better option for this.
    //       The server might just observe CourseFiles
    //       to see whenever files get added.
    //var courseFile = CourseFiles.findOne({_id: file.uniqueIdentifier});
    Meteor.call('courseFileUploadFinished', file.uniqueIdentifier);
  });

  // More robust error handling needed!
  CourseFiles.resumable.on('fileError', function(file) {
    console.log("CourseFiles:fileError for file ", file);
    Session.set(file.uniqueIdentifier, undefined);
  });

});
