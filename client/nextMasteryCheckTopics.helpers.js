Template.nextMasteryCheckTopicsPage.helpers({
	getTopicsSelectedForNextMasteryCheck: function(courseId) {
		console.log("Template.nextMasteryCheckTopicsPage.helpers.getTopicsSelectedForNextMasteryCheck("+courseId+")");
		var topicIds = TopicStatus.find({courseId: courseId, toBeChecked:true}).map(function(ts) {return ts.topicId});
		return Topics.find({_id: {$in: topicIds}});
	},
	getNumberOfTopicsToBeCheckedForNextMasteryCheck: function(courseId) {
		return TopicStatus.find({courseId: courseId, toBeChecked:true}).count();
	},
	getNumberOfStudentsToBeCheckedForNextMasteryCheck: function(courseId) {
		console.log("Template.nextMasteryCheckTopicsPage.helpers.getNumberOfStudentsSelectedForNextMasteryCheck("+courseId+")");
		var studentIds = TopicStatus.find({courseId: courseId, toBeChecked:true}).map(function(ts) {return ts.studentId});
		console.log(studentIds);
		var uniqueStudentIds = _.uniq(studentIds);
		console.log(uniqueStudentIds);
		return uniqueStudentIds.length;
	},
	getNumberOfStudentsWhoSelectedThisTopic: function(topicId) {
		return TopicStatus.find({topicId: topicId, toBeChecked:true}).count();
	},
	getEstimatedTotalDuration: function(courseId, estimatedDurationPerCheck, numberOfTesters) {
		return TopicStatus.find({courseId: courseId, toBeChecked:true}).count() * estimatedDurationPerCheck / numberOfTesters;
	},
	countAllStudentsPassedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: true}).count();
	},
	countAllStudentsFailedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: false}).count();
	},
	countAllStudentsAnnotationsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, removed: {$ne: true}}).count();
	},
});
