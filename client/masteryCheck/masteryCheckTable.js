function getMasteryChecks(courseId, studentId, testerId, ongoing) {
	if (courseId) {
		var query = {
			courseId: courseId,
		};
		query.finished = !ongoing;
		if (studentId) {
			//query.studentId = studentId;
			query["$or"] = [{studentId: studentId}, {studentIds: studentId}];
		} else if (testerId) {
			query.testerId = testerId;
		}
		return MasteryChecks.find(query, {sort: {startDate: -1}});
	} else {
		return undefined;
	}
}

Template.masteryCheckTable.helpers({
	getMasteryChecks: function(courseId, studentId, testerId, ongoing) {
		console.log("getMasteryChecks()");
		return getMasteryChecks(courseId, studentId, testerId, ongoing);
	},
	countMasteryChecks: function(courseId, studentId, testerId, ongoing) {
		console.log("countMasteryChecks()");
		return getMasteryChecks(courseId, studentId, testerId, ongoing).count();
	},
});
