Template.newMasteryCheckPage.created = function() {
	console.log("Template.newMasteryCheckPage.created");
	var pageCreated = moment(new Date());
	this.timer = Meteor.setInterval(function() {
		var now = moment(new Date());
		var minutes = now.diff(pageCreated, 'minutes');
		Session.set("minutesSincePageCreated", minutes);
	}, 1000);
};

Template.newMasteryCheckPage.destroyed = function() {
	console.log("Template.newMasteryCheckPage.destroyed");
	Meteor.clearInterval(this.timer);
};

Template.newMasteryCheckPage.rendered = function() {
	console.log("Template.newMasteryCheckPage.rendered");
    var moment1 = moment(new Date());
    console.log(moment1.format("dddd, MMMM Do YYYY, H:mm:ss"));
    var moment2 = moment(new Date());
    console.log(moment2.format("dddd, MMMM Do YYYY, H:mm:ss"));
    var diffInMinutes = moment1.diff(moment2, 'minutes');
    console.log(diffInMinutes);
};

Template.newMasteryCheckPage.helpers({
	getMinutesSincePageRendered: function() {
		return Session.get("minutesSincePageCreated");
	},
	getTopicChecksForMasteryCheck: function(masteryCheckId) {
		return TopicChecks.find({masteryCheckId: masteryCheckId});
	},
});

Template.newMasteryCheckPage.events({
	'click .finish-mastery-check-button': function(ev, template) {
		console.log("finish-mastery-check-button clicked");
		var masteryCheck = Template.currentData();
		var courseId = masteryCheck.courseId;
		//var studentId = masteryCheck.studentId;
		//var studentRegistration = StudentRegistrations.findOne({courseId: courseId, studentId: studentId});
		Meteor.call('finishMasteryCheck', masteryCheck._id, function(error, masteryCheckId) {
			if (error) {
				throwError(error.reason);
			} else {
        Router.go("masteryCheckQueuePage", {_id: masteryCheck.courseId});
				//Router.go("studentPage", {courseId: courseId, _id: studentRegistration._id});
			}
		});
	},
	'blur .mastery-check-notes': function(ev, template) {
		var masteryCheckId = Template.currentData()._id;
		console.log(".mastery-check-notes area blurred: masteryCheckId: "+masteryCheckId);
		var notes = ev.target.value;
		Meteor.call('setMasteryCheckNotes', masteryCheckId, notes, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .delete-mastery-check-button': function(e, t) {
		var masteryCheckId = Template.currentData()._id;
		console.log(".delete-mastery-check-button clicked: masteryCheckId: "+masteryCheckId);
		var masteryCheck = Template.currentData();
		Meteor.call('deleteMasteryCheck', masteryCheckId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
        console.log("Router.go('masteryCheckListPage', {courseId: "+masteryCheck.courseId+"})");
        Router.go("masteryCheckListPage", {courseId: masteryCheck.courseId});
			}
		});
	},
	'click .reopen-mastery-check-button': function(e, t) {
		var masteryCheckId = Template.currentData()._id;
		console.log(".reopen-mastery-check-button clicked: masteryCheckId: "+masteryCheckId);
		Meteor.call('reopenMasteryCheck', masteryCheckId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
