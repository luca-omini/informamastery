Template.masteryCheckQueuePage.created = function() {
	console.log("Template.masteryCheckQueuePage.created");
	this.timer = Meteor.setInterval(function() {
		Session.set("timerTime", new Date());
	}, 1000);
};

Template.masteryCheckQueuePage.destroyed = function() {
	console.log("Template.masteryCheckQueuePage.destroyed");
	Meteor.clearInterval(this.timer);
};

Template.masteryCheckQueuePage.helpers({
  getOngoingMasteryCheckSession: function(courseId) {
    return MasteryCheckSessions.findOne({courseId: courseId, ongoing: true});
  },
  determineDuration: function(date) {
    var now = moment(Session.get("timerTime"));
    var minutes = now.diff(date, 'minutes');
    return minutes;
  },
  getMasteryCheckQueueEntries: function(masteryCheckSessionId) {
    return MasteryCheckQueueEntries.find({masteryCheckSessionId: masteryCheckSessionId}, {sort: {creationDate: 1}});
  },
  getMasteryCheckRegistration: function(masteryCheckRegistrationId) {
    return MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId});
  },
});

Template.masteryCheckQueuePage.events({
  'click .start-mastery-check-session-button': function(ev, template) {
    console.log("start-mastery-check-session-button clicked");
		var courseId = this._id;
    console.log("courseId", courseId);
		Meteor.call('startNewMasteryCheckSession', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .stop-mastery-check-session-button': function(ev, template) {
    console.log("stop-mastery-check-session-button clicked");
		var courseId = this._id;
    console.log("courseId", courseId);
		Meteor.call('stopOngoingMasteryCheckSessions', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .start-mastery-check-button': function(ev, template) {
    console.log("start-mastery-check-button clicked");
		var masteryCheckRegistrationId = this._id;
    var masteryCheckRegistration = MasteryCheckRegistrations.findOne({_id: masteryCheckRegistrationId});
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
    console.log(masteryCheckRegistration);
    Meteor.call('startNewMasteryCheck', masteryCheckRegistrationId, function(error, masteryCheckId) {
      if (error) {
        throwError(error.reason);
      } else {
        console.log("Router.go('newMasteryCheckPage', {})");
        Router.go("newMasteryCheckPage", {courseId: masteryCheckRegistration.courseId, masteryCheckId: masteryCheckId});
      }
    });
	},
});
