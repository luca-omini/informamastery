Template.masteryCheckRegistrationListPage.helpers({
	getAllMasteryCheckRegistrations: function(courseId) {
    return MasteryCheckRegistrations.find({courseId: courseId}, {sort: {creationDate: -1}});
  },
});
