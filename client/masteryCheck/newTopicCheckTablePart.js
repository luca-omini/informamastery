Template.newTopicCheckTablePart.rendered = function() {
	console.log("Template.newTopicCheckTablePart.rendered");
	this.$('.study-task-label').popup({
    inline: true,
		position: 'top center'
  });
};

Template.newTopicCheckTablePart.helpers({
	getSkillChecksForTopicCheck: function(topicCheckId) {
		return SkillChecks.find({topicCheckId: topicCheckId});
	},
	getStudyTasksForTopic: function(topicId) {
		console.log("getStudyTasksForTopic("+topicId+")");
		var ss = StudyTasks.find({topicId: topicId, removed: {$ne: true}} );
		return ss;
	},
	getStudyTaskStatusColor: function(studyTaskId, userId) {
		console.log("getStudyTaskStatusColor("+studyTaskId+", "+userId+")");
		//TODO: Treat textbook and class session study task kinds specially.
		//TODO: Move this to creation of mastery check method (and store result in TopicCheck),
		//TODO  and here just fetch result stored there in SkillCheck.
		var status = StudyTaskStatus.findOne({studyTaskId: studyTaskId, userId: userId});
		if (!status) {
			status = {};
		}
		var s = "";
		var color = "";
		if (status.recallText) {
			s="review";
			color="blue";
		} else if (status.finished) {
			s="recall";
			color="teal";
		} else if (status.started) {
			s="study";
			color="gray";
		} else {
			s="prepare";
		}
		return color;
	},
});

Template.newTopicCheckTablePart.events({
	'blur .topic-feedback': function(ev, template) {
		var topicCheckId = Template.currentData().topicCheck._id;
		console.log(".topic-feedback area blurred: topicCheckId: "+topicCheckId);
	  var feedback = ev.target.value;
		Meteor.call('setTopicCheckFeedback', topicCheckId, feedback, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
