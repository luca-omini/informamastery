Template.newSkillCheckRow.rendered = function() {
	console.log("Template.newSkillCheckRow.rendered");
	this.$('.skill-label').popup({
    inline: true,
		position: 'top center'
  });
};

Template.newSkillCheckRow.helpers({
	getSkillStatusColor: function(skillId, userId) {
		console.log("getSkillStatusColor("+skillId+", "+userId+")");
		//TODO: push to mastery check creation
		var skillStatus = SkillStatus.findOne({studentId: userId, skillId: skillId});
		if (!skillStatus) {
			skillStatus = {};
		}
		var color = "";
		if (skillStatus.status=="Red") {
			color="red";
		} else if (skillStatus.status=="Amber") {
			color="yellow";
		} else if (skillStatus.ready) {
			// skillStatus.ready implies skillStatus.status=="Green"
			color="green";
		}
		return color;
	},
});

Template.newSkillCheckRow.events({
	'click .set-mastered-button': function(ev, template) {
		var skillCheckId = Template.currentData().skillCheck._id;
		console.log(".set-mastered-button clicked: skillCheckId: "+skillCheckId);
		Meteor.call('setSkillCheckMastered', skillCheckId, true, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .clear-mastered-button': function(ev, template) {
		var skillCheckId = Template.currentData().skillCheck._id;
		console.log(".clear-mastered-button clicked: skillCheckId: "+skillCheckId);
		Meteor.call('setSkillCheckMastered', skillCheckId, false, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
