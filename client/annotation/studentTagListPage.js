Template.studentTagListPage.helpers({
	countUserTagsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "tag", removed: false}).count();
	},
	getUserTagsForCourse: function(userId, courseId) {
		return Annotations.find({courseId: courseId, userId: userId, kind: "tag", removed: false}, {sort: {modificationDate: -1}});
	},
	getUserTagTextsForCourse: function(userId, courseId) {
		var tagTexts = Annotations.find({courseId: courseId, userId: userId, kind: "tag", removed: false}, {sort: {modificationDate: -1}}).map(function(doc) {return doc.text;});
		var uniqueTagTexts = _.uniq(tagTexts, true);
		return uniqueTagTexts;
	},
	getUserTagsForCourseAndText: function(userId, courseId, text) {
		console.log("getUserTagsForCourseAndText("+userId+", "+courseId+", "+text+")");
		return Annotations.find({courseId: courseId, text: text, userId: userId, kind: "tag", removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
});


Template.studentTagListPage.events({
	'click .remove-annotation-button': function(ev, template) {
		var annotationId = this._id;
		//console.log(".remove-annotation-button clicked");
		Meteor.call('removeAnnotation', annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
