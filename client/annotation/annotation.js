Template.annotation.created = function() {
  console.log("Template.annotation.created()");
  this.isEditing = !this.data.text;
  this.isEditingDep = new Deps.Dependency();
};

Template.annotation.rendered = function() {
  console.log("Template.annotation.rendered()");
  this.$('.question-annotation-label')
  .popup({
    inline   : true,
    hoverable: true,
    position : 'bottom left',
    delay: {
      show: 300,
      hide: 100
    }
  });
};

Template.annotation.events({
  'dblclick .body-label': function(e, t) {
    console.log("Template.annotation.events: click .body-label");
    t.isEditing = true;
    t.isEditingDep.changed();
  },
  'submit form': function(event, template) {
    console.log("submit form");
    event.preventDefault();
		var annotationId = this._id;
    console.log(annotationId);
    var form = $(event.target);
    console.log(form);
		var text = form.find('.body-field').val();
    console.log(text);
		Meteor.call("updateAnnotationText", annotationId, text, function(error, result) {
			if (error) {
        console.log("Error");
				throwError(error.reason);
			} else {
				console.log("removed.");
			}
		})
		template.isEditing = false;
		template.isEditingDep.changed();
	},
  'blur .body-field': function(e, t) {
    console.log("Template.annotation.events.blur()");
    var annotationId = this._id;
    var text = e.target.value;
    Meteor.call("updateAnnotationText", annotationId, text, function(error, result) {
      if (error) {
        throwError(error.reason);
      } else {
        console.log("removed.");
      }
    })
    t.isEditing = false;
    t.isEditingDep.changed();
  },
  'click .remove-annotation-button': function(e, t) {
    console.log("Template.annotation.events: click .remove-annotation-button");
    var annotationId = this._id;
    Meteor.call("removeAnnotation", annotationId, function(error, result) {
      if (error) {
        throwError(error.reason);
      } else {
        console.log("removed.");
      }
    });
  },
  'click .question-header': function(event, template) {
    console.log("Template.annotation.events: click .question-header");
    var courseId = this.courseId;
    var annotationId = this._id;
    Router.go("questionPage", {courseId: courseId, questionId: annotationId});
  },
});

Template.annotation.helpers({
  editing: function() {
    console.log("Template.annotation.helpers.editing()");
    var t = Template.instance();
    t.isEditingDep.depend();
    return t.isEditing;
  },
  kindIs: function(arg) {
    console.log("Template.annotation.helpers.kindIs("+arg+")");
    //console.log("this:");
    //console.log(this);
    return this.kind===arg;
  },
  kindIsUndefinedOr: function(arg) {
    console.log("Template.annotation.helpers.kindIs("+arg+")");
    //console.log("this:");
    //console.log(this);
    return this.kind===undefined || this.kind===arg;
  },
  getAnswerCount: function(annotationId) {
    return AnnotationResponses.find({annotationId: annotationId, removed: {$ne: true}}).count();
  },
  isOpen: function(annotationId) {
    return AnnotationResponses.find({annotationId: annotationId, accepted: true, removed: {$ne: true}}).count();
  },
});
