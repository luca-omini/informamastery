// TODO: extract functions used in global Template helpers in InformaMastery.js (e.g., isAdmin()) and place them here

// NOTE: This is a client-only file, so these functions are NOT visible for server-side code.
//       That's bad, because we would like functions like these to be usable on client and server code.
//       We should place these functions in lib/registration.methods.js instead.
isAdmin = function() {
  //console.log("isAdmin()");
  var result = Roles.userIsInRole(Meteor.userId(), 'admin', Roles.GLOBAL_GROUP);
  //console.log("isAdmin() -> "+result);
  return result;
}

isOnTeachingTeam = function(courseId) {
  //console.log("isOnTeachingTeam("+courseId+")");
  var result = (!!AssistantRegistrations.findOne({assistantId: Meteor.userId(), courseId: courseId}))
    || (!!InstructorRegistrations.findOne({instructorId: Meteor.userId(), courseId: courseId}));
  //console.log("isOnTeachingTeam() -> "+result);
  return result;
}
