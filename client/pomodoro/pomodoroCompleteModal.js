Template.pomodoroCompleteModal.helpers({
});


Template.pomodoroCompleteModalInner.helpers({
	getOngoingPomodoro: function() {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		return Pomodoros.findOne({courseId: courseId, userId: Meteor.userId(), ongoing: true});
	},
});


Template.pomodoroCompleteModal.events({
});


Template.pomodoroCompleteModalInner.events({
	'click .complete-pomodoro-toggle-button': function (event) {
		console.log(event);
		console.log(event.currentTarget);
		//$(".complete-pomodoro-toggle-button").addClass("basic");
		//$(event.target).removeClass("basic");
	},
	'submit .form': function(event, template) {
		console.log("submit .form");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
		// Prevent default form submit
		return false;
	},
});
