Tracker.autorun(function() {
	//console.log("====== executing autorun check for showing/hiding complete-pomodoro-modal ======");
	try {
		var course = Session.get("course");
		var courseId = course?course._id:undefined;
		var pomodoro = Pomodoros.findOne({courseId: courseId, userId: Meteor.userId(), ongoing: true});
		if (pomodoro) {
			var startMoment = moment(pomodoro.startTime);
			var endMoment = moment(TimeSync.serverTime(null, 1000)); // update every 1 seconds
			var durationInMinutes = endMoment.diff(startMoment, 'minutes');
			var minutesLeft = _pomodoroDuration - durationInMinutes;
			if (minutesLeft<=0) {
				$('.complete-pomodoro-modal').modal({
					closable: false,
					onShow: function() {
						console.log("complete-pomodoro-modal onShow()");
						$(this).find(".complete-pomodoro-comment-field").val("");
						$(this).find(".complete-pomodoro-toggle-button").addClass("basic");
						// play sound (once?)
						var sound = new Howl({
							urls: ['/sounds/PomodoroTimerEnd.mp3'],
						});
						console.log(sound);
						sound.play();
						// register handler for toggle buttons (Spacebars template won't work for detached modals?)
						var self = this;
						$(this).find(".complete-pomodoro-toggle-button").click(function(event) {
							console.log(".complete-pomodoro-toggle-button click handler in onShow");
							$(self).find(".complete-pomodoro-toggle-button").addClass("basic");
							$(event.currentTarget).removeClass("basic");
						});
					},
					onDeny: function(){
						//return false;
					},
					onApprove: function() {
						console.log("complete-pomodoro-modal onApprove()");
						console.log(this);
						var course = Session.get("course");
						var courseId = course?course._id:undefined;
						var pomodoro = Pomodoros.findOne({userId: Meteor.userId(), courseId: courseId, ongoing: true});
						var pomodoroId = pomodoro?pomodoro._id:undefined;
						var comment = $(this).find(".complete-pomodoro-comment-field").val();
						var goalAccomplished = $(this).find(".complete-pomodoro-toggle-button").not(".basic").attr("name");
						Meteor.call("completePomodoro", pomodoroId, goalAccomplished, comment, function(error, result) {
							if (error) {
								throwError(error.reason);
							} else {
							}
						});
					},
				}).modal('show');
			} else {
				$('.complete-pomodoro-modal').modal('hide');
			}
		} else {
			$('.complete-pomodoro-modal').modal('hide');
		}
	} catch (err) {
		console.log("Tracker.autorun(....) in pomodoroCompleteDialogTracker.js -- something threw exception:");
		console.log(err);
		console.log("Moving on.");
	}
});
