Template.pomodoroGoalAccomplishedCell.helpers({
	getGoalAccomplishedIconName: function(goalAccomplished) {
		if (goalAccomplished==="no") {
			return "red frown";
		} else if (goalAccomplished==="partially") {
			return "yellow meh";
		} else {
			return "green smile";
		}
	},
});
