Template.pomodoroStartModal.helpers({
});


Template.pomodoroStartModalInner.helpers({
	getTopics: function() {
		var course = Session.get("course");
		if (course) {
			return Topics.find({courseId: course._id});
		} else {
			return null;
		}
	},
	getLabs: function() {
		var course = Session.get("course");
		if (course) {
			return Labs.find({courseId: course._id});
		} else {
			return null;
		}
	},
});


Template.pomodoroStartModalInner.events({
	'submit .form': function(event, template) {
		console.log("submit .form");
		console.log("event:");
		console.log(event);
		console.log("template:");
		console.log(template);
		console.log("data context:");
		console.log(this);
		// Prevent default form submit
		return false;
	},
});
