Template.topicQuestionsPage.helpers({
	getTopicAnnotationsForCourse: function(topicId, courseId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({courseId: courseId, skillId: {$in: skillIds}, removed: false}, {sort: {modificationDate: -1}});
	},
	containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
	countQuestionsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, kind: "question", removed: false}).count();
	},
	getQuestionsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, kind: "question", removed: false}, {sort: {modificationDate: -1}});
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "creationDate",
					label: "Created",
					tmpl: Template.annotationCreationDateCell,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "modificationDate",
					label: "Modified",
					sort: "descending",
					tmpl: Template.annotationModificationDateCell,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "userId",
					label: "Author",
					tmpl: Template.annotationAuthorCell,
				},
				{
					key: "text",
					label: "Question",
					tmpl: Template.annotationTextCell,
				},
				{
					key: "score",
					label: "Score",
					headerClass: "collapsing",
					cellClass: "right aligned",
				},
				{
					key: "answerCount",
					label: "Answers",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {
						if (object) {
							return AnnotationResponses.find({annotationId: object._id}).count();
						} else {
							return 0;
						}
					},
				},
			],
		};
	},
});
