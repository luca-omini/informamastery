Template.topicRow.helpers({
	hasMasteryCheckRegistration: function() {
		console.log("topicRow::hasMasteryCheckRegistration() -> ");
		var count = MasteryCheckRegistrations.find({studentIds: Meteor.userId()}).count();
		console.log("  "+count);
		return count;
	},
	isTopicRegisteredForMasteryCheck: function(topicId) {
		console.log("topicRow::isTopicRegisteredForMasteryCheck("+topicId+") -> ");
		var count = MasteryCheckRegistrations.find({studentIds: Meteor.userId(), topicIds: topicId}).count();
		console.log("  "+count);
		return count>0;
	},
	hasTopicBeenMastered: function(topicId) {
		console.log("topicRow::hasTopicBeenMastered("+topicId+") -> ");
		var count = TopicChecks.find({studentIds: Meteor.userId(), topicId: topicId, mastered: true}).count();
		console.log("  "+count);
		return count>0;
	},
	isTopicMastered: function(topicId) {
		return TopicStatus.find({studentId: Meteor.userId(), topicId: topicId, mastered: true}).count()>0;
	},
	isTopicToBeChecked: isTopicToBeChecked,
	isTopicMastered: isTopicMastered,
	isTopicReady: isTopicReady,
	canAddMoreTopicsToBeChecked: canAddMoreTopicsToBeChecked,
	updateProgressBar: function(topicId) {
		// Getting this progress bar to update reactively was really, REALLY painful!
		// Template.showTopic.rendered() is NOT called when the template changes reactively.
		// So to call some jQuery code ($('#progress').progress()) to get SemanticUI's progress bar
		// to update itself reactively has to be done otherwise.
		// After a LOT of StackOverflow and Google, I found out that we can use a helper (this one)
		// that will be triggered reactively (here, because the computeProgress() computation depends on changed values).
		// Then, given that the helper is run when the data changes (specifically, when the contents of the template is recomputed),
		// we need to somehow update the DOM. To access the DOM, we can get the Template.instance().
		// But at the time this helper runs, the DOM does not yet exist.
		// So we defer(), and only retrieve the progress bar's DOM node then (after the template has been rendered/updated).
		//console.log("Template.topicRow.helpers.updateProgressBar("+topicId+")");
		var progress = computeProgress(topicId);
		var context = this;
		var template = Template.instance();
		Meteor.defer(function() {
			//console.log("Template.topicRow.helpers.updateProgressBar....deferredFunc");
			//console.log(context);
			//console.log(template);
			var progressBar = template.$('.progress');
			//console.log(progressBar);
			progressBar.progress({percent: progress});
			//progressBar.progress({label: "ratio", text: {ratio: "{value} of {total}"}});
		});
	},
	countAllStudentsPassedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: true}).count();
	},
	countAllStudentsFailedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: false}).count();
	},
	countAllStudentsAnnotationsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, removed: {$ne: true}}).count();
	},
	countAllStudentsQuestionsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, kind: "question", removed: {$ne: true}}).count();
	},
	countAllPublishedUnarchivedPracticeProblemsForTopic: function(topicId) {
		return PracticeProblems.find({topicId: topicId, published: true, archived: false}).count();
	},
	countOwnPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), published:true, archived:false}).count();
	},
});


Template.topicRow.events({
	'click .add-topic-to-check-button': function(ev, template) {
		var topicId = this._id;
		var reg = MasteryCheckRegistrations.findOne({studentIds: Meteor.userId()});
		var masteryCheckRegistrationId = reg._id;
		Meteor.call('addTopicToMasteryCheckRegistration', masteryCheckRegistrationId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .remove-topic-to-check-button': function(ev, template) {
		var topicId = this._id;
		var reg = MasteryCheckRegistrations.findOne({studentIds: Meteor.userId()});
		var masteryCheckRegistrationId = reg._id;
		Meteor.call('removeTopicFromMasteryCheckRegistration', masteryCheckRegistrationId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
