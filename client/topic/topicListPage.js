Template.topicListPage.helpers({
	lookupThemeTitle: function(themeId) {
		return Themes.findOne({_id: themeId}).title;
	},
	lookupThemeIcon: function(themeId) {
		return Themes.findOne({_id: themeId}).icon;
	},
	lookupThemeColor: function(themeId) {
		return Themes.findOne({_id: themeId}).color;
	},
	getTopicsForCourse: function(courseId) {
		//console.log("courseTopicsShow::getTopicsForCourse("+courseId+")");
		var ts = Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}} );
		//console.log(ts.fetch());
		return ts;
	},
	hasTopicsForCourseToBeChecked: function(courseId) {
		return getTopicsForCourseToBeChecked(courseId).count()>0;
	},
	canAddMoreTopicsForCourseToBeChecked: function(courseId) {
		console.log("Template.courseTopicsShow.helpers.canAddMoreTopicsForCourseToBeChecked("+courseId+")");
		var max = Courses.findOne({_id: courseId}).maxTopicsPerMasteryCheck;
		console.log("max: "+max);
		return getTopicsForCourseToBeChecked(courseId).count()<max;
	},
	getTopicsForCourseToBeChecked: function(courseId) {
		return getTopicsForCourseToBeChecked(courseId);
	},
});
