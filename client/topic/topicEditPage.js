Template.topicEditPage.rendered = function() {
	console.log("Template.topicEditPage.rendered");
	$(".why-pedagogy-label").popup({
    inline: true
	});
	$('.theme-dropdown').dropdown({
		onChange: function(value, text, selectedItem) {
			console.log("theme changed. value="+value+", text="+text+", selectedItem="+selectedItem);
		},
	});
};


Template.topicEditPage.helpers({
	getThemes: function(courseId) {
		return Themes.find({courseId: courseId, removed: {$ne: true}});
	},
});


Template.topicEditPage.events({
	'submit .form': function (event) {
		event.preventDefault();
		var courseId = this.course._id;
		var topicId = this.topic._id;
		var title = event.target.title.value;
		var description = event.target.description.value;
		var why = event.target.why.value;
		var studyGuide = event.target.studyGuide.value;
		var themeId = event.target.themeId.value;
		var masteryRequiredForGrade = event.target.masteryRequiredForGrade.value;
		// convert to number if possible
		// so that sorting in MongoDB will work properly
		if (masteryRequiredForGrade!==undefined && masteryRequiredForGrade!=="") {
			masteryRequiredForGrade = isNaN(masteryRequiredForGrade) ? masteryRequiredForGrade : parseFloat(masteryRequiredForGrade);
		}
		//console.log(".form submitted: set skill readiness. topicId: "+topicId);
		Meteor.call('updateTopic', topicId, title, description, why, studyGuide, themeId, masteryRequiredForGrade, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("topicPage", {courseId: courseId, topicId: topicId});
			}
		});
	},
	'click .add-skill-button': function(ev, template) {
		var courseId = this.course._id;
		var topicId = this.topic._id;
		//console.log(".add-skill-button clicked");
		Meteor.call('addSkill', topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("skillEditPage", {courseId: courseId, skillId: result});
			}
		});
	},
	'click .remove-skill-button': function(ev, template) {
		var skillId = this._id;
		//console.log(".remove-skill-button clicked");
		Meteor.call('removeSkill', skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .add-study-task-button': function(ev, template) {
		var courseId = this.course._id;
		var topicId = this.topic._id;
		//console.log(".add-study-task-button clicked");
		Meteor.call('addStudyTask', topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("studyTaskEditPage", {courseId: courseId, studyTaskId: result});
			}
		});
	},
	'click .remove-study-task-button': function(ev, template) {
		var studyTaskId = this._id;
		//console.log(".remove-study-task-button clicked");
		Meteor.call('removeStudyTask', studyTaskId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
