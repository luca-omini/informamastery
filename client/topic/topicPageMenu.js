Template.topicPageMenu.helpers({
	countAllStudentsPassedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: true}).count();
	},
	countAllStudentsFailedTopicChecksForTopic: function(topicId) {
		return TopicChecks.find({topicId: topicId, mastered: false}).count();
	},
	countAllStudentsQuestionsForTopic: function(topicId) {
		var skillIds = Skills.find({topicId: topicId}).map(function(skill) {
			return skill._id;
		});
		return Annotations.find({skillId: {$in: skillIds}, kind: "question", removed: {$ne: true}}).count();
	},
	countUnsolvedPracticeProblems: function(topicId) {
		return _findUnsolvedPracticeProblems(topicId).count();
	},
	countSolvedPracticeProblems: function(topicId) {
		return PracticeProblemSolutions.find({topicId: topicId, userId: Meteor.userId()}).count();
	},
	countOwnPublishedUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), published: true, archived: false}).count();
	},
	countOwnUnarchivedPracticeProblems: function(topicId) {
		return PracticeProblems.find({topicId: topicId, userId: Meteor.userId(), archived: false}).count();
	},
});
