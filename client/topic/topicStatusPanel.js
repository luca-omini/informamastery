Template.topicStatusPanel.events({
  'click .show-button': function(event, template) {
		console.log("click .show-button");
		var topicId = this.topic._id;
		Meteor.call("showTopic", topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .hide-button': function(event, template) {
		console.log("click .hide-button");
		var topicId = this.topic._id;
		Meteor.call("hideTopic", topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
