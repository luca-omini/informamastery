Template.topicPrint.helpers({
	skills: function(topicId) {
		return Skills.find({topicId: topicId});
	},
	studyTasks: function(topicId) {
		return StudyTasks.find({topicId: topicId});
	},
});
