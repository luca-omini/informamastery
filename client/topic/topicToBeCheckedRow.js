Template.topicToBeCheckedRow.events({
	'click .remove-topic-to-check-button': function(ev, template) {
		var topicId = this._id;
		//console.log(".remove-topic-to-check-button clicked. Topic's _id: "+topicId);
		Meteor.call('removeTopicToBeChecked', topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
