Template.registerStudentButtonCell.helpers({
  getStudentRegistration: function(studentId, courseId) {
    console.log("Template.registerStudentButtonCell.helpers: getStudentRegistration(",studentId, courseId, ")");
		return StudentRegistrations.findOne({courseId: courseId, studentId: studentId});
	},
});

Template.registerStudentButtonCell.events({
	'click .register-student-button': function(event, template) {
    console.log("Template.registerStudentButtonCell.events: click .register-student-button");
    console.log(template);
    console.log(template.data);
    var userId = template.data._id;
    var courseId = Session.get("course")._id;
    console.log("courseId:", courseId);
    console.log("userId:", userId);
    Meteor.call('registerAndApproveUserAsStudent', userId, courseId, function(error, studentRegistrationId) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'click .unregister-student-button': function(event, template) {
    console.log("Template.registerStudentButtonCell.events: click .unregister-student-button");
    console.log(template);
    console.log(template.data);
    var userId = template.data._id;
    var courseId = Session.get("course")._id;
    console.log("courseId:", courseId);
    console.log("userId:", userId);
  },
});
