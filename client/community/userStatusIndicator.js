Template.userStatusIndicator.helpers({
	getMinutesLeftInOngoingPomodoro: function(user) {
		if (user.status.pomodoroStartTime) {
			var startMoment = moment(user.status.pomodoroStartTime);
			var endMoment = moment(TimeSync.serverTime(null, 1000)); // update every 1 seconds
			var durationInMinutes = endMoment.diff(startMoment, 'minutes');
			var minutesLeft = _pomodoroDuration - durationInMinutes;
			return minutesLeft;
		} else {
			return "?";
		}
	},
});
