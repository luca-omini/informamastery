Tracker.autorun(function() {
	if (Meteor.userId()) {
		try {
			console.log("Tracker.autorun(....) in userStatusBusyTracker.js -- Meteor.userId() changed? Calling UserStatus.startMonitor()");
			UserStatus.startMonitor({
				threshold: 120000, // after 2 minutes without mouse click or key press, user is considered idle
				interval: 5000,
				idleOnBlur: false});
		} catch (err) {
			console.log("Tracker.autorun(....) in userStatusBusyTracker.js -- UserStatus.startMonitor() threw exception:");
			console.log(err);
			console.log("Moving on.");
		}
	} else {
		try {
			console.log("Tracker.autorun(....) in userStatusBusyTracker.js -- Meteor.userId() changed? Calling UserStatus.stopMonitor()");
			UserStatus.stopMonitor();
		} catch (err) {
			console.log("Tracker.autorun(....) in userStatusBusyTracker.js -- UserStatus.stopMonitor() threw exception:");
			console.log(err);
			console.log("Moving on.");
		}
	}
});
