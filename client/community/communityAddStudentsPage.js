Template.communityAddStudentsPage.created = function () {
	// ReactiveVar to hold the bulk create student data
	this.students = new ReactiveVar([]);
};


Template.communityAddStudentsPage.helpers({
  getStudentCount: function(courseId) {
		return StudentRegistrations.find({courseId: courseId, withdrawn: false}).count();
	},
	getStudents: function() {
		console.log("Template.communityAddStudentsPage.helpers.getStudents()");
		// retrieve the bulk create student data from the ReactiveVar set below
		var students = Template.instance().students.get();
		console.log(students);
		return students;
	},
  findUsers: function(courseId) {
    //var userIds = StudentRegistrations.find({courseId: courseId}).map(function(studentRegistration) {return studentRegistration.studentId;});
		//return Meteor.users.find({_id: {$nin: userIds}});
    return Meteor.users.find({});
	},
	makeSettings: function(courseId) {
		return {
			rowsPerPage: 5,
			class: "ui compact table",
			fields: [
				{
					key: "gravatar",
					label: "Photo",
					sortable: false,
					tmpl: Template.userGravatar,
					headerClass: "collapsing",
				},
				{
					key: "profile.firstName",
					label: "First",
					sort: "ascending",
					headerClass: "collapsing",
				},
				{
					key: "profile.lastName",
					label: "Last",
					headerClass: "collapsing",
				},
				{
					key: "primaryEmail",
					label: "Primary Email",
					tmpl: Template.userPrimaryMail,
					fn: function(value, object) {return object.emails[0].address;},
				},
        {
          key: "register",
          label: "Register",
          headerClass: "right aligned",
          cellClass: "right aligned",
          tmpl: Template.registerStudentButtonCell,
        }
			],
		};
	},
});


Template.communityAddStudentsPage.events({
	'submit .bulk-register-students-form': function(event, template) {
		var courseId = this._id;
		var text = event.target.students.value;

		console.log("submit .bulk-register-students-form");
		console.log(text);

		var students = [];
		try {
			var lines = text.split("\n");
			_.each(lines, function(element, index, list) {
				var line = element.trim();
				console.log("LINE: '"+line+"'");
				var studentRegEx = /^([^,]+),([^,]+),([^,]*)$/;
				if (studentRegEx.test(line)) {
					var matches = studentRegEx.exec(line);
					var first = matches[1].trim();
          var last = matches[2].trim();
          var email = matches[3].trim().toLowerCase();
          var user = Meteor.users.findOne({"emails.address": email});
					console.log("user:", user);
					// note that the following is case sensitive; if they differ by case, we won't find them
					var query = {"profile.firstName": first, "profile.lastName": last};
					if (user) {
						query._id = {$ne: user._id};
					}
					console.log("query:", query);
					var otherUsersWithSameName = Meteor.users.find(query).fetch();
					console.log("otherUsersWithSameName:", otherUsersWithSameName);
          var registration;
          if (user) {
            registration = StudentRegistrations.findOne({courseId: courseId, studentId: user._id});
          }
					console.log("registration:", registration);
					console.log("STUDENT '"+first+"', '"+last+"', '"+email+"'");
					var student = {
						first: first,
            last: last,
            email: email,
            user: user,
						otherUsersWithSameName: otherUsersWithSameName,
            registration: registration,
					};
          students.push(student);
				}
			});
			console.log(students);
		} catch (error) {
			console.log(error);
		}
		// store the bulk create student data in the ReactiveVar retrieved above
		template.students.set(students);
		// Prevent default form submit
		return false;
	},
});
