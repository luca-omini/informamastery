var sortableOptions = {
  connectWith: ".students",
  stop: function(e, ui) {
    console.log("stop.");
    //console.log("ui=", ui, "e=", e);
    var destinationBuddyStudents = ui.item.closest('.students.buddy-students').get(0);

    var studentRegistration = Blaze.getData(ui.item.get(0));
    console.log("StudentRegistration:", studentRegistration);
    var studentId = studentRegistration.studentId;
    if (destinationBuddyStudents) {
      console.log("Dropped on buddy students:", destinationBuddyStudents);
      Meteor.call('addBuddyPreference', studentRegistration.courseId, Meteor.userId(), studentId, function(error, result) {
        if (error) {
          throwError(error.reason);
        }
      });
    } else {
      console.log("Dropped outside buddy students");
      Meteor.call('removeBuddyPreference', studentRegistration.courseId, Meteor.userId(), studentId, function(error, result) {
        if (error) {
          throwError(error.reason);
        }
			});
    }
  }
}

Template.communityPage.rendered = function() {
  var self = this;
  console.log("Template.communityPage.rendered");
  console.log("this", this);
  //console.log("this.$('.team')", this.$('.team'));
  //console.log("$('.team')", $('.team'));
  this.$('.students').sortable(sortableOptions);
};

Template.communityPage.helpers({
	getBuddyPreferences: function(courseId) {
		console.log("Template.communityPage.helpers:getBuddyPreferences("+courseId+")");
		return BuddyPreferences.find({courseId: courseId, userId: Meteor.userId(), active: true});
	},
	getNotApprovedNotWithdrawnStudentUsersWhoAreNotMyBuddies: function(courseId) {
		console.log("Template.communityPage.helpers:getNotApprovedNotWithdrawnStudentUsersWhoAreNotMyBuddies("+courseId+")");
		var buddyIds = BuddyPreferences.find({courseId: courseId, userId: Meteor.userId(), active: true}).map(function(buddyPreference) {return buddyPreference.buddyId});
		console.log("buddyIds:", buddyIds);
		var excludeIds = _.union(buddyIds, [Meteor.userId()]);
		console.log("excludeIds:", excludeIds);
		var studentIds = StudentRegistrations.find({studentId: {$nin: excludeIds}, courseId: courseId, approved: {$ne: true}, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
		console.log("studentIds:", studentIds);
		return Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
	},
	getApprovedNotWithdrawnStudentUsersWhoAreNotMyBuddies: function(courseId) {
		console.log("Template.communityPage.helpers:getApprovedNotWithdrawnStudentUsersWhoAreNotMyBuddies("+courseId+")");
		var buddyIds = BuddyPreferences.find({courseId: courseId, userId: Meteor.userId(), active: true}).map(function(buddyPreference) {return buddyPreference.buddyId});
		console.log("buddyIds:", buddyIds);
		var excludeIds = _.union(buddyIds, [Meteor.userId()]);
		console.log("excludeIds:", excludeIds);
		var studentIds = StudentRegistrations.find({studentId: {$nin: excludeIds}, courseId: courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
		console.log("studentIds:", studentIds);
		return Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
	},
	getStudentCount: function(courseId) {
		return StudentRegistrations.find({courseId: courseId, withdrawn: false}).count();
	},
	getTeachingTeamUsers: function(courseId) {
		var assistantIds = AssistantRegistrations.find({courseId: courseId, withdrawn: {$ne: true}}).map(function(assistantRegistration) {return assistantRegistration.assistantId;});
		var instructorIds = InstructorRegistrations.find({courseId: courseId}).map(function(instructorRegistration) {return instructorRegistration.instructorId;});
		var userIds = _.union(assistantIds, instructorIds);
		return Meteor.users.find({_id: {$in: userIds}}, {sort: {"profile.firstName": 1}});
	},
  getWithdrawnTeachingTeamUsers: function(courseId) {
		var assistantIds = AssistantRegistrations.find({courseId: courseId, withdrawn: true}).map(function(assistantRegistration) {return assistantRegistration.assistantId;});
		var instructorIds = InstructorRegistrations.find({courseId: courseId, withdrawn: true}).map(function(instructorRegistration) {return instructorRegistration.instructorId;});
		var userIds = _.union(assistantIds, instructorIds);
		return Meteor.users.find({_id: {$in: userIds}}, {sort: {"profile.firstName": 1}});
	},
  countWithdrawnTeachingTeamUsers: function(courseId) {
		var assistantIds = AssistantRegistrations.find({courseId: courseId, withdrawn: true}).map(function(assistantRegistration) {return assistantRegistration.assistantId;});
		var instructorIds = InstructorRegistrations.find({courseId: courseId, withdrawn: true}).map(function(instructorRegistration) {return instructorRegistration.instructorId;});
		var userIds = _.union(assistantIds, instructorIds);
		return Meteor.users.find({_id: {$in: userIds}}, {sort: {"profile.firstName": 1}}).count();
	},
});
