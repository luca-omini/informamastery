Template.communityStudentPhotoCard.helpers({
  getPhoto: function(studentRegistrationId) {
    console.log("Template.communityStudentPhotoCard.helpers:getPhoto("+studentRegistrationId+")");
		return StudentPhotoFiles.findOne({"metadata.studentRegistrationId": studentRegistrationId});
	},
});
