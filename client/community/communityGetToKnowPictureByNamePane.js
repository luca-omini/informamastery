Template.communityGetToKnowPictureByNamePane.helpers({
  getPhotoLink: function(userId) {
    console.log("Template.communityGetToKnowPictureByNamePane.helpers:getPhotoLink("+userId+")");
		var photo = StudentPhotoFiles.findOne({"metadata.userId": userId});
    if (photo) {
      return StudentPhotoFiles.baseURL + "/md5/" + photo.md5;
    } else {
      return undefined;
    }
	},
});
