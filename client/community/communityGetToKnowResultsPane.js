Template.communityGetToKnowResultsPane.helpers({
  getStudentRegistrations: function(courseId) {
		return StudentRegistrations.find({courseId: courseId, withdrawn: false});
	},
  getPhoto: function(studentRegistrationId) {
    console.log("Template.communityGetToKnowResultsPane.helpers:getPhoto("+studentRegistrationId+")");
		return StudentPhotoFiles.findOne({"metadata.studentRegistrationId": studentRegistrationId});
	},
  getPhotoLink: function() {
    return StudentPhotoFiles.baseURL + "/md5/" + this.md5;
  },
  getKnowledgeForUser: function(userId) {
    var knowledge = Meteor.user().communityKnowledge;
    if (knowledge) {
      return knowledge[userId];
    }
    return undefined;
  },
  undefinedToZero: function(number) {
    return !number?0:number;
  },
  add: function(a, b) {
    if (!a && !b) {
      return 0;
    } else if (!a) {
      return b;
    } else if (!b) {
      return a;
    } else {
      return a+b;
    }
  },
});
