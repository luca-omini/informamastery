Template.jointJsTest.onRendered(function () {
  console.log("Template.jointJsTest.onRendered");


  // to work around a bug in old joint.js version used:
  // see: https://github.com/clientIO/joint/issues/203
  // that's because https://atmospherejs.com/mxmxmx/jointjs-all
  // uses Joint.js 0.9.3 (current is Joint.js 0.9.6)
  //SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(toElement) {
  //  return toElement.getScreenCTM().inverse().multiply(this.getScreenCTM());
  //};


  // Create custom shapes
  joint.shapes.informa = {};
  joint.shapes.informa.Object = joint.shapes.basic.Generic.extend({
    markup: '<g class="rotatable"><g class="scalable"><rect/><line/></g><text/></g>',
    defaults: joint.util.deepSupplement({
        type: 'informa.Object',
        attrs: {
            'rect': { fill: 'hsl(0,80%,90%)', stroke: 'hsl(0,80%,60%)', rx: 10, ry: 10, 'stroke-width': 3, 'follow-scale': true, width: 80, height: 40 },
            'line': { stroke: 'blue', 'stroke-width': 3, 'follow-scale': true, ref: 'rect', 'ref-x': 0, 'ref-y': .1 },
            'text': { fill: 'hsl(0,80%,60%)', 'font-size': 14, 'font-weight': 'bold', 'ref-x': .5, 'ref-y': .5, ref: 'rect', 'y-alignment': 'middle', 'x-alignment': 'middle' }
        }
    }, joint.shapes.basic.Generic.prototype.defaults)
  });
  joint.shapes.informa.DiamondRotated = joint.shapes.basic.Generic.extend({
      markup: '<g class="rotatable"><g class="scalable"><rect/></g><text/></g>',
      defaults: joint.util.deepSupplement({
          type: 'informa.DiamondRotated',
          attrs: {
              'rect': { fill: '#FFFFFF', stroke: 'black', width: 1, height: 1, transform: 'rotate(45)' },
              'text': { 'font-size': 14, text: '', 'ref-x': .5, 'ref-y': .5, ref: 'rect', 'y-alignment': 'middle', 'x-alignment': 'middle', fill: 'black' }
          }
      }, joint.shapes.basic.Generic.prototype.defaults)
  });



  // custom shape with ports
  joint.shapes.informa.CustomElement = joint.shapes.basic.Generic.extend(_.extend({}, joint.shapes.basic.PortsModelInterface, {
    markup: '<g class="rotatable"><g class="scalable"><rect class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/></g><g class="element-tools"></g>',
    portMarkup: '<g class="port port<%= id %>"><circle class="port-body"/><text class="port-label"/></g>',
    toolMarkup: [
      '<g class="element-tool-remove"><circle fill="red" r="11"/>',
      '<path transform="scale(.8) translate(-16, -16)" d="M24.778,21.419 19.276,15.917 24.777,10.415 21.949,7.585 16.447,13.087 10.945,7.585 8.117,10.415 13.618,15.917 8.116,21.419 10.946,24.248 16.447,18.746 21.948,24.248z"/>',
      '<title>Remove this element</title>',
      '</g>'
    ].join(''),
    defaults: joint.util.deepSupplement({
      type: 'informa.CustomElement',
      size: { width: 1, height: 1 },
      inPorts: [],
      outPorts: [],
      attrs: {
          '.': {
            magnet: false,
          },
          '.body': {
            width: 150,
            height: 250,
          },
          '.port-body': {
            r: 6,
          },
          '.outPorts .port-body': {
            magnet: true,
            type: "output",
          },
          '.inPorts .port-body': {
            magnet: "passive",
            type: "input",
          },
          text: { // what's this about?
            'pointer-events': 'none',
          },
          '.label': {
            text: 'Informa',
            'ref-x': .5,
            'ref-y': 10,
            ref: '.body',
            'text-anchor': 'middle',
          },
          '.inPorts .port-label': {
            //x: -4,
            //dy: -30,
            //'text-anchor': 'end',
            x: 0,
            y: 0,
            dx: 0,
            dy: '-0.4em',
            'text-anchor': 'middle',
            //'alignment-baseline': 'middle',
          },
          '.outPorts .port-label': {
            //x: 4,
            //dy: 15,
            x: 0,
            y: 0,
            dx: 0,
            dy: '-0.4em',
            'text-anchor': 'middle',
            //'alignment-baseline': 'middle',
          }
      }
    }, joint.shapes.basic.Generic.prototype.defaults),
    getPortAttrs: function(portName, index, total, selector, type) {
      var attrs = {};
      var portClass = 'port' + index;
      var portSelector = selector + '>.' + portClass;
      var portLabelSelector = portSelector + '>.port-label';
      var portBodySelector = portSelector + '>.port-body';
      attrs[portLabelSelector] = {
        text: portName=='fallthrough'?"":portName,
      };
      attrs[portBodySelector] = {
        port: {
          id: portName || _.uniqueId(type),
          type: type
        }
      };
      attrs[portSelector] = {
        ref: '.body',
        'ref-x': (index + 0.5) * (1 / total)
      };
      if (selector === '.outPorts') {
        attrs[portSelector]['ref-dy'] = 0;
      }
      return attrs;
    }
  }));
  //joint.shapes.informa.CustomElementView = joint.dia.ElementView.extend(joint.shapes.basic.PortsViewInterface);
  joint.shapes.informa.CustomElementView = joint.dia.ElementView.extend({
    // added extra functionality for port support taken from joint.shapes.basic.PortsViewInterface
    // added extra functionality for tool support
    initialize: function() {
        // `Model` emits the `process:ports` whenever it's done configuring the `attrs` object for ports.
        this.listenTo(this.model, 'process:ports', this.update);
        // call super()
        joint.dia.ElementView.prototype.initialize.apply(this, arguments);
    },
    update: function() {
        // First render ports so that `attrs` can be applied to those newly created DOM elements
        // in `ElementView.prototype.update()`.
        this.renderPorts();
        // INJECT THIS CALL HERE? (example called it from render())
        this.renderTools();
        // call super()
        joint.dia.ElementView.prototype.update.apply(this, arguments);
    },
    renderPorts: function() {
      console.log("renderPorts()");
      var $inPorts = this.$('.inPorts').empty();
      var $outPorts = this.$('.outPorts').empty();
      var portTemplate = _.template(this.model.portMarkup);
      _.each(_.filter(this.model.ports, function(p) { return p.type === 'in' }), function(port, index) {
          $inPorts.append(V(portTemplate({ id: index, port: port })).node);
      });
      _.each(_.filter(this.model.ports, function(p) { return p.type === 'out' }), function(port, index) {
          $outPorts.append(V(portTemplate({ id: index, port: port })).node);
      });
    },
    // tool support
    renderTools: function () {
      console.log("renderTools()");
      var $tools = this.$('.element-tools').empty();
      console.log("$tools=", $tools);
      var toolMarkup = this.model.toolMarkup || this.model.get('toolMarkup');
      console.log("toolMarkup=", toolMarkup);
      if (toolMarkup) {
        $tools.append(V(toolMarkup).node);
      }
    },
    // tool support
    pointerclick: function (ev, x, y) {
      var className = ev.target.parentNode.getAttribute('class');
      switch (className) {
        case 'element-tool-remove':
          this.model.remove();
          return;
      }
      // call super()
      joint.dia.ElementView.prototype.pointerclick.apply(this, arguments);
    },
  });

  joint.shapes.informa.CustomLink = joint.dia.Link.extend({
    defaults: {
      type: 'informa.CustomLink',
      router: {
        name: 'manhattan',
        args: {
          startDirections: ["bottom"],
          endDirections: ["top"],
        },
      },
      connector: {
        name: 'rounded',
      },
      attrs: {
        '.connection': {
          'stroke-width': 3,
        },
        '.marker-target': {
          stroke: 'black',
          fill: 'black',
          d: 'M 10 0 L 0 5 L 10 10 z',
        },
      },
      labels: [
         {
           position: .5,
           attrs: {
             text: {
               text: '?',
               fill: 'white',
               'font-size': '7pt',
             },
             rect: {
               stroke: 'black',
               fill: 'black',
               'stroke-width': 12,
               rx: 3,
               ry: 3,
             },
           },
         }
      ],
    }
  });
  joint.shapes.informa.FallThroughLink = joint.dia.Link.extend({
    defaults: {
      type: 'informa.FallThroughLink',
      connector: {
        name: 'rounded',
      },
      attrs: {
        '.connection': {
          'stroke-width': 3,
        },
        '.marker-target': {
          stroke: 'black',
          fill: 'black',
          d: 'M 10 0 L 0 5 L 10 10 z',
        },
      },
      labels: [],
    }
  });


  // Paper from which you take shapes
  var stencilGraph = new joint.dia.Graph;
  var stencilPaper = new joint.dia.Paper({
    el: $('.stencil'),
    height: 50,
    model: stencilGraph,
    defaultLink: function(cellView, magnet) {
      return null;
    },
    interactive: false
  });

  /*
  var stencil1 = new joint.shapes.basic.Rect({
    position: { x: 10, y: 10 },
    size: { width: 100, height: 30 },
    attrs: {
      rect: { fill: 'hsl(200, 100%, 40%)', stroke: 'hsl(200, 100%, 30%)' },
      text: { text: 'Statement', fill: 'white' }
    }
  });
  */
  /*
  var stencil2 = new joint.shapes.basic.Rect({
    position: { x: 120, y: 10 },
    size: { width: 100, height: 30 },
    attrs: {
      rect: { fill: 'hsl(100, 100%, 40%)', stroke: 'hsl(100, 100%, 30%)' },
      text: { text: 'Condition', fill: 'white' }
    }
  });
  */
  /*
  var stencil3 = new joint.shapes.informa.DiamondRotated({
    position: { x: 240, y: 10 },
    size: { width: 100, height: 100 },
    attrs: {
      text: { text: '3', fill: 'red' }
    }
  });
  */
  /*
  // NOTE: This is the way we want to create a diamond (branch/condition) shape
  var stencil4 = new joint.shapes.basic.Path({
    position: { x: 120, y: 10 },
    size: { width: 100, height: 30 },
    attrs: {
      path: { d: 'M 30 0 L 60 30 30 60 0 30 z', fill: 'hsl(100, 100%, 40%)', stroke: 'hsl(100, 100%, 30%)' },
      text: {
          text: 'Diamond', fill: 'white',
          'ref-y': .5 // basic.Path text is originally positioned under the element
      }
    }
  });
  */
  /*
  var stencil5 = new joint.shapes.basic.Path({
    position: { x: 480, y: 10 },
    size: { width: 100, height: 30 },
    attrs: {
      path: { d: 'M 50 0 L 0 20 0 80 50 100 100 80 100 20 z'},
      text: {
          text: 'Hexagon',
          'ref-y': .5 // basic.Path text is originally positioned under the element
      }
    }
  });
  var stencil6 = new joint.shapes.basic.Path ({
    position: { x: 600, y: 10 },
    size: { width: 100, height: 30 },
    attrs: {
      path: { d: 'M 25 0 L 175 0 200 25 200 50 200 75 175 100 25 100 0 75 0 25 z' },
      text: {
        text: 'Octagon',
        'ref-y': .5
      }
    }
  });
  */
  var stencil7 = new joint.shapes.informa.CustomElement({
    position: { x: 20, y: 10 },
    size: { width: 100, height: 30 },
    inPorts: ["in"],
    outPorts: ["fallthrough"],
    attrs: {
      '.label': { text: 'instruction' },
    }
  });
  var stencil8 = new joint.shapes.informa.CustomElement({
    position: { x: 140, y: 10 },
    size: { width: 100, height: 30 },
    inPorts: ["in"],
    outPorts: ["f", "t"],
    attrs: {
      '.body': { fill: 'hsl(200, 100%, 40%)' },
      '.port-body': { fill: 'hsl(200, 100%, 40%)' },
      '.label': { text: 'branch' },
    }
  });
  stencilGraph.addCells([stencil7, stencil8]);

  // Paper where you create diagram
  var graph = new joint.dia.Graph;
  var paper = new joint.dia.Paper({
    el: this.$('.paper'),
    width: 800,
    height: 400,
    model: graph,
    gridSize: 10,
    //linkConnectionPoint: joint.util.shapePerimeterConnectionPoint, // may be slow
    defaultLink: function(cellView, magnet) {
      console.log("defaultLink(cellView=", cellView, "magnet=", magnet, ")");
      console.log(magnet.getAttribute('port'));
      var link;
      if (magnet.getAttribute('port')==='fallthrough') {
        link = new joint.shapes.informa.FallThroughLink();
        //link.set('labels', []); // remove label
        //link.unset('router'); // no router, straight line
      } else {
        link = new joint.shapes.informa.CustomLink();
        link.label(0, {
          attrs: {
            text: { text: magnet.getAttribute('port') },
          }
        });
      }
      return link;
    },
    validateConnection: function(cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
      console.log("validateConnection(cellViewS=", cellViewS, "magnetS=", magnetS, "cellViewT=", cellViewT, "magnetT=", magnetT, "end=", end, "linkView=", linkView, ")");
      var valid;
      // Prevent linking from input ports.
      if (magnetS && magnetS.getAttribute('type') === 'input') {
        valid = false;
      } else {
        // Prevent linking from output ports to input ports within one element.
        //if (cellViewS === cellViewT) return false;
        // Prevent linking to input ports.
        valid = magnetT && magnetT.getAttribute('type') === 'input';
      }
      console.log("=> valid="+valid);
      return valid;
    },
    // Enable marking available cells & magnets
    markAvailable: true,
  });

  // for debugging (access from browser console)
  HRM = {};
  HRM.paper = paper;

  /*
  var rect = new joint.shapes.basic.Rect({
    position: { x: 100, y: 30 },
    size: { width: 100, height: 30 },
    attrs: {
      rect: { fill: 'hsl(200, 100%, 40%)', stroke: 'hsl(200, 100%, 30%)' },
      text: { text: 'my box', fill: 'white' }
    }
  });

  var rect2 = rect.clone();
  rect2.translate(200);

  var obj = new joint.shapes.informa.Object({
    position: { x: 400, y: 30 },
    size: { width: 150, height: 80 },
    attrs: {
      'text': { text: 'MyClass' }
    }
  });

  var link = new joint.dia.Link({
    source: { id: rect.id },
    target: { id: rect2.id },
    //smooth: true,
    router: { name: 'manhattan', args: { startDirections: ["bottom"], endDirections: ["top"] } },
    connector: { name: 'rounded' },
    attrs: {
      '.connection': { stroke: 'hsl(200, 100%, 20%)', 'stroke-width': 3 },
      '.marker-target': { fill: 'hsl(200, 100%, 20%)', d: 'M 10 0 L 0 5 L 10 10 z' }
    }
  });

  graph.addCells([rect, rect2, link, obj]);
  */

  console.log("graph (model): ", graph);
  console.log("paper (view):", paper);

  paper.on('blank:pointerdown', function(e, x, y) {
    // remove prior text-editors
    $('.text-editor').remove();
  });

  paper.on('cell:pointerclick', function(cellView, e, x, y) {
    console.log("paper.on('cell:pointerclick', function(cellView,e,x,y)): ", cellView, e, x, y);
    console.log("text/text: "+cellView.model.attr('text/text'));
    console.log(".label/text: "+cellView.model.attr('.label/text'));
    // remove prior text-editors
    $('.text-editor').remove();
    // create a new text-editor
    $('.paper').append('<input type="text" class="text-editor"/>');
    // compute bounding box of clicked cellView
    var bbox = cellView.model.getBBox();
    console.log("BBox: "+bbox);
    // place text-editor on top of cellView
    $('.text-editor').css({
      position: "absolute",
      "z-index": 2,
      width: bbox.width,
      height: bbox.height,
      left: bbox.x,
      top: bbox.y,
      transform: 'rotate(' + (cellView.model.get('angle') || 0) + 'deg)',
      border: 'none'
    });
    // set text-editor's text to model text
    $('.text-editor').val(cellView.model.attr('.label/text'));
    $('.text-editor').focus();
    $('.text-editor').select();
    // on change, apply change, and remove text-editor
    $('.text-editor').on('change', function(e) {
      console.log('.text-editor on change');
      cellView.model.attr('.label/text', $(e.target).val());
      $(e.target).remove();
    });
    // on blur, remove text-editor
    $('.text-editor').on('blur', function(e) {
      console.log('.text-editor on blur');
      $(e.target).remove();
    });
    // prevent paper from seeing clicks in the text-editor
    $('.text-editor').on('mousedown click', function(e) {
      console.log('.text-editor on mousedown click');
      e.stopPropagation();
    });
  });


  // Code for drag-and-drop from stencilPaper to paper
  stencilPaper.on('cell:pointerdown', function(cellView, e, x, y) {
    console.log("stencilPaper.on('cell:pointerdown', ...), cellView=", cellView, ", e=", e, "x=", x, "y=", y);
    $('body').append('<div class="flyPaper" style="position:fixed;z-index:100;opacity:.7;pointer-event:none;"></div>');
    var flyGraph = new joint.dia.Graph;
    var flyPaper = new joint.dia.Paper({
      el: $('.flyPaper'),
      model: flyGraph,
      interactive: false
    }),
    flyShape = cellView.model.clone();
    // position of stencil on stencilPaper
    pos = cellView.model.position();
    console.log("pos=", pos);
    offset = {
      x: x - pos.x,
      y: y - pos.y
    };
    console.log("offset=", offset);

    flyShape.position(0, 0);
    flyGraph.addCell(flyShape);
    $(".flyPaper").offset({
      left: e.pageX - offset.x,
      top: e.pageY - offset.y
    });
    console.log("$('.flyPaper').offset()=", $('.flyPaper').offset());
    $('body').on('mousemove.fly', function(e) {
      console.log("$('body').on('mousemove.fly', ...)");
      $(".flyPaper").offset({
        left: e.pageX - offset.x,
        top: e.pageY - offset.y
      });
    });
    $('body').on('mouseup.fly', function(e) {
      console.log("$('body').on('mouseup.fly', ...)");
      var x = e.pageX;
      var y = e.pageY;
      var target = paper.$el.offset();
      // Dropped over paper ?
      if (x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) {
        var s = flyShape.clone();
        //s.position(x - target.left - offset.x, y - target.top - offset.y);
        var newSx = x - target.left - offset.x;
        var newSy = y - target.top - offset.y;
        var gridSize = paper.options.gridSize;
        newSx = gridSize * Math.floor(newSx / gridSize);
        newSy = gridSize * Math.floor(newSy / gridSize);
        s.position(newSx, newSy);
        graph.addCell(s);
      }
      $('body').off('mousemove.fly').off('mouseup.fly');
      flyShape.remove();
      $('.flyPaper').remove();
    });
  });
});

Template.jointJsTest.events({
  'click .save-button': function (event, template) {
    console.log("click .save-button", event, template);
    console.log("JSON:", HRM.paper.model.toJSON());
  },
});
