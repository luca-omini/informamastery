Template.studyTaskBookSectionItem.rendered = function() {
	var template = this;
	console.log("Template.studyTaskBookSectionItem.rendered()");

	var context = Template.currentData();
	//console.log("context:", context);
	var sectionId = context._id;
	//console.log("sectionId:", sectionId);

	var parentContext = Template.parentData(1);
	//console.log("parentContext:", parentContext);
	var sectionIds = parentContext.studyTask && parentContext.studyTask.detail ? parentContext.studyTask.detail.sectionIds : [];
	//console.log("sectionIds:", sectionIds);

	template.$('.section-checkbox').checkbox({});
	if (_.contains(sectionIds, sectionId)) {
		//console.log("CHECK");
		template.$('.section-checkbox').checkbox('check');
	}
};
