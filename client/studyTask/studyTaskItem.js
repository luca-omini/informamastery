Template.studyTaskItem.helpers({
	studyTaskCompleted: function(studyTaskId) {
		var studyTaskStatus = StudyTaskStatus.findOne({studyTaskId: studyTaskId, userId: Meteor.userId()});
		if (studyTaskStatus && studyTaskStatus.recallText) {
			return true;
		} else {
			return false;
		}
	},
	getSumOfDurationsOfRemainingSections: function(sectionIds) {
		console.log("Template.studyTaskItem.helpers.getSumOfDurationsOfRemainingSections("+sectionIds+")");
		return sectionIds?getSumOfDurationsOfRemainingSections(sectionIds):undefined;
	},
	bookStudyTaskCompleted: function(studyTaskId) {
		console.log("bookStudyTaskCompleted("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			console.log("sectionIds: ", sectionIds);
			var allRecalled = true;
			_.each(sectionIds, function(sectionId) {
				var sectionStatus = SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()});
				if (sectionStatus && sectionStatus.recallText) {
					// recalled
				} else {
					// not yet recalled
					allRecalled = false;
				}
			});
			return allRecalled;
		} else {
			return false;
		}
	},
	countStudyTaskSections: function(studyTaskId) {
		console.log("countStudyTaskSections("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			return studyTask.detail.sectionIds.length;
		} else {
			return 0;
		}
	},
	countOwnRecalledStudyTaskSections: function(studyTaskId) {
		console.log("countOwnRecalledStudyTaskSections("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			var recalledCount = 0;
			SectionStatus.find({sectionId: {$in: sectionIds}, userId: Meteor.userId()}).forEach(function(sectionStatus) {
				if (sectionStatus && sectionStatus.recallText) {
					// recalled
					recalledCount++;
				}
			});
			return recalledCount;
		} else {
			return 0;
		}
	},
	countOwnStudyTaskSectionStatuses: function(studyTaskId) {
		console.log("countOwnStudyTaskSectionStatuses("+studyTaskId+")");
		var studyTask = StudyTasks.findOne({_id: studyTaskId});
		if (studyTask && studyTask.detail) {
			var sectionIds = studyTask.detail.sectionIds;
			return SectionStatus.find({sectionId: {$in: sectionIds}, userId: Meteor.userId()}).count();
		} else {
			return 0;
		}
	},
});
