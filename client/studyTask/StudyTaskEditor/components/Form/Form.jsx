import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setFormDescr,
  setFormDuration,
  setFormShortDesc,
  setFormTitle,
  setTimeoutRunner,
} from '../../../../../lib/redux/action';
import Input from '../Input/Input';
import InputLabeled from '../Input/InputLabeled';
import InputTextArea from '../Input/InputTextArea';
import InputFile from '../Input/InputFile';

const Form = () => {
  const dispatch = useDispatch();
  const studyTask = useSelector((state) => state.studyTaskEdit);
  const [title, setTitle] = useState(studyTask.title);
  const [description, setDescription] = useState(studyTask.description);
  const [duration, setDuration] = useState(studyTask.duration);
  const [body, setBody] = useState(studyTask.body);
  const [timeout, setTimeout] = useState(studyTask.timeout);
  const images = [
    '\'registry.gitlab.com/openpatch/runner-java:2-extract-class-method-and-line-from-junit-output\'',
    '\'registry.gitlab.com/openpatch/runner-java\'',
  ].sort();

  useEffect(() => {
    setTitle(studyTask.title);
  }, [studyTask.title]);

  useEffect(() => {
    setDescription(studyTask.description);
  }, [studyTask.description]);

  useEffect(() => {
    setDuration(studyTask.duration);
  }, [studyTask.duration]);

  useEffect(() => {
    setBody(studyTask.body);
  }, [studyTask.body]);

  useEffect(() => {
    setTimeout(studyTask.timeout);
  }, [studyTask.timeout]);

  const fileUpload = async (ev) => {
    ev.preventDefault();
    const reader = new FileReader();
    reader.onload = async (event) => {
      const text = event.target.result;
      dispatch(setFormDescr(text));
    };
    reader.readAsText(ev.target.files[0]);
  };

  return (
    <>
      <Input
        value={title}
        label="Title:"
        placeholder="Short title"
        onChange={(event) => setTitle(event.target.value)}
        onBlur={() => dispatch(setFormTitle(title))}
      />
      <Input
        value={description}
        label="Short Description:"
        placeholder="One-sentence description of the contents of this study task"
        onChange={(event) => setDescription(event.target.value)}
        onBlur={() => dispatch(setFormShortDesc(description))}
      />
      <InputLabeled
        value={duration}
        label="Estimated duration:"
        tag="minutes"
        placeholder="duration"
        onChange={(event) => setDuration(event.target.value)}
        onBlur={() => dispatch(setFormDuration(duration))}
      />
      <InputTextArea
        value={body}
        label="Complete Description"
        placeholder="Complete description of this study task"
        onChange={(event) => setBody(event.target.value)}
        onBlur={() => dispatch(setFormDescr(body))}
      />
      <InputFile
        label="upload a markdown text"
        id="markdown"
        onChange={(ev) => fileUpload(ev)}
      />
      <InputLabeled
        value={timeout}
        label="Runner Timeout:"
        tag="seconds"
        placeholder="timeout"
        onChange={(event) => setTimeout(event.target.value)}
        onBlur={() => dispatch(setTimeoutRunner(timeout))}
      />
    </>
  );
};

export default Form;
