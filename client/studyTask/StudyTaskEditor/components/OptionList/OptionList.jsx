import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import TestEditor from '../TestEditor';
import SolutionEditor from '../SolutionEditor.js';
import Button from '../Button/Button';
import { removeOutput, updateConsoleOutput } from '../../../../../lib/redux/action';

const OptionList = (props) => {
  const { id, type } = props;
  const dispatch = useDispatch();
  const studyTask = useSelector((state) => state.studyTaskEdit);
  const source = studyTask.sources;
  const tests = studyTask.tests;
  const timeout = parseInt(studyTask.timeout);

  const [run, setRun] = useState('RUN');
  const [isRun, setIsRun] = useState(false);
  const [displayTest, setDisplayTest] = useState(false);
  const [displaySol, setDisplaySol] = useState(false);


  const handleRequest = () => {
    // const url = 'http://localhost:5011/api/v1/run/post';
    const url = 'http://localhost:5011/api/v1/run';
    // const url = '/api/v1/run';

    const image = 'registry.gitlab.com/openpatch/runner-java:2-extract-class-method-and-line-from-junit-output';
    // const image = 'registry.gitlab.com/openpatch/runner-java';
    // eslint-disable-next-line no-undef,no-underscore-dangle
    const user = Meteor.user()._id;
    const payload = {
      sources: [
        {
          code: source.code,
          fqcn: source.fqcn,
        },
      ],
      tests: [
        {
          code: tests.code,
          fqcn: tests.fqcn,
        },
      ],
    };

    const request = {
      image,
      payload,
      timeout,
      user,
    };

    setIsRun(true);
    // eslint-disable-next-line no-undef
    Meteor.call('HTTPRequestServer', 'POST', url, request, (err, res) => {
      if (err) {
        setIsRun(false);
        return err;
      }
      if (res.data.error) {
        alert(`${res.data.runner_http_status} ${res.data.error}`);
        setIsRun(false);
        return false;
      }
      dispatch(updateConsoleOutput(res.data.payload));
      console.log(res.data.payload);
      setIsRun(false);
      setRun('Run again');
      return res;
    });
  };

  const handleOnSubmit = () => {
    const kind = 'Coding';
    const detail = {};
    const code = {
      solution: {
        code: source.code,
        fqcn: source.fqcn,
      },
      tests: {
        code: tests.code,
        fqcn: tests.fqcn,
      },
    };
    // eslint-disable-next-line no-undef
    Meteor.call('updateStudyTaskCode', id, studyTask.title, studyTask.description, kind,
      // eslint-disable-next-line no-unused-vars
      studyTask.duration, studyTask.body, detail, code, timeout, (error, result) => {
        if (error) {
          // eslint-disable-next-line no-undef
          throwError(error.reason);
        } else {
          dispatch(removeOutput());
          // eslint-disable-next-line no-undef
          Router.go('studyTaskPage', { courseId: studyTask.courseId, studyTaskId: id });
        }
        return false;
      });
  };

  return (
    <div className="ui segment" style={{ marginBottom: '10px' }}>
      <Button
        type="button"
        value={run}
        color="primary"
        loading={isRun}
        onClick={() => handleRequest()}
      />
      {
                type === 'student' || type === 'showTest'
                  ? (
                    <Button
                      type="button"
                      value="Display Test"
                      color="gray"
                      loading={false}
                      onClick={() => setDisplayTest(!displayTest)}
                    />
                  )
                  : null
            }
      {
                type === 'student'
                  ? (
                    <Button
                      type="button"
                      value="Display Solution"
                      color="gray"
                      loading={false}
                      onClick={() => setDisplaySol(!displaySol)}
                    />
                  )
                  : null
            }
      {
                type === 'edit'
                  ? (
                    <Button
                      type="button"
                      value="Update Study Task"
                      color="primary"
                      loading={false}
                      onClick={() => handleOnSubmit()}
                    />
                  )
                  : null
            }


      {displayTest ? <TestEditor readOnly /> : null}
      {displaySol ? <SolutionEditor readOnly /> : null}
    </div>
  );
};

OptionList.defaultProps = {
  type: '',
};

OptionList.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string,
};


export default OptionList;
