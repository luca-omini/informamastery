import { withTracker } from 'meteor/react-meteor-data';
import OptionList from './OptionList';

const OptionListContainer = withTracker((props) => {
  const id = Template.parentData(props.parentNumber).studyTask._id;

  return {
    id,
  };
})(OptionList);

export default OptionListContainer;
