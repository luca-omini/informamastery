import React from 'react';
import AceEditor from 'react-ace';
import PropTypes from 'prop-types';
import 'ace-builds/src-noconflict/mode-java';
import 'ace-builds/src-noconflict/theme-monokai';
import 'ace-builds/src-noconflict/ext-language_tools';

const CodeEditor = ({
  value, onChange, onBlur, annotations, readOnly,
}) => (
  <AceEditor
    mode="java"
    theme="monokai"
    value={value}
    onChange={onChange}
    onBlur={onBlur}
    width="100%"
    height="50vh"
    showPrintMargin
    highlightActiveLine
    annotations={annotations}
    readOnly={readOnly}
    setOptions={{
      useWorker: true,
      copyWithEmptySelection: true,
      autoScrollEditorIntoView: true,
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: true,
      showLineNumbers: true,
      tabSize: 4,
    }}
    style={{
      marginTop: '1px',
      marginBottom: '5px',
      border: '1px solid lightgray',
      borderRadius: '5px',
    }}
  />
);

CodeEditor.defaultProps = {
  annotations: [],
  readOnly: false,
};

CodeEditor.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  annotations: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.oneOfType(
    [PropTypes.number, PropTypes.string],
  ))),
  readOnly: PropTypes.bool,
};

export default CodeEditor;
