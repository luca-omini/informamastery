import React from 'react';
import PropTypes from 'prop-types';

const BreadCrumb = ({ path }) => (
  <div className="ui breadcrumb">
    {path.map((el, index) => (
      index === path.length - 1
        ? (
      // eslint-disable-next-line react/no-array-index-key
          <React.Fragment key={index}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className="active section">{el}</a>
          </React.Fragment>
        )
        : (
      // eslint-disable-next-line react/no-array-index-key
          <React.Fragment key={index}>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className="section">{el}</a>
            <div className="divider"> /</div>
          </React.Fragment>
        )
    ))}
  </div>
);

BreadCrumb.propTypes = {
  path: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default BreadCrumb;
