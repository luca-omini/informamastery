import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Input from '../Input/Input';
import { setCodeSource, setFqcnSource } from '../../../../../lib/redux/action';
import CodeEditor from '../CodeEditor/CodeEditor';
import { parseCodeResult, parseFQCN } from '../../../../../lib/utilityFunctions/parseCodeResult';
import InputFile from '../Input/InputFile';

const SourceEditor = ({ type, readOnly }) => {
  const dispatch = useDispatch();
  const studyTask = useSelector((state) => state.studyTaskEdit);
  const output = useSelector((state) => state.runner);
  const [fqcn, setFqcn] = useState(studyTask.sources.fqcn);
  const [code, setCode] = useState(studyTask.sources.fqcn);
  const stderr = output.src_compile === undefined ? '' : output.src_compile.stderr;
  const annotations = parseCodeResult(output.hasOutput, stderr);

  useEffect(() => {
    setFqcn(studyTask.sources.fqcn);
  }, [studyTask.sources.fqcn]);

  useEffect(() => {
    setCode(studyTask.sources.code);
  }, [studyTask.sources.code]);

  const handleOnBlurFqcn = (value) => {
    dispatch(setFqcnSource(value));
    if (type === 'student') {
      // eslint-disable-next-line no-undef
      Meteor.call('updateFqcnSolution', studyTask.id, value, (err, res) => {
        if (err) {
          return err.response;
        }
        return res;
      });
    }
  };

  const handleOnBlurCode = (value) => {
    dispatch(setCodeSource(value));
    if (type === 'student') {
      // eslint-disable-next-line no-undef
      Meteor.call('updateCodeSolution', studyTask.id, value, (err, res) => {
        if (err) {
          return err.response;
        }
        return res;
      });
    }
  };

  const fileUpload = async (ev) => {
    ev.preventDefault();
    const reader = new FileReader();
    reader.onload = async (event) => {
      const body = event.target.result;
      const fullyQualifiedClassName = parseFQCN(body);
      handleOnBlurCode(body);
      handleOnBlurFqcn(fullyQualifiedClassName);
    };
    reader.readAsText(ev.target.files[0]);
  };

  return (
    <>
      <Input
        label="Provide here your solution"
        placeholder="Fully Qualified Class Name"
        value={fqcn}
        onChange={(event) => setFqcn(event.target.value)}
        onBlur={(ev) => handleOnBlurFqcn(ev.target.value)}
      />
      <CodeEditor
        value={code}
        annotations={annotations}
        onChange={(value) => setCode(value)}
        onBlur={() => handleOnBlurCode(code)}
        readOnly={readOnly}
      />
      {
                !readOnly
                  ? (
                    <InputFile
                      label="Upload a source file"
                      id="source"
                      onChange={(ev) => fileUpload(ev)}
                    />
                  )
                  : null
            }
    </>
  );
};

SourceEditor.defaultProps = {
  type: '',
  readOnly: false,
};

SourceEditor.propTypes = {
  type: PropTypes.string,
  readOnly: PropTypes.bool,
};

export default SourceEditor;
