import React from 'react';
import { useSelector } from 'react-redux';

const OutputTest = () => {
  const output = useSelector((state) => state.runner);
  const tests = output.test_results;

  if (!output.hasOutput) {
    return null;
  }

  if (tests[0].error !== undefined) {
    return null;
  }
  let testFailed = 0;
  let totalTestsRun = 0;

  tests.map((test) => {
    testFailed += test.total_tests_failed;
    totalTestsRun += test.total_tests_run;
    return testFailed;
  });

  return (
    <div>
      <h2>Statistics</h2>
      <div className="ui statistics">
        <div className={` ${testFailed === 0 ? 'green' : 'red'} statistic`}>
          <div className="value">
            {testFailed}
          </div>
          <div className="label">
            {testFailed > 1 ? 'Tests Failed' : 'Test Failed'}
          </div>
        </div>
        <div className={` ${testFailed === 0 ? 'green' : 'red'} statistic`}>
          <div className="value">
            {totalTestsRun - testFailed}
            {' '}
            /
            {totalTestsRun}
          </div>
          <div className="label">
            {totalTestsRun - testFailed > 1 ? 'Tests passed' : 'Test Passed'}
          </div>
        </div>
        <div className="gray statistic">
          <div className="value">
            {totalTestsRun}
          </div>
          <div className="label">
            Total Tests Run
          </div>
        </div>
      </div>
    </div>
  );
};

export default OutputTest;
