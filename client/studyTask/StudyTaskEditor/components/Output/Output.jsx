import React from 'react';
import { useSelector } from 'react-redux';
import Message from '../Message/Message';
import List from '../List/List';
import Console from '../Message/Console';

const Output = () => {
  const runner = useSelector((state) => state.runner);
  if (!runner.hasOutput) {
    return null;
  }
  const hasCompilerError = runner.src_compile.stderr !== '';
  const hasRuntimeError = runner.src_run.stderr !== '';

  const consoleOutput = [runner.src_run.stdout];
  const consoleError = hasRuntimeError ? [runner.src_run.stderr] : [];

  const compilerOutput = [runner.src_compile.stdout];
  const compilerError = hasCompilerError ? [runner.src_compile.stderr] : [];

  let error = 0;
  let testFailed = 0;

  runner.test_results.map((test) => {
    if (test.total_tests_failed === undefined) {
      error = test;
      return false;
    }
    testFailed += test.total_tests_failed;
    return testFailed;
  });

  return (
    <div className="ui vertical segment">
      <>
        { hasRuntimeError
          ? (
            <Console
              info="Console"
              text={consoleError}
              error={hasRuntimeError}
            />
          )
          : (
            <Console
              info="Console"
              text={consoleOutput}
              error={hasRuntimeError}
            />
          )}
      </>
      <>
        { hasCompilerError
          ? (
            <Console
              info="Compiler"
              text={compilerError}
              error={hasCompilerError}
            />
          )
          : (
            <Console
              info="Compiler"
              text={compilerOutput}
              error={hasCompilerError}
            />
          )}
      </>
      {
        testFailed !== 0 && !error
          // eslint-disable-next-line react/no-array-index-key
          ? runner.test_results.map((result, index) => <List tests={result.values} key={index} />)
          : null
      }
      {
        testFailed === 0 && !error
          ? (
            <Message
              severity="success"
              info="You passed all the tests"
              messages={[]}
            />
          )
          : null
      }
      {
                error
                  ? runner.test_compile.map((test, index) => (
                    <Console
                      // eslint-disable-next-line react/no-array-index-key
                      key={index}
                      error={error}
                      info={`Test: ${error.error}`}
                      text={[test.stderr]}
                    />
                  ))
                  : null
            }
      <></>

    </div>
  );
};

export default Output;
