import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Input from '../Input/Input';
import { setCodeTest, setFqcnTest } from '../../../../../lib/redux/action';
import CodeEditor from '../CodeEditor/CodeEditor';
import {
  parseCodeResult,
  parseFQCN,
  parseTestFailure,
} from '../../../../../lib/utilityFunctions/parseCodeResult';
import BreadCrumb from '../BreadCrumb/BreadCrumb';
import InputFile from '../Input/InputFile';

const TestEditor = ({ readOnly }) => {
  const dispatch = useDispatch();
  const studyTask = useSelector((state) => state.studyTaskEdit);
  const runner = useSelector((state) => state.runner);
  const testFailures = runner.hasOutput && runner.test_results[0].values
    ? runner.test_results[0].values
    : [];
  const output = useSelector((state) => state.runner);
  const [fqcn, setFqcn] = useState(studyTask.tests.fqcn);
  const [code, setCode] = useState(studyTask.tests.code);
  const stderr = output.test_compile === undefined ? '' : output.test_compile[0].stderr;
  const annotationsStderr = readOnly ? [] : parseCodeResult(output.hasOutput, stderr);
  const annotationsTests = parseTestFailure(output.hasOutput, testFailures);
  const annotations = [...annotationsStderr, ...annotationsTests];
  const path = fqcn.split('.');

  useEffect(() => {
    setFqcn(studyTask.tests.fqcn);
  }, [studyTask.tests.fqcn]);

  useEffect(() => {
    setCode(studyTask.tests.code);
  }, [studyTask.tests.code]);

  const handleOnBlurFqcn = () => {
    if (readOnly) {
      return false;
    }
    dispatch(setFqcnTest(fqcn));
    return true;
  };

  const handleOnBlurCode = () => {
    if (readOnly) {
      return false;
    }
    dispatch(setCodeTest(code));
    return true;
  };

  const fileUpload = async (ev) => {
    ev.preventDefault();
    const reader = new FileReader();
    reader.onload = async (event) => {
      const body = event.target.result;
      const fullyQualifiedName = parseFQCN(body);
      dispatch(setFqcnTest(fullyQualifiedName));
      dispatch(setCodeTest(body));
    };
    reader.readAsText(ev.target.files[0]);
  };

  return (
    <div className="ui vertical segment">
      {
                readOnly
                  ? (
                    <BreadCrumb
                      path={path}
                    />
                  )
                  : (
                    <Input
                      label="Test"
                      placeholder="Fully Qualified Class Name"
                      value={fqcn}
                      onChange={(event) => !readOnly && setFqcn(event.target.value)}
                      onBlur={() => handleOnBlurFqcn()}
                    />
                  )
            }
      <CodeEditor
        value={code}
        annotations={annotations}
        onChange={(value) => !readOnly && setCode(value)}
        onBlur={() => handleOnBlurCode()}
        readOnly={readOnly}
      />
      {
                !readOnly
                  ? (
                    <InputFile
                      label="Upload a test file"
                      id="test"
                      onChange={(ev) => fileUpload(ev)}
                    />
                  )
                  : null
            }
    </div>
  );
};

TestEditor.defaultProps = {
  readOnly: false,
};

TestEditor.propTypes = {
  readOnly: PropTypes.bool,
};

export default TestEditor;
