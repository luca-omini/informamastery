import { useDispatch } from 'react-redux';
import { removeOutput, setStudyTask } from '../../../../../lib/redux/action';

const GetMeteorData = (props) => {
  const { parentNumber, studyTask } = props;
  const dispatch = useDispatch();
  if (parentNumber === 2) {
    // eslint-disable-next-line no-undef
    Meteor.call('getUserCode', studyTask.id, (err, res) => {
      studyTask.sources.code = res.code;
      studyTask.sources.fqcn = res.fqcn;
      dispatch(removeOutput());
      dispatch(setStudyTask(props.studyTask));
    });
  } else {
    dispatch(removeOutput());
    dispatch(setStudyTask(props.studyTask));
  }

  return null;
};

export default GetMeteorData;
