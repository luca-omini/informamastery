// eslint-disable-next-line import/no-unresolved
import { withTracker } from 'meteor/react-meteor-data';
import GetMeteorData from './GetMeteorData';

const GetMeteorDataContainer = withTracker((props) => {
  const parentData = Template.parentData(props.parentNumber);

  const studyTask = {
    id: parentData.studyTask._id,
    courseId: parentData.course._id,
    title: parentData.studyTask.title,
    description: parentData.studyTask.description,
    duration: parentData.studyTask.duration,
    body: parentData.studyTask.body,
    timeout: parentData.studyTask.timeout,
    tests: {
      code: parentData.studyTask.code === undefined
        ? 'import org.junit.Test;\n'
        + 'import static org.junit.Assert.assertEquals;\n\n'
        + 'public class myTest {\n\n'
        + '}\n'
        : parentData.studyTask.code.tests.code,
      fqcn: parentData.studyTask.code === undefined
        ? 'myTest'
        : parentData.studyTask.code.tests.fqcn,
    },
    sources: {
      code: parentData.studyTask.code === undefined
        ? ''
        : parentData.studyTask.code.solution.code,
      fqcn: parentData.studyTask.code === undefined
        ? ''
        : parentData.studyTask.code.solution.fqcn,
    },
    solution: {
      code: parentData.studyTask.code === undefined
        ? ''
        : parentData.studyTask.code.solution.code,
      fqcn: parentData.studyTask.code === undefined
        ? ''
        : parentData.studyTask.code.solution.fqcn,
    },
  };

  return {
    studyTask,
  };
})(GetMeteorData);

export default GetMeteorDataContainer;
