import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import CodeEditor from '../CodeEditor/CodeEditor';
import BreadCrumb from '../BreadCrumb/BreadCrumb';

const SolutionEditor = ({ readOnly }) => {
  const solution = useSelector((state) => state.studyTaskEdit.solution);
  const path = solution.fqcn.split('.');

  return (
    <div className="ui vertical segment">
      <BreadCrumb
        path={path}
      />
      <CodeEditor
        value={solution.code}
        annotations={[]}
        onChange={() => false}
        onBlur={() => false}
        readOnly={readOnly}
      />
    </div>
  );
};

SolutionEditor.defaultProps = {
  readOnly: false,
};

SolutionEditor.propTypes = {
  readOnly: PropTypes.bool,
};

export default SolutionEditor;
