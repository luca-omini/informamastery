import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
  label, placeholder, value, onChange, onBlur,
}) => (
  <div className="ui input field" style={{ marginBottom: '10px' }}>
    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
    <label htmlFor={`input-${label}`}>{label}</label>
    <input
      id={`input-${label}`}
      type="text"
      name={label}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
    />
  </div>
);

Input.defaultProps = {
  label: '',
  placeholder: '',
  value: '',
};

Input.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
};

export default Input;
