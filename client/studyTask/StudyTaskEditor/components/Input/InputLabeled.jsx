import React from 'react';
import PropTypes from 'prop-types';

const InputLabeled = ({
  label, placeholder, value, onChange, onBlur, tag,
}) => (
  <div className="ui input field" style={{ marginBottom: '10px' }}>
    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
    <label htmlFor={`input-${label}`}>{label}</label>
    <div className="ui right labeled input">
      <input
        id={`input-${label}`}
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
      <div className="ui basic label">{tag}</div>
    </div>
  </div>
);

InputLabeled.defaultProps = {
  label: '',
  placeholder: '',
  tag: '',
  value: '',
};

InputLabeled.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  tag: PropTypes.string,
};

export default InputLabeled;
