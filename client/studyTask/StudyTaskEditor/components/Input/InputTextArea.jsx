import React from 'react';
import PropTypes from 'prop-types';

const InputTextArea = ({
  label, placeholder, value, onChange, onBlur,
}) => (
  <div className="ui input field" style={{ marginBottom: '10px' }}>
    {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
    <label htmlFor={`input-${label}`}>{label}</label>
    <textarea
      id={`input-${label}`}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
    />
  </div>
);

InputTextArea.defaultProps = {
  label: '',
  placeholder: '',
  value: '',
};

InputTextArea.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
};

export default InputTextArea;
