import React from 'react';
import PropTypes from 'prop-types';

const InputFile = ({ label, onChange, id }) => (
  <>
    <label htmlFor={id} className="ui icon button" style={{ marginBottom: '10px' }}>
      <i className="file icon" />
      {label}
    </label>
    <input
      type="file"
      id={id}
      onChange={onChange}
      style={{ display: 'none' }}
    />
  </>
);

InputFile.defaultProps = {
  label: '',
};

InputFile.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default InputFile;
