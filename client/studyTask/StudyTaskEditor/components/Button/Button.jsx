import React from 'react';
import PropTypes from 'prop-types';

const Button = ({
  color, loading, onClick, value,
}) => (
  <button
    type="button"
    className={`ui ${color} ${loading ? 'loading' : ''}  button`}
    onClick={onClick}
  >
    { value }
  </button>
);

Button.defaultProps = {
  color: 'primary',
  loading: false,
};

Button.propTypes = {
  color: PropTypes.string,
  loading: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default Button;
