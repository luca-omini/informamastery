import React from 'react';
import PropTypes from 'prop-types';

const Console = ({ error, info, text }) => (
    <div className={`ui ${error ? 'red' : ''} message`}>
      <div className={`ui top left attached ${error ? 'red' : 'green'} label`}>{info}</div>
      <div className="content">
        {text.map((message, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <span style={{ whiteSpace: 'pre-wrap' }} key={index}>
            {message}
          </span>
        ))}
      </div>
    </div>
);

Console.defaultProps = {
  error: false,
  info: 'Console',
};

Console.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  error: PropTypes.any,
  info: PropTypes.string,
  text: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Console;
