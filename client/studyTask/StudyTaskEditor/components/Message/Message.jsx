import React from 'react';
import PropTypes from 'prop-types';

const Message = ({ severity, info, messages }) => {
  let color;
  let icon;
  switch (severity) {
    case 'error':
      icon = 'bug';
      color = 'red';
      break;
    case 'success':
      icon = 'circle check';
      color = 'green';
      break;
    case 'info':
      icon = 'info';
      color = 'blue';
      break;
    default:
      color = '';
      icon = '';
      break;
  }
  return (
    <div className={`ui icon ${color} message`}>
      <i className={`${icon} icon`} />
      <div className="content">
        <div className="header">
          {info}
        </div>
        <ul>
          {messages.map((message, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <li key={index}>
              {' '}
              {message}
              {' '}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

Message.defaultProps = {
  info: '',
  severity: 'default',
};

Message.propTypes = {
  severity: PropTypes.oneOf(['success', 'warning', 'error', 'info', 'default']),
  info: PropTypes.string,
  messages: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Message;
