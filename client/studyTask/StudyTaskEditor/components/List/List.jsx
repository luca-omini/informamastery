import React from 'react';
import PropTypes from 'prop-types';

const List = ({ tests }) => (
  <div className="ui celled red list">
    {tests.map((test) => (
      // eslint-disable-next-line jsx-a11y/anchor-is-valid
      <a className="item" key={test.number}>
        <i className="bug icon" />
        <div className="content">
          <div className="header">
            {`Class ${test.class}: test ${test.method} on line ${test.line} differ!`}
          </div>
          <div className="description">
            {`Expected Value: ${test.expected_value}; Actual Value: ${test.actual_value}`}
          </div>
        </div>
      </a>
    ))}
  </div>

);

List.propTypes = {
  tests: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default List;
