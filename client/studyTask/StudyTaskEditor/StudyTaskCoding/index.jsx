import React from 'react';
import { Provider } from 'react-redux';
import store from '../../../../lib/redux/store';
import OutputTests from '../components/OutputTest';
import GetMeteorDataContainer from '../components/GetMeteorData';
import SourceEditor from '../components/SourceEditor';
import Output from '../components/Output';
import OptionListContainer from '../components/OptionList';


const StudyTaskCoding = () => (
  <Provider store={store}>
    <GetMeteorDataContainer parentNumber={2} />
    <SourceEditor type="student" readOnly={false} />
    <Output />
    <OutputTests />
    <OptionListContainer parentNumber={2} type="showTest" />
  </Provider>
);

export default StudyTaskCoding;
