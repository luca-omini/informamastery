import React from 'react';
import { Provider } from 'react-redux';
import store from '../../../../lib/redux/store';
import GetMeteorDataContainer from '../components/GetMeteorData';
import OutputTests from '../components/OutputTest';
import Form from '../components/Form';
import TestEditor from '../components/TestEditor';
import Output from '../components/Output';
import SourceEditor from '../components/SourceEditor';
import OptionListContainer from '../components/OptionList';


const StudyTaskCodingEditor = () => (
  <Provider store={store}>
    <GetMeteorDataContainer parentNumber={1} />
    <Form />
    <TestEditor readOnly={false} />
    <SourceEditor type="edit" readOnly={false} />
    <Output />
    <OutputTests />
    <OptionListContainer parentNumber={1} type="edit" />
  </Provider>
);

export default StudyTaskCodingEditor;
