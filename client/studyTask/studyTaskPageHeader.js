// function used below and also in studyTaskItem.js
getSumOfDurationsOfRemainingSections = function(sectionIds) {
	console.log("getSumOfDurationsOfRemainingSections("+sectionIds+")");
	var sum = 0;
	var allDurationsKnown = true;
	Sections.find({_id: {$in: sectionIds}}).forEach(function(section) {
		console.log(section);
		if (section.duration) {
			var sectionStatus = SectionStatus.findOne({sectionId: section._id, userId: Meteor.userId()});
			if (sectionStatus && sectionStatus.recallText) {
				// recalled
			} else {
				// not recalled yet
				sum += parseInt(section.duration);
				console.log("sum incremented by "+section.duration+", now is "+sum);
			}
		} else {
			allDurationsKnown = false;
		}
	});
	console.log(sum);
	return allDurationsKnown?sum:undefined;
}

Template.studyTaskPageHeader.helpers({
	getSumOfDurationsOfRemainingSections: function(sectionIds) {
		console.log("Template.studyTaskPageHeader.helpers.getSumOfDurationsOfRemainingSections("+sectionIds+")");
		return getSumOfDurationsOfRemainingSections(sectionIds);
	},
});
