Template.ISubmittedPracticeProblemFeedback.helpers({
	getQualityText: function(qualityNumber) {
		return _convertQualityNumberToText(qualityNumber);
	},
	getDifficultyText: function(difficultyNumber) {
		return _convertDifficultyNumberToText(difficultyNumber);
	},
});
