var INITIAL_LIMIT = 20;
var LIMIT_INCREMENT = 10;

incrementCourseFeedLengthLimit = function() {
	console.log("incrementCourseFeedLengthLimit");
	newLimit = Session.get('courseFeedLengthLimit') + INITIAL_LIMIT;
	Session.set('courseFeedLengthLimit', newLimit);
};



Template.courseFeedPage.created = function() {
	console.log("Template.courseFeedPage.created");
	Session.setDefault('courseFeedLengthLimit', LIMIT_INCREMENT);
	// Deps.autorun() automatically rerun the subscription whenever Session.get('feedLengthLimit') changes
	// http://docs.meteor.com/#deps_autorun
	Deps.autorun(function() {
		var course = Session.get("course");
		if (course) {
			console.log("### Subscribing to 'course-feed' (courseId="+course._id+", limit="+Session.get('courseFeedLengthLimit')+")");
			Meteor.subscribe('course-feed', course._id, Session.get('courseFeedLengthLimit'));
		}
	});
};


Template.courseFeedPage.rendered = function() {
	console.log("Template.courseFeedPage.rendered");
	// is triggered every time we scroll
	$(window).scroll(function() {
		if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
			//TODO: Don't do this unconditionally! Ensure we show this page in the router!
			//incrementCourseFeedLengthLimit();
		}
	});
}


Template.courseFeedPage.helpers({
	getEvents: function(courseId) {
		console.log("Template.courseFeedPage.helpers.getEvents("+courseId+")");
		return FeedEvents.find({courseId: courseId, active: true}, {sort: {date: -1}, limit: Session.get('courseFeedLengthLimit')});
	},
	hasMoreEvents: function(courseId) {
		// If, once the subscription is ready, we have less events than we asked for,
		// we've got all the events in the feed.
		return !(FeedEvents.find({courseId: courseId, active: true}).count() < Session.get("courseFeedLengthLimit"));
	},
});


Template.courseFeedPage.events({
	'click .more-events-button': function(evt) {
		console.log("Template.courseFeedPage.events click .more-events-button");
		incrementCourseFeedLengthLimit();
	},
});
