var INITIAL_LIMIT = 20;
var LIMIT_INCREMENT = 10;

incrementStudentFeedLengthLimit = function() {
	console.log("incrementStudentFeedLengthLimit");
	newLimit = Session.get('studentFeedLengthLimit') + INITIAL_LIMIT;
	Session.set('studentFeedLengthLimit', newLimit);
};


Template.studentFeedPage.created = function() {
	console.log("Template.studentFeedPage.created");
	Session.setDefault('studentFeedLengthLimit', LIMIT_INCREMENT);
};


Template.studentFeedPage.rendered = function() {
	console.log("Template.studentFeedPage.rendered");
	console.log("Template.studentFeedPage.rendered: this=", this);
	var self = this;
	// Deps.autorun() automatically rerun the subscription whenever Session.get('studentFeedLengthLimit') changes
	// http://docs.meteor.com/#deps_autorun
	Deps.autorun(function() {
		console.log("Template.studentFeedPage.rendered: Deps.autorun function");
		var course = Session.get("course");
		if (self.data) {
			var studentId = self.data.studentId;
			console.log("### Subscribing to 'user-feed' (courseId="+course._id+", studentId="+studentId+", limit="+Session.get('studentFeedLengthLimit')+")");
			Meteor.subscribe('user-feed', course._id, studentId, Session.get('studentFeedLengthLimit'));
		}
	});
	// is triggered every time we scroll
	$(window).scroll(function() {
		if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
			//TODO: Don't do this unconditionally! Ensure we show this page in the router!
			//incrementStudentFeedLengthLimit();
		}
	});
};


Template.studentFeedPage.helpers({
	getEvents: function(courseId, studentId) {
		console.log("Template.studentFeedPage.helpers.getEvents("+courseId+", "+studentId+")");
		return FeedEvents.find({courseId: courseId, userId: studentId}, {sort: {date: -1}, limit: Session.get('studentFeedLengthLimit')});
	},
	hasMoreEvents: function(courseId, studentId) {
		// If, once the subscription is ready, we have less events than we asked for,
		// we've got all the events in the feed.
		return !(FeedEvents.find({courseId: courseId, userId: studentId}).count() < Session.get("studentFeedLengthLimit"));
	},
});


Template.studentFeedPage.events({
	'click .more-events-button': function(evt) {
		console.log("Template.studentFeedPage.events click .more-events-button");
		incrementStudentFeedLengthLimit();
	},
});
