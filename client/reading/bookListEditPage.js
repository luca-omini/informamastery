Template.bookListEditPage.events({
	'submit .form': function(event, template) {
		var courseId = this.course._id;
		var introduction = event.target.introduction.value;
		Meteor.call('updateBookListIntroduction', courseId, introduction, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("bookListPage", {courseId: courseId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .add-book-button': function(ev, template) {
		var courseId = this.course._id;
		Meteor.call('addBook', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("bookEditPage", {courseId: courseId, bookId: result});
			}
		});
	},
	'click .remove-book-button': function(ev, template) {
		var bookId = this._id;
		Meteor.call('removeBook', bookId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
