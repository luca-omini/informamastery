Template.sectionPage.helpers({
	stage: function(status) {
		if (status.recallText) {
			return "review";
		} else if (status.finishedReading) {
			return "recall";
		} else if (status.startedReading) {
			return "read";
		} else {
			return "prepare";
		}
	},
	stageIs: function(status, stage) {
		var s = "";
		if (status.recallText) {
			s="review";
		} else if (status.finishedReading) {
			s="recall";
		} else if (status.startedReading) {
			s="read";
		} else {
			s="prepare";
		}
		return stage==s;
	},
	prepareState: function(status) {
		if (status.startedReading || status.finishedReading || status.recallText) {
			return "completed";
		} else {
			return "active";
		}
	},
	readState: function(status) {
		if (status.startedReading && !status.finishedReading) {
			return "active";
		} else if (status.finishedReading || status.recallText) {
			return "completed";
		} else {
			return "disabled";
		}
	},
	recallState: function(status) {
		if (status.finishedReading && !status.recallText) {
			return "active";
		} else if (status.recallText) {
			return "completed";
		} else {
			return "disabled";
		}
	},
	reviewState: function(status) {
		return status.recallText?"active":"disabled";
	},
	hasOwnStatus: function(sectionId) {
		//console.log("hasOwnStatus("+sectionId+")");
		return SectionStatus.find({sectionId: sectionId, userId: Meteor.userId()}).count()>0;
	},
	getOwnStatus: function(sectionId) {
		//console.log("getOwnStatus("+sectionId+")");
		return SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()});
	},
	getOwnStatusOrNullObject: function(sectionId) {
		var status = SectionStatus.findOne({sectionId: sectionId, userId: Meteor.userId()});
		if (!status) {
			status = {};
		}
		return status;
	},
});

Template.sectionPage.events({
	'click .start-button': function(event, template) {
		console.log("Template.sectionPage click .start-button");
		console.log("this:", this);
		console.log("Template.parentData(0):", Template.parentData(0));
		console.log("Template.parentData(1):", Template.parentData(1));
		console.log("Template.parentData(2):", Template.parentData(2));
		var section = Template.parentData(0).section;
		var sectionId = section._id;
		Meteor.call('startReadingSection', sectionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .finish-button': function(event, template) {
		console.log("Template.sectionPage click .finish-button");
		console.log("this:", this);
		console.log("Template.parentData(0):", Template.parentData(0));
		console.log("Template.parentData(1):", Template.parentData(1));
		console.log("Template.parentData(2):", Template.parentData(2));
		var section = Template.parentData(0).section;
		var sectionId = section._id;
		Meteor.call('finishReadingSection', sectionId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'submit .form': function(event, template) {
		event.preventDefault();
		console.log("Template.sectionPage click .save-recall-text-button");
		console.log("this:", this);
		console.log("Template.parentData(0):", Template.parentData(0));
		console.log("Template.parentData(1):", Template.parentData(1));
		console.log("Template.parentData(2):", Template.parentData(2));
		var section = Template.parentData(0).section;
		var sectionId = section._id;
		var recallText = event.target.recallText.value;
		Meteor.call('updateSectionStatus', sectionId, recallText, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
