Template.sectionEditPage.events({
	'submit .form': function(event, template) {
		var courseId = this.course._id;
		var bookId = this.book._id;
		var chapterId = this.chapter._id;
		var sectionId = this.section._id;
		var title = event.target.title.value;
		var number = event.target.number.value;
		var sortKey = event.target.sortKey.value;
		var duration = event.target.duration.value;
		var contents = event.target.contents.value;
		var embedUrl = event.target.embedUrl.value;
		var externalUrl = event.target.externalUrl.value;
		Meteor.call('updateSection', sectionId, title, number, sortKey, duration, contents, embedUrl, externalUrl, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("sectionPage", {courseId: courseId, bookId: bookId, chapterId: chapterId, sectionId: sectionId});
			}
		});
		// Prevent default form submit
		return false;
	},
});
