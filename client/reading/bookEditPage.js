Template.bookEditPage.created = function () {
	// ReactiveVar to hold the bulk create book structure data
	this.structure = new ReactiveVar([]);
};


Template.bookEditPage.helpers({
	getStructure: function() {
		console.log("Template.bookEditPage.helpers.getStructure()");
		// retrieve the bulk create book structure data from the ReactiveVar set below
		var structure = Template.instance().structure.get();
		console.log(structure);
		return structure;
	},
});


Template.bookEditPage.events({
	'submit .update-form': function(event, template) {
		var courseId = this.course._id;
		var bookId = this.book._id;
		var abbreviation = event.target.abbreviation.value;
		var title = event.target.title.value;
		var subtitle = event.target.subtitle.value;
		var author = event.target.author.value;
		var url = event.target.url.value;
		var imageUrl = event.target.imageUrl.value;
		var description = event.target.description.value;
		Meteor.call('updateBook', bookId, abbreviation, title, subtitle, author, url, imageUrl, description, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("bookPage", {courseId: courseId, bookId: bookId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .add-chapter-button': function(ev, template) {
		var courseId = this.course._id;
		var bookId = this.book._id;
		Meteor.call('addChapter', bookId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("chapterEditPage", {courseId: courseId, bookId: bookId, chapterId: result});
			}
		});
	},
	'click .remove-chapter-button': function(ev, template) {
		var chapterId = this._id;
		Meteor.call('removeChapter', chapterId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'submit .bulk-create-book-structure-form': function(event, template) {
		var courseId = this.course._id;
		var bookId = this.book._id;
		var toc = event.target.toc.value;

		console.log("submit .bulk-create-book-structure-form");
		console.log(toc);

		var structure = [];
		try {
			var lines = toc.split("\n");
			_.each(lines, function(element, index, list) {
				var line = element.trim();
				console.log("LINE: '"+line+"'");
				var chapterRegEx = /^([0-9a-zA-Z]+)\.?\s+(.*)$/;
				if (chapterRegEx.test(line)) {
					var matches = chapterRegEx.exec(line);
					var chapterNumber = matches[1];
					var chapterTitle = matches[2];
					console.log("CHAPTER '"+chapterNumber+"': '"+chapterTitle+"'");
					var chapter = _.findWhere(structure, {number: chapterNumber});
					if (!chapter) {
						chapter = {
							sections: [],
						};
						structure.push(chapter);
					}
					chapter.title = chapterTitle;
					chapter.number = chapterNumber;
				}
				var sectionPattern = /^([0-9a-zA-Z]+)\.([0-9]+)\.?\s+(.*)$/;
				if (sectionPattern.test(line)) {
					var matches = sectionPattern.exec(line);
					var chapterNumber = matches[1];
					var sectionNumber = matches[2];
					var sectionTitle = matches[3];
					console.log("SECTION '"+chapterNumber+"."+sectionNumber+"': '"+sectionTitle+"'");
					var chapter = _.findWhere(structure, {number: chapterNumber});
					if (!chapter) {
						chapter = {
							number: chapterNumber,
							title: "?",
							sections: [],
						};
						structure.push(chapter);
					}
					chapter.sections.push({
						number: sectionNumber,
						title: sectionTitle,
					});
				}
			});
			console.log(structure);
		} catch (error) {
			console.log(error);
		}
		// store the bulk create book structure data in the ReactiveVar retrieved above
		template.structure.set(structure);
		// Prevent default form submit
		return false;
	},
});
