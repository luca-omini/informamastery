Template.sectionPageHeader.helpers({
  hasOwnBookmark: function(sectionId) {
		return Annotations.find({sectionId: sectionId, userId: Meteor.userId(), kind: "bookmark", removed: false}).count()>0;
	},
	getOwnAnnotations: function(sectionId) {
		return Annotations.find({sectionId: sectionId, userId: Meteor.userId(), removed: false});
	},
	getOtherQuestions: function(sectionId) {
		return Annotations.find({sectionId: sectionId, userId: {$ne: Meteor.userId()}, kind: "question", removed: false});
	},
	hasAnnotationsToDisplay: function(sectionId) {
		var count =
			Annotations.find({sectionId: sectionId, userId: Meteor.userId(), removed: false}).count()+
			Annotations.find({sectionId: sectionId, userId: {$ne: Meteor.userId()}, kind: "question", removed: false}).count();
		//console.log("Template.sectionPage.helpers..hasAnnotationsToDisplay("+sectionId+") -> "+count);
		return count;
	},
  getPreviousSection: function(section) {
		if (section) {
			return Sections.findOne({chapterId: section.chapterId, removed: false, sortKey: {$lt: section.sortKey}}, {sort: {sortKey: -1}});
		} else {
			return undefined;
		}
	},
	getNextSection: function(section) {
		if (section) {
			return Sections.findOne({chapterId: section.chapterId, removed: false, sortKey: {$gt: section.sortKey}}, {sort: {sortKey: 1}});
		} else {
			return undefined;
		}
	},
  getTopicsForSectionId: function(sectionId) {
		//console.log("getTopicsForSectionId("+sectionId+")");
		var topicIds = StudyTasks.find({"detail.sectionIds": sectionId}).map(function(studyTask) {return studyTask.topicId});
		//console.log("topicIds", topicIds);
		return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
	},
});
