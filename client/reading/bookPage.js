Template.bookPage.helpers({
	countBookSections: function(bookId) {
		console.log("countBookSections("+bookId+")");
		return Sections.find({bookId: bookId}).count();
	},
	countOwnBookSectionStatuses: function(bookId) {
		console.log("countOwnBookSectionStatuses("+bookId+")");
		return SectionStatus.find({bookId: bookId, userId: Meteor.userId()}).count();
	},
	chapterCompleted: function(chapterId) {
		return SectionStatus.find({chapterId: chapterId, userId: Meteor.userId()}).count()
			== Sections.find({chapterId: chapterId}).count();
	},
	countChapterSections: function(chapterId) {
		console.log("countChapterSections("+chapterId+")");
		return Sections.find({chapterId: chapterId}).count();
	},
	countOwnChapterSectionStatuses: function(chapterId) {
		console.log("countOwnChapterSectionStatuses("+chapterId+")");
		return SectionStatus.find({chapterId: chapterId, userId: Meteor.userId()}).count();
	},
});
