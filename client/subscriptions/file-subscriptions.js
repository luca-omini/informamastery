// Set up an autorun to keep the X-Auth-Token cookie up-to-date and
// to update the subscriptions when the userId changes.
Tracker.autorun(function() {
  console.log("subscription autorun executing");
  var userId = Meteor.userId();
  var course = Session.get("course");
  var courseId = undefined;
  if (course) {
    courseId = course._id;
  }
  console.log("### Subscribing to 'labSubmissionFiles' (userId: "+userId+", courseId: "+courseId+")");
  Meteor.subscribe('labSubmissionFiles', userId, courseId);
  console.log("### Subscribing to 'studentPhotoFiles' (userId: "+userId+", courseId: "+courseId+")");
  Meteor.subscribe('studentPhotoFiles', userId, courseId);
  console.log("### Subscribing to 'courseFiles' (userId: "+userId+", courseId: "+courseId+")");
  Meteor.subscribe('courseFiles', userId, courseId);
  // $.cookie() assumes use of the "jquery-cookie" Atmosphere package.
  console.log("Accounts._storedLoginToken() -> ", Accounts._storedLoginToken());
  // Note: use the path option to make the cookie valid for the entire site
  //       (given that we use routing, the cookie will be created on one page,
  //        but used on another page, and won't be found unless path: "/")
  $.cookie('X-Auth-Token', Accounts._storedLoginToken(), {path: "/"});
});
