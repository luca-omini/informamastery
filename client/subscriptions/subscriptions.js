// all clients always subscribe to the course collection
// (seeing all non-deleted courses)
console.log("### Subscribing to 'courses'");
Meteor.subscribe("courses");


// the "course" session variable drives the subscription to the static contents of a course
Deps.autorun(function() {
  var course = Session.get("course");
  if (course) {
    console.log("### Subscribing to 'course-contents' (courseId: "+course._id+")");
    Meteor.subscribe("course-contents", course._id);
  }
});

// the Meteor.user variable drives the subscription to the dynamic contents of a given user
//TODO: check that this works!
Deps.autorun(function() {
    var user = Meteor.user();
    if (user) {
      console.log("### Subscribing to 'my-user-contents' (userId: "+user._id+")");
      Meteor.subscribe("my-user-contents");
      if (isAdmin()) {
        console.log("### Subscribing to 'all-user-contents' (userId: "+user._id+")");
        Meteor.subscribe("all-user-contents");
      }
    }
});

//TODO: subscribe to user-course-contents
Deps.autorun(function() {
    var user = Meteor.user();
    var course = Session.get("course");
    if (user && course) {
      console.log("### Subscribing to 'my-user-course-contents' (userId: "+user._id+", courseId: "+course._id+")");
      Meteor.subscribe("my-user-course-contents", course._id);
    }
});
