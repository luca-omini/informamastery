Template.labFeedbackPage.helpers({
	gravatarUrl: function(user) {
		return Gravatar.imageUrl(user.emails[0].address, {size: 200, secure: window.location.protocol==="https:"});
	},
  teams: function(labId) {
    return LabTeams.find({labId: labId});
  },
  getLabFeedbackForLabAndUser: function(labId, userId) {
    return LabFeedbacks.findOne({labId: labId, userId: userId});
  },
});
