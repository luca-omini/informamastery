function createNewRubricCategory(rubricId) {
  Meteor.call("createRubricCategory", rubricId, function(error2, result2) {
    if (error2) {
      throwError(error2.reason);
    }
    Meteor.call("createRubricItem", result2, function(error3, result3) {
      if (error3) {
        throwError(error3.reason);
      }
      Meteor.call("setRubricItemName", result3, "Nothing correct", function(error4, result4) {
        if (error4) {
          throwError(error4.reason);
        }
      });
      Meteor.call("setRubricItemPoints", result3, 0, function(error5, result5) {
        if (error5) {
          throwError(error5.reason);
        }
      });
    });
  });
}

Template.rubricEditPane.helpers({
  hasRubric: function(contentItemId) {
		return Rubrics.find({itemId: contentItemId}).count()>0;
	},
	getRubric: function(contentItemId) {
		return Rubrics.findOne({itemId: contentItemId, removed: {$ne: true}});
	},
  getRubricCategories: function(rubricId) {
    return RubricCategories.find({rubricId: rubricId, removed: {$ne: true}}, {sort: {index: 1}});
  },
});

Template.rubricEditPane.events({
  'click .create-rubric-button': function(event, template) {
		console.log("click .create-rubric-button");
		var item = this;
		Meteor.call("createRubric", item._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
      createNewRubricCategory(result);
		});
	},
  'click .add-rubric-category-button': function(event, template) {
		console.log("click .add-rubric-category-button");
		var rubric = this;
    createNewRubricCategory(rubric._id);
	},
});
