Template.computeLabScoreSummaryPane.helpers({
  getLabScoreSummary: function(courseId) {
    console.log("getLabScoreSummary(", courseId, ")");
    var labScoreSummary = Summaries.findOne({courseId: courseId, kind: "labScoreSummary"} , {sort: {date: -1}, limit: 1});
    console.log("labScoreSummary:", labScoreSummary);
    return labScoreSummary;
  },
});

Template.computeLabScoreSummaryPane.events({
	'click .recompute-summary-button': function(ev, template) {
		console.log("recompute-summary-button clicked");
		var course = Template.currentData();
    console.log(course);
		var courseId = course._id;
    Meteor.call("computeLabScoreSummary", courseId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
    });
	},
});
