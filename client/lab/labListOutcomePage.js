Template.labListOutcomePage.helpers({
  getLabScoreSummary: function(courseId) {
    console.log("getLabScoreSummary(", courseId, ")");
    var labScoreSummary = Summaries.findOne({courseId: courseId, kind: "labScoreSummary"} , {sort: {date: -1}, limit: 1});
    console.log("labScoreSummary:", labScoreSummary);
    return labScoreSummary;
  },
  extractMaxScore: function(labScoreSummary, labId) {
    //console.log("extractMaxScore(", labScoreSummary, labId, ")");
    var maxScore = labScoreSummary.data[labId]["maxScore"];
    //console.log(maxScore);
    return maxScore;
  },
  extractScore: function(labScoreSummary, labId, userId) {
    //console.log("extractScore(", labScoreSummary, labId, userId, ")");
    var score = labScoreSummary.data[labId]["userScores"][userId];
    //console.log(score);
    return score;
  },
  getPastNonHiddenLabs: function(courseId) {
    return getPastNonHiddenLabs(courseId);
  },
  getApprovedNotWithdrawnUsers: function(courseId) {
    var studentIds = StudentRegistrations.find({courseId: courseId, approved: true, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
		console.log("studentIds:", studentIds);
		return Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
  },
  getLabTeam: function(labId, userId) {
    // assume there is just one...
    var team = LabTeams.findOne({labId: labId, memberIds: userId});
    return team;
  },
  computeOverallScore: function(labId, labTeamId) {
    return computeOverallScore(labId, labTeamId);
  },
  computeMaxOverallScore: function(labId) {
    return computeMaxOverallScore(labId);
  },
});
