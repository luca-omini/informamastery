Template.labReferenceSolutionPage.helpers({
  getContentItems: function(labId) {
		return ContentItems.find({containerId: labId, removed: {$ne: true}}, {sort: {index: 1}});
	},
	getNumberOfQuestionItems: function(labId) {
    console.log("Template.labReferenceSolutionPage.helpers:getNumberOfQuestionItems("+labId+")");
    //TODO: Should we also count fileSubmission items?
		return ContentItems.find({containerId: labId, kind: "question", removed: {$ne: true}}).count();
	},
	getNumberOfReferenceSubmissions: function(labId) {
		//TODO Should we also count fileSubmission items?
		var itemIds = ContentItems.find({containerId: labId, kind: "question", removed: {$ne: true}}).map(function (item) {return item._id;});
    // reference solutions are stored with null for userIds
		return ContentItemSubmissions.find({itemId: {$in: itemIds}, labTeamId: null, userIds: null}).count();
	},
});
