Template.scoreItemEditPane.helpers({
  isSelected: function(contentItemId, teamId, rubricItemId) {
    console.log("Template.scoreItemEditPane.helpers:isSelected(", contentItemId, teamId, rubricItemId, ")");
    return ContentItemScores.find({contentItemId: contentItemId, teamId: teamId, checkedRubricItemIds: rubricItemId}).count()>0;
  },
});

Template.scoreItemEditPane.events({
  'click .item-circle': function(event, template) {
    console.log("click .item-circle");
		var rubricItem = this;
    //console.log("this", this);
    //var parent0 = Template.parentData(0);
    //var parent1 = Template.parentData(1);
    //var parent2 = Template.parentData(2);
    //var parent3 = Template.parentData(3);
    //console.log("parent0", parent0);
    //console.log("parent1", parent1);
    //console.log("parent2", parent2);
    //console.log("parent3", parent3);
    var contentItemId = rubricItem.itemId;
    var teamId = Template.parentData(3).team._id;
    var rubricItemId = rubricItem._id;
    var selected = ContentItemScores.find({contentItemId: contentItemId, teamId: teamId, checkedRubricItemIds: rubricItemId}).count()>0;
    Meteor.call("scoreSetRubricItemState", contentItemId, teamId, rubricItemId, !selected, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'submit .item-name-form': function(event, template) {
		console.log("submit .item-name-form");
		var rubricItem = this;
    var itemNameInput = template.$('.item-name-input');
    console.log(itemNameInput);
    var name = template.$('.item-name-input').val();
    console.log(name);
		Meteor.call("setRubricItemName", rubricItem._id, name, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
    // prevent default
		return false;
	},
  'blur .item-name-input': function(event, template) {
    console.log("blur .item-name-input");
    var rubricItem = this;
    var itemNameInput = template.$('.item-name-input');
    console.log(itemNameInput);
    var name = template.$('.item-name-input').val();
    console.log(name);
		Meteor.call("setRubricItemName", rubricItem._id, name, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'submit .item-points-form': function(event, template) {
		console.log("submit .item-points-form");
		var rubricItem = this;
    var itemPointsInput = template.$('.item-points-input');
    console.log(itemPointsInput);
    var points = template.$('.item-points-input').val();
    console.log(name);
		Meteor.call("setRubricItemPoints", rubricItem._id, points, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
    // prevent default
		return false;
	},
  'blur .item-points-input': function(event, template) {
    console.log("blur .item-points-input");
    var rubricItem = this;
    var itemPointsInput = template.$('.item-points-input');
    console.log(itemPointsInput);
    var points = template.$('.item-points-input').val();
    console.log(name);
		Meteor.call("setRubricItemPoints", rubricItem._id, points, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
});
