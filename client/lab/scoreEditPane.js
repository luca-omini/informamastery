Template.scoreEditPane.helpers({
  hasRubric: function(contentItemId) {
		return Rubrics.find({itemId: contentItemId}).count()>0;
	},
	getRubric: function(contentItemId) {
		return Rubrics.findOne({itemId: contentItemId, removed: {$ne: true}});
	},
  getRubricCategories: function(rubricId) {
    return RubricCategories.find({rubricId: rubricId, removed: {$ne: true}}, {sort: {index: 1}});
  },
  getContentItemScore: function(contentItemId, teamId) {
    return ContentItemScores.findOne({contentItemId: contentItemId, teamId: teamId});
  },
  computeRubricPoints: function(rubric, teamId) {
    //console.log("Template.scoreEditPane.helpers:computeRubricPoints(", rubric, teamId, ")");
    return computeRubricPoints(rubric, teamId);
  },
  computeMaxRubricPoints: function(rubric) {
    return computeMaxRubricPoints(rubric);
  },
  multipleCateogoriesOrPointAdjustment: function(rubric, pointAdjustment) {
    console.log("Template.scoreEditPane.helpers:multipleCateogoriesOrPointAdjustment(", rubric, pointAdjustment, ")");
    return pointAdjustment || RubricCategories.find({rubricId: rubric._id, removed: {$ne: true}}).count()>1;
  },
});

Template.scoreEditPane.events({
  'blur .comment-input': function(event, template) {
    console.log("blur .comment-input");
    //var commentInput = template.$('.comment-input');
    //console.log(commentInput);
    var comment = template.$('.comment-input').val();
    console.log("comment", comment);
    var contentItemId = Template.parentData(0).contentItem._id;
    console.log("contentItemId", contentItemId);
    var teamId = Template.parentData(0).team._id;
    console.log("teamId", teamId);
		Meteor.call("scoreSetComment", contentItemId, teamId, comment, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'blur .point-adjustment-input': function(event, template) {
    console.log("blur .point-adjustment-input");
    //var pointAdjustmentInput = template.$('.point-adjustment-input');
    //console.log(pointAdjustmentInput);
    var pointAdjustment = template.$('.point-adjustment-input').val();
    console.log("pointAdjustment", pointAdjustment);
    var contentItemId = Template.parentData(0).contentItem._id;
    console.log("contentItemId", contentItemId);
    var teamId = Template.parentData(0).team._id;
    console.log("teamId", teamId);
		Meteor.call("scoreSetPointAdjustment", contentItemId, teamId, pointAdjustment, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
});
