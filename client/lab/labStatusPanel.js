Template.labStatusPanel.events({
  'click .show-as-preview-button': function(event, template) {
		console.log("click .show-as-preview-button");
		var labId = this.lab._id;
		Meteor.call("showLabAsPreview", labId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .open-button': function(event, template) {
		console.log("click .open-button");
		var labId = this.lab._id;
		Meteor.call("openLab", labId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .close-button': function(event, template) {
		console.log("click .close-button");
		var labId = this.lab._id;
		Meteor.call("closeLab", labId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .reopen-button': function(event, template) {
		console.log("click .reopen-button");
		var labId = this.lab._id;
		Meteor.call("reopenLab", labId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .hide-button': function(event, template) {
		console.log("click .hide-button");
		var labId = this.lab._id;
		Meteor.call("hideLab", labId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
