function _getFutureLabs(courseId) {
	var now = new Date();
  return Labs.find({courseId: courseId, removed: false, deadlineDate: {$gt: now}}, {sort: {deadlineDate: 1}});
}

function _getUnscheduledLabs(courseId) {
  return Labs.find({courseId: courseId, removed: false, $or: [{deadlineDate: {$exists: false}}, {deadlineDate: null}] });
}

Template.labListPage.helpers({
	countLabs: function(courseId) {
		return Labs.find({courseId: courseId, removed: false}).count();
	},
	getUnscheduledLabs: function(courseId) {
    return _getUnscheduledLabs(courseId);
  },
  countUnscheduledLabs: function(courseId) {
    return _getUnscheduledLabs(courseId).count();
  },
	getPastLabs: function(courseId) {
    return getPastLabs(courseId);
  },
  countPastLabs: function(courseId) {
    return getPastLabs(courseId).count();
  },
  getFutureLabs: function(courseId) {
		console.log("getFutureLabs("+courseId+")");
    return _getFutureLabs(courseId);
  },
  countFutureLabs: function(courseId) {
		console.log("countFutureLabs("+courseId+")");
    return _getFutureLabs(courseId).count();
  },
});
