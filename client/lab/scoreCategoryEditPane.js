Template.scoreCategoryEditPane.helpers({
  getRubricItems: function(rubricCategoryId) {
    return RubricItems.find({rubricCategoryId: rubricCategoryId, removed: {$ne: true}}, {sort: {index: 1}});
  },
  computePoints: function(rubricCategory, teamId) {
    console.log("Template.scoreCategoryEditPane.helpers:computePoints(", rubricCategory, teamId, ")");
    return computeCategoryPoints(rubricCategory, teamId);
  },
});

Template.scoreCategoryEditPane.events({
  'click .add-rubric-item-button': function(event, template) {
		console.log("click .add-rubric-item-button");
		var rubricCategory = this;
		Meteor.call("createRubricItem", rubricCategory._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
