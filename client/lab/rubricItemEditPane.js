Template.rubricItemEditPane.events({
  'submit .item-name-form': function(event, template) {
		console.log("submit .item-name-form");
		var rubricItem = this;
    var itemNameInput = template.$('.item-name-input');
    console.log(itemNameInput);
    var name = template.$('.item-name-input').val();
    console.log(name);
		Meteor.call("setRubricItemName", rubricItem._id, name, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
    // prevent default
		return false;
	},
  'blur .item-name-input': function(event, template) {
    console.log("blur .item-name-input");
    var rubricItem = this;
    var itemNameInput = template.$('.item-name-input');
    console.log(itemNameInput);
    var name = template.$('.item-name-input').val();
    console.log(name);
		Meteor.call("setRubricItemName", rubricItem._id, name, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'submit .item-points-form': function(event, template) {
		console.log("submit .item-points-form");
		var rubricItem = this;
    var itemPointsInput = template.$('.item-points-input');
    console.log(itemPointsInput);
    var points = template.$('.item-points-input').val();
    console.log(name);
		Meteor.call("setRubricItemPoints", rubricItem._id, points, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
    // prevent default
		return false;
	},
  'blur .item-points-input': function(event, template) {
    console.log("blur .item-points-input");
    var rubricItem = this;
    var itemPointsInput = template.$('.item-points-input');
    console.log(itemPointsInput);
    var points = template.$('.item-points-input').val();
    console.log(name);
		Meteor.call("setRubricItemPoints", rubricItem._id, points, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
});
