Template.scoreCategoryShowPane.helpers({
  getRubricItems: function(rubricCategoryId) {
    return RubricItems.find({rubricCategoryId: rubricCategoryId, removed: {$ne: true}}, {sort: {index: 1}});
  },
  computePoints: function(rubricCategory, teamId) {
    return computeCategoryPoints(rubricCategory, teamId);
  },
});
