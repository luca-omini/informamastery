Template.labItemContents.helpers({
	kindIs: function(k) {
		return this.kind===k;
	},
	typeIs: function(t) {
		return this.type===t;
	},
	labIsOpenOrClosed: function(itemId) {
		var item = this;
		var lab = Labs.findOne({_id: item.containerId});
		return lab.status=="open" || lab.status=="closed";
	},
	labHasTeam: function(itemId) {
		// do we have a lab team for this lab?
		var item = this;
		var lab = Labs.findOne({_id: item.containerId});
		var labTeam = LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId()});
		return labTeam;
	},
	hasTeamSubmission: function(itemId) {
		// do we have a submission for this item made by our current lab team?
		var item = this;
		var lab = Labs.findOne({_id: item.containerId});
		var labTeam = LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId()});
		if (labTeam) {
			return ContentItemSubmissions.find({itemId: itemId, labTeamId: labTeam._id}).count()>0;
		} else {
			return false;
		}
	},
	getTeamSubmission: function(itemId) {
		// get the submission for this item made by our current lab team
		var item = this;
		var lab = Labs.findOne({_id: item.containerId});
		var labTeam = LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId()});
		if (labTeam) {
			return ContentItemSubmissions.findOne({itemId: itemId, labTeamId: labTeam._id});
		} else {
			return undefined;
		}
	},
	hasReferenceSubmission: function(itemId) {
		return ContentItemSubmissions.find({itemId: itemId, labTeamId: null}).count()>0;
	},
	getReferenceSubmission: function(itemId) {
		return ContentItemSubmissions.findOne({itemId: itemId, labTeamId: null});
	},
	countOwnSubmissionsOutsideTeam: function(itemId) {
		// do we have submissions that have not been submitted by the current lab team?
		// (e.g., submissions submitted by a prior team, before lab teams got modified)
		var item = this;
		var lab = Labs.findOne({_id: item.containerId});
		var labTeam = LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId()});
		if (labTeam) {
			return ContentItemSubmissions.find({itemId: itemId, labTeamId: {$ne: labTeam._id}, userIds: Meteor.userId()}).count();
		} else {
			return ContentItemSubmissions.find({itemId: itemId, userIds: Meteor.userId()}).count();
		}
	},
	getOwnSubmissionsOutsideTeam: function(itemId) {
		// get the submissions that have not been submitted by the current lab team
		// (e.g., submissions submitted by a prior team, before lab teams got modified)
		var item = this;
		var lab = Labs.findOne({_id: item.containerId});
		var labTeam = LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId()});
		if (labTeam) {
			return ContentItemSubmissions.find({itemId: itemId, labTeamId: {$ne: labTeam._id}, userIds: Meteor.userId()});
		} else {
			return ContentItemSubmissions.find({itemId: itemId, userIds: Meteor.userId()});
		}
	},
	readonlyUnlessLabIsOpen: function() {
		//console.log("readonlyUnlessLabIsOpen()");
		var item = this;
		//console.log("item:", item);
		// makes this work only for content items sitting in Labs!!
		var lab = Labs.findOne({_id: item.containerId});
		//console.log("lab:", lab);
		if (lab) {
			if (lab.status=="open") {
				return "";
			} else {
				return "readonly";
			}
		} else {
			return "readonly";
		}
	},
});


Template.labItemContents.events({
	'blur .question-field': function(e, t) {
		console.log("Template.labItemContents.events blur .question-field");
		var itemId = this._id;
		var value = e.target.value;
		var item = this;

		// get the submission for this item made by our current lab team
		var lab = Labs.findOne({_id: item.containerId});
		var labTeam = LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId()});
		if (labTeam) {
			var submission = ContentItemSubmissions.findOne({itemId: itemId, labTeamId: labTeam._id});
			if ((!submission && value!="") || (submission && submission.value!=value)) {
				Meteor.call("saveContentItemSubmission", itemId, value, function(error, result) {
					if (error) {
						throwError(error.reason);
					} else {
						console.log("saved.");
					}
				});
			} else {
				console.log("nothing changed, won't save.")
			}
		}
	},
	'blur .reference-question-field': function(e, t) {
		console.log("Template.labItemContents.events blur .reference-question-field");
		var itemId = this._id;
		var value = e.target.value;
		var item = this;

		var submission = ContentItemSubmissions.findOne({itemId: itemId, labTeamId: null, userIds: null});
		if ((!submission && value!="") || (submission && submission.value!=value)) {
			Meteor.call("saveReferenceContentItemSubmission", itemId, value, function(error, result) {
				if (error) {
					throwError(error.reason);
				} else {
					console.log("saved.");
				}
			});
		} else {
			console.log("nothing changed, won't save.")
		}
	},
});
