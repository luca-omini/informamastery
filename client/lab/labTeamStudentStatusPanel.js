Template.labTeamStudentStatusPanel.helpers({
  isPreviewOrOpenOrClosed: function(status) {
    return status=="preview" || status=="open" || status=="closed";
  },
	getTeam: function(lab) {
    return LabTeams.findOne({labId: lab._id, memberIds: Meteor.userId(), removed: {$ne: true}});
  },
  moreThanOneMember: function() {
    return this.memberIds.length>1;
  },
});
