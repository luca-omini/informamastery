var pickadateSubmitFormat = "mmmm d, yyyy";
var momentDateParseFormat = "MMMM D, YYYY";
var pickadateDisplayFormat = "dddd mmmm d, yyyy"

var pickatimeSubmitFormat = "HH:i";
var momentTimeParseFormat = "HH:mm";
var pickatimeDisplayFormat = "HH:i";

Template.labEditPage.rendered = function() {
	var self = this;
	console.log("Template.labEditPage.rendered", self);
	$('.datepicker').pickadate({
    formatSubmit: pickadateSubmitFormat,
    format: pickadateDisplayFormat,
    onSet: function(thingSet) {
      //console.log('Set stuff:', thingSet);
    },
	});
	$('.timepicker').pickatime({
		formatSubmit: pickatimeSubmitFormat,
		format: pickatimeDisplayFormat,
		interval: 5,
	});
	//$('.ui.checkbox').checkbox();
	this.$('.add-topic-dropdown').dropdown({
		onChange: function(value, text, selectedItem) {
			//console.log("changed. value="+value+", text="+text+", selectedItem="+selectedItem);
			var labId = self.data.lab._id;
			var topicId = value;
			//console.log("this:", this);
			Meteor.call('addLabTopic', labId, topicId, function(error, result) {
				$('.add-topic-dropdown').dropdown("restore defaults");
				// for some reason the above attaches the default class, making the text grey
				$('.add-topic-dropdown span.text.default').removeClass("default");
				if (error) {
					throwError(error.reason);
				}
			});
		},
	});
	this.$('.insert-item-in-front-dropdown').dropdown();
	// Set up JQuery UI's sortable functionality
	this.$('#items').sortable({
		stop: function(e, ui) {
			// get the dragged html element and the one before and after it
			var el = ui.item.get(0)
			var before = ui.item.prev().get(0)
			var after = ui.item.next().get(0)
			var newIndex;

			// Blaze.getData takes as a parameter an html element
			// and returns the data context that was bound when that html element was rendered
			if (!before) {
				// if it was dragged into the first position grab the
				// next element's data context and subtract one from the position
				newIndex = Blaze.getData(after).index - 1;
			} else if (!after) {
				// if it was dragged into the last position grab the
				// previous element's data context and add one to the position
				newIndex = Blaze.getData(before).index + 1;
			} else {
				// else take the average of the two positions of the previous
				// and next elements
				newIndex = (Blaze.getData(after).index + Blaze.getData(before).index) / 2;
			}
			// update the dragged Item's position
			var contentItemId = Blaze.getData(el)._id;
			Meteor.call('updateContentItemPosition', contentItemId, newIndex, function(error, result) {
				if (error) {
					throwError(error.reason);
				}
			});
		}
	})
};

Template.labEditPage.events({
	'submit .update-form': function(event, template) {
		var courseId = this.course._id;
		var labId = this.lab._id;
		var title = event.target.title.value;
		var description = event.target.description.value;

		var deadlineDate = null;
		var deadlineDateMoment = moment(event.target.deadlineDate_submit.value, momentDateParseFormat);
		console.log(deadlineDateMoment);
		var deadlineTimeMoment = moment(event.target.deadlineTime_submit.value, momentTimeParseFormat);
		console.log(deadlineTimeMoment);

		if (deadlineDateMoment.isValid() && deadlineTimeMoment.isValid()) {
	    var deadlineDateDate = deadlineDateMoment.toDate();
			console.log(deadlineDateDate);

	    var deadlineTimeDate = deadlineTimeMoment.toDate();
			console.log(deadlineTimeDate);

			var deadlineMoment = deadlineDateMoment;
			deadlineMoment.minutes(deadlineTimeMoment.minutes());
			deadlineMoment.hours(deadlineTimeMoment.hours());
			console.log(deadlineMoment);
			deadlineDate = deadlineMoment.toDate();
		}
		console.log(deadlineDate);

		var groupSize = event.target.groupSize.value;

		Meteor.call('updateLab', labId, title, description, deadlineDate, groupSize, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("labPage", {courseId: courseId, labId: labId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .insert-item-in-front': function(event, template) {
		console.log("click .insert-item-in-front");
		var containerId = this.lab._id;
		console.log("event:");
		console.log(event);
		var target = $(event.target);
		console.log("target:");
		console.log(target);
		var kind = target.attr("data-kind");
		console.log("kind:");
		console.log(kind);

		// If there are no other items, set index to 0
		var newIndex;
		if (this.contentItems.count()>0) {
			newIndex = this.contentItems.fetch()[0].index-1;
		} else {
			newIndex = 0;
		}
		Meteor.call("addContentItem", containerId, "Labs", kind, newIndex, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
	'click .remove-topic-button': function(event, template) {
		event.preventDefault();
		console.log("click .remove-topic-button");
		var topicId = this._id;
		console.log("this: ", this);
		console.log("Template.instance(): ", Template.instance());
		console.log("Template.currentData(): ", Template.currentData());
		console.log("Template.parentData(): ", Template.parentData());
		console.log("Template.parentData(2): ", Template.parentData(2));
		var labId = Template.currentData().lab._id;
		Meteor.call("removeLabTopic", labId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});


Template.labEditPage.helpers({
	getTopics: function(topicIds) {
		if (!topicIds) {
			topicIds = [];
		}
		return Topics.find({_id: {$in: topicIds}});
	},
	getCourseTopics: function(courseId) {
		return Topics.find({courseId: courseId, removed: {$ne: true}});
	},
});
