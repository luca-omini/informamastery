Template.labListEditPage.events({
	'submit .form': function(event, template) {
		var courseId = this.course._id;
		var introduction = event.target.introduction.value;
		Meteor.call('updateLabListIntroduction', courseId, introduction, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("labListPage", {courseId: courseId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .add-lab-button': function(ev, template) {
		var courseId = this.course._id;
		Meteor.call('addLab', courseId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("labEditPage", {courseId: courseId, labId: result});
			}
		});
	},
	'click .remove-lab-button': function(ev, template) {
		var labId = this._id;
		Meteor.call('removeLab', labId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
