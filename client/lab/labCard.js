Template.labCard.helpers({
	countQuestionItems: function(labId) {
    //TODO: Shouldn't this also count fileSubmissions (not just questions)?
		return ContentItems.find({containerId: labId, kind: "question", removed: {$ne: true}}).count();
	},
	countOwnSubmissions: function(labId) {
    //TODO: Shouldn't this also count fileSubmissions (not just questions)?
		var itemIds = ContentItems.find({containerId: labId, kind: "question", removed: {$ne: true}}).map(function (item) {return item._id;});
		return ContentItemSubmissions.find({itemId: {$in: itemIds}, userIds: Meteor.userId()}).count();
	},
});
