Template.labItem.helpers({
	hasOwnBookmark: function(itemId) {
		return Annotations.find({contentItemId: itemId, userId: Meteor.userId(), kind: "bookmark", removed: false}).count()>0;
	},
	getOwnAnnotations: function(itemId) {
		return Annotations.find({contentItemId: itemId, userId: Meteor.userId(), removed: false});
	},
	getOtherQuestions: function(itemId) {
		return Annotations.find({contentItemId: itemId, userId: {$ne: Meteor.userId()}, kind: "question", removed: false});
	},
	hasAnnotationsToDisplay: function(itemId) {
		var count =
			Annotations.find({contentItemId: itemId, userId: Meteor.userId(), removed: false}).count()+
			Annotations.find({contentItemId: itemId, userId: {$ne: Meteor.userId()}, kind: "question", removed: false}).count();
		console.log("Template.labItem.helpers..hasAnnotationsToDisplay("+itemId+") -> "+count);
		return count;
	},
});


Template.labItem.events({
});
