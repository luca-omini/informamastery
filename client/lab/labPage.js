Template.labPage.helpers({
	getTopics: function(topicIds) {
		topicIds = topicIds || [];
		return Topics.find({_id: {$in: topicIds}}, {sort: {masteryRequiredForGrade: 1}});
	},
	getTheme: function(themeId) {
		return Themes.findOne({_id: themeId});
	},
	getItems: function(labId) {
		return ContentItems.find({containerId: labId, removed: {$ne: true}}, {sort: {index: 1}});
	},
	kindIs: function(k) {
		return this.kind===k;
	},
	getNumberOfQuestionItems: function(labId) {
    //TODO: Shouldn't we also count fileSubmission items?
		return ContentItems.find({containerId: labId, kind: "question", removed: {$ne: true}}).count();
	},
	getNumberOfOwnSubmissions: function(labId) {
    //TODO: Shouldn't we also count fileSubmission tiems?
		var itemIds = ContentItems.find({containerId: labId, kind: "question", removed: {$ne: true}}).map(function (item) {return item._id;});
		return ContentItemSubmissions.find({itemId: {$in: itemIds}, userIds: Meteor.userId()}).count();
	},
});


Template.labPage.events({
});
