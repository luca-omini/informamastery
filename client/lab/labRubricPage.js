Template.labRubricPage.helpers({
  getQuestionItems: function(labId) {
		return ContentItems.find({containerId: labId, $or: [{kind: "question"}, {kind: "fileSubmission"}], removed: {$ne: true}}, {sort: {index: 1}});
	},
});
