Template.userReadingPage.helpers({
	getBooks: function(courseId) {
		return Books.find({courseId: courseId, removed: {$ne: true}});
	},
  getChapters: function(bookId) {
    return Chapters.find({bookId: bookId, removed: {$ne: true}}, {sort: {sortKey: 1}});
	},
  getSections: function(chapterId) {
    return Sections.find({chapterId: chapterId, removed: {$ne: true}}, {sort: {sortKey: 1}});
	},
  countCourseSections: function(courseId) {
    //TODO: when removing books or chapters, also mark all their sections as removed
		return Sections.find({courseId: courseId, removed: {$ne: true}}).count();
	},
  countBookSections: function(bookId) {
    //TODO: when removing chapters, also mark all their sections as removed
		return Sections.find({bookId: bookId, removed: {$ne: true}}).count();
	},
  countChapterSections: function(chapterId) {
    return Sections.find({chapterId: chapterId, removed: {$ne: true}}).count();
  },
  hasUserSectionStatus: function(sectionId, userId) {
		var result = SectionStatus.find({sectionId: sectionId, userId: userId}).count()>0;
    return result;
	},
	getUserSectionStatus: function(sectionId, userId) {
		var result = SectionStatus.findOne({sectionId: sectionId, userId: userId});
    return result;
	},
  countUserChapterSectionStatuses: function(chapterId, userId) {
		return SectionStatus.find({chapterId: chapterId, userId: userId}).count();
	},
  countUserBookSectionStatuses: function(bookId, userId) {
    return SectionStatus.find({bookId: bookId, userId: userId}).count();
  },
  countUserCourseSectionStatuses: function(courseId, userId) {
    return SectionStatus.find({courseId: courseId, userId: userId}).count();
  },
});
