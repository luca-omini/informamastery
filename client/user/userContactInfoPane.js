Template.userContactInfoPane.helpers({
	hasAnyContactInfo: function(user) {
		return user.profile && (
      user.profile.skypeName ||
      user.profile.faceTimeId ||
      user.profile.gmailAddress
    );
	},
});
