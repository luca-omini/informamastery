_findUserUnsolvedPracticeProblems = function(userId) {
	var solvedPracticeProblemIds = PracticeProblemSolutions.find({userId: userId}).map(function(practiceProblemSolution) {return practiceProblemSolution.practiceProblemId;});
	return PracticeProblems.find({userId: {$ne: userId}, _id: {$nin: solvedPracticeProblemIds}, published:true, archived:false});
}

Template.userPracticeProblemUnsolvedListPage.helpers({
	findUserUnsolvedPracticeProblems: function(userId) {
		return _findUserUnsolvedPracticeProblems(userId);
	},
	countUserUnsolvedPracticeProblems: function(userId) {
		return _findUserUnsolvedPracticeProblems(userId).count();
	},
	makeSettings: function() {
		return {
			rowsPerPage: 50,
			class: "ui compact table",
			fields: [
				{
					key: "publishedDate",
					label: "Published",
					sort: "descending",
					tmpl: Template.practiceProblemPublishedDate,
					headerClass: "collapsing",
					cellClass: "left aligned",
				},
				{
					key: "topic",
					label: "Topic",
					cellClass: "left aligned",
					fn: function(value, object) {
						if (object) {
							var topic = Topics.findOne({_id: object.topicId});
							return topic?topic.title:"";
						} else {
							return "";
						}
					},
					tmpl: Template.practiceProblemTopic,
				},
				{
					key: "title",
					label: "Title",
					cellClass: "left aligned",
					tmpl: Template.clickablePracticeProblemTitle,
				},
				{
					key: "solutions",
					label: "Solutions",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function(value, object) {return _countPracticeProblemSolutions(object._id)},
					tmpl: Template.practiceProblemSolutionCount,
				},
				{
					key: "difficulty",
					label: "Difficulty",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function (value, object) {return _computeAverageDifficultyRating(object._id)},
					tmpl: Template.practiceProblemDifficultyRating,
				},
				{
					key: "quality",
					label: "Quality",
					headerClass: "collapsing",
					cellClass: "right aligned",
					fn: function (value, object) {return _computeAverageQualityRating(object._id)},
					tmpl: Template.practiceProblemQualityRating,
				},
			],
		};
	},
});
