Template.userQuestionListPage.helpers({
  countUserQuestionsForCourse: function(userId, courseId) {
    var count = Annotations.find({courseId: courseId, userId: userId, kind: "question", removed: {$ne: true}}).count();
    return count;
	},
  getUserQuestionsForCourse: function(userId, courseId) {
    return Annotations.find({courseId: courseId, userId: userId, kind: "question", removed: {$ne: true}});
	},
	getOpenQuestionsForCourse: function(courseId) {
    //console.log("getOpenQuestionsForCourse("+courseId+")");
    var closedAnnotationIds = [];
    AnnotationResponses.find({courseId: courseId, accepted: true, removed: {$ne: true}}).forEach(function(annotationResponse) {closedAnnotationIds.push(annotationResponse.annotationId);});
    //console.log(closedAnnotationIds);
    return Annotations.find({courseId: courseId, _id: {$nin: closedAnnotationIds}, kind: "question", removed: {$ne: true}}, {sort: {modificationDate: -1}});
	},
  containerIs: function(itemId, collectionName) {
		return ContentItems.find({_id: itemId, containerCollection: collectionName}).count()>0;
	},
  countAnswers: function(annotationId) {
    return AnnotationResponses.find({annotationId: annotationId, removed: {$ne: true}}).count();
  },
  haveUpvoted: function(question) {
    return question.upvoteUserIds && _.contains(question.upvoteUserIds, Meteor.userId());
  },
  haveDownvoted: function(question) {
    return question.downvoteUserIds && _.contains(question.downvoteUserIds, Meteor.userId());
  },
  haveNotVoted: function(question) {
    return ( !question.upvoteUserIds || !_.contains(question.upvoteUserIds, Meteor.userId()) )
    && ( !question.downvoteUserIds || !_.contains(question.downvoteUserIds, Meteor.userId()) );
  },
});

Template.userQuestionListPage.events({
  'click .upvote-question-button': function(event, template) {
		console.log("Template.userQuestionListPage.events click .upvote-question-button");
		var annotationId = this._id;
		Meteor.call("upvoteQuestion", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .downvote-question-button': function(event, template) {
		console.log("Template.userQuestionListPage.events click .downvote-question-button");
		var annotationId = this._id;
		Meteor.call("downvoteQuestion", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
	'click .clearvote-question-button': function(event, template) {
		console.log("Template.userQuestionListPage.events click .clearvote-question-button");
		var annotationId = this._id;
		Meteor.call("clearvoteQuestion", annotationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				console.log("done.");
			}
		});
	},
});
