Template.adminUserPage.helpers({
	courses: function() {
		return Courses.find({});
	},
});


Template.adminUserPage.events({
	'click .impersonate': function() {
		// See: https://dweldon.silvrback.com/impersonating-a-user
		var userId = this._id;
		console.log("Impersonating user with id: "+userId);
		Meteor.call('impersonate', userId, function(err) {
			if (!err) {
				Session.set('impersonating', true);
				Meteor.connection.setUserId(userId);
				Router.go('home');
			}
		});
	},
});
