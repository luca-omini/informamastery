var INITIAL_LIMIT = 20;
var LIMIT_INCREMENT = 10;

incrementUserFeedLengthLimit = function() {
	console.log("incrementUserFeedLengthLimit");
	newLimit = Session.get('userFeedLengthLimit') + INITIAL_LIMIT;
	Session.set('userFeedLengthLimit', newLimit);
};


Template.userFeedPage.created = function() {
	console.log("Template.userFeedPage.created");
	Session.setDefault('userFeedLengthLimit', LIMIT_INCREMENT);
};


Template.userFeedPage.rendered = function() {
	console.log("Template.userFeedPage.rendered");
	console.log("Template.userFeedPage.rendered: this=", this);
	var self = this;
	// Deps.autorun() automatically rerun the subscription whenever Session.get('userFeedLengthLimit') changes
	// http://docs.meteor.com/#deps_autorun
	Deps.autorun(function() {
		console.log("Template.userFeedPage.rendered: Deps.autorun function");
		var course = Session.get("course");
		if (self.data) {
			var userId = self.data.user._id;
			console.log("### Subscribing to 'user-feed' (courseId="+course._id+", userId="+userId+", limit="+Session.get('userFeedLengthLimit')+")");
			Meteor.subscribe('user-feed', course._id, userId, Session.get('userFeedLengthLimit'));
		}
	});
	// is triggered every time we scroll
	$(window).scroll(function() {
		if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
			//TODO: Don't do this unconditionally! Ensure we show this page in the router!
			//incrementUserFeedLengthLimit();
		}
	});
};


Template.userFeedPage.helpers({
	getEvents: function(courseId, userId) {
		console.log("Template.userFeedPage.helpers.getEvents("+courseId+", "+userId+")");
		return FeedEvents.find({courseId: courseId, userId: userId}, {sort: {date: -1}, limit: Session.get('userFeedLengthLimit')});
	},
	hasMoreEvents: function(courseId, userId) {
		// If, once the subscription is ready, we have less events than we asked for,
		// we've got all the events in the feed.
		return !(FeedEvents.find({courseId: courseId, userId: userId}).count() < Session.get("userFeedLengthLimit"));
	},
});


Template.userFeedPage.events({
	'click .more-events-button': function(evt) {
		console.log("Template.userFeedPage.events click .more-events-button");
		incrementUserFeedLengthLimit();
	},
});
