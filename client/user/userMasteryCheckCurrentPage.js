Template.userMasteryCheckCurrentPage.rendered = function() {
  var self = this;
	console.log("Template.userMasteryCheckCurrentPage.rendered", self);
  this.$('.lab-dropdown').dropdown();
};

Template.userMasteryCheckCurrentPage.helpers({
  getLabsWithTeams: function(courseId, userId) {
    var labIds = LabTeams.find({courseId: courseId, memberIds: userId}).map(function(labTeam) {return labTeam.labId});
    console.log("labIds: ", labIds)
    return Labs.find({courseId: courseId, _id: {$in: labIds}});
  },
  getLabTeam: function(labId, userId) {
    console.log("getLabTeam("+labId+", "+userId+")");
    return LabTeams.findOne({labId: labId, memberIds: userId, removed: {$ne: true}});
  },
  moreThanOneElements: function(array) {
    return array.length>1;
  },
  getMasteryCheckRegistration: function(courseId, userId) {
    return MasteryCheckRegistrations.findOne({courseId: courseId, studentIds: userId});
  },
  getTopics: function(topicIds) {
    if (topicIds) {
      return Topics.find({topicId: {$in: topicIds}});
    } else {
      return null;
    }
  },
  isRegistrationEnqueued: function(masteryCheckRegistrationId) {
    return MasteryCheckQueueEntries.find({masteryCheckRegistrationId: masteryCheckRegistrationId}).count()>0;
  },
  doesRegistrationHaveCheck: function(masteryCheckRegistrationId) {
    return MasteryChecks.find({masteryCheckRegistrationId: masteryCheckRegistrationId}).count()>0;
  },
  getRegistrationCheck: function(masteryCheckRegistrationId) {
    return MasteryChecks.findOne({masteryCheckRegistrationId: masteryCheckRegistrationId});
  },
  isSessionOngoing: function(courseId) {
    console.log("isSessionOngoing("+courseId+")");
    return MasteryCheckSessions.findOne({courseId: courseId, ongoing: true});
  },
});

Template.userMasteryCheckCurrentPage.events({
  'submit .form': function(event, template) {
		event.preventDefault();
    console.log("this", this);
    var courseId = this.course._id;
    console.log("courseId", courseId);
    var labId = $(".lab-input").val();
    console.log("labId", labId);
    Meteor.call('createMasteryCheckRegistration', courseId, labId, function(error, result) {
      if (error) {
        throwError(error.reason);
      }
    });
  },
  'click .remove-topic-button': function(ev, template) {
    console.log("remove-topic-button clicked");
		var topicId = ""+this;
    console.log("topicId", topicId);
    var reg = MasteryCheckRegistrations.findOne({studentIds: Meteor.userId()});
    var masteryCheckRegistrationId = reg._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
    Meteor.call('removeTopicFromMasteryCheckRegistration', masteryCheckRegistrationId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'click .delete-mastery-check-registration-button': function(ev, template) {
    console.log("delete-mastery-check-registration-button clicked");
		var masteryCheckRegistrationId = this._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
		Meteor.call('deleteMasteryCheckRegistration', masteryCheckRegistrationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .enqueue-mastery-check-registration-button': function(ev, template) {
    console.log("enqueue-mastery-check-registration-button clicked");
		var masteryCheckRegistrationId = this._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
		Meteor.call('enqueueMasteryCheckRegistration', masteryCheckRegistrationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },
  'click .dequeue-mastery-check-registration-button': function(ev, template) {
    console.log("dequeue-mastery-check-registration-button clicked");
		var masteryCheckRegistrationId = this._id;
    console.log("masteryCheckRegistrationId", masteryCheckRegistrationId);
		Meteor.call('dequeueMasteryCheckRegistration', masteryCheckRegistrationId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
  },

});
