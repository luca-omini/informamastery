Template.userGravatarLarge.helpers({
	gravatarUrl: function(user) {
		console.log("gravatarUrl(",user,")");
		return Gravatar.imageUrl(user.emails[0].address, {size: 200, secure: window.location.protocol==="https:"});
	},
	gravatarProfileUrl: function(user) {
		return "http://www.gravatar.com/"+Gravatar.hash(user.emails[0].address);
	},
});
