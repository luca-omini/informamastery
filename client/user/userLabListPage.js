function _getPastLabs(courseId) {
  // labs which are closed and have a deadlineDate that's earlier than now
  var now = new Date();
  return Labs.find({courseId: courseId, removed: false, status: "closed", deadlineDate: {$lt: now}}, {sort: {deadlineDate: 1}});
}


Template.userLabListPage.helpers({
  getPastLabs: function(courseId) {
    return _getPastLabs(courseId);
  },
  countPastLabs: function(courseId) {
    return _getPastLabs(courseId).count();
  },
  countPastLabsTaken: function(courseId, userId) {
    var pastLabs = _getPastLabs(courseId);
    var labsTaken = 0;
    pastLabs.forEach(function(lab) {
      if (LabTeams.find({labId: lab._id, memberIds: userId}).count()>0) {
        labsTaken++;
      }
    });
    return labsTaken;
  },
  getLabTeam: function(labId, userId) {
    // assume there is just one...
    var team = LabTeams.findOne({labId: labId, memberIds: userId});
    return team;
  },
  getLabFeedback: function(labId, userId) {
    var feedback = LabFeedbacks.findOne({labId: labId, userId: userId});
    return feedback;
  },
  getStudentRegistrationForCourseIdAndUserId: function(courseId, userId) {
    return StudentRegistrations.findOne({courseId: courseId, studentId: userId});
  },
  computeOverallScore: function(labId, labTeamId) {
    return computeOverallScore(labId, labTeamId);
  },
  computeMaxOverallScore: function(labId) {
    return computeMaxOverallScore(labId);
  },
});
