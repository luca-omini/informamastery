Template.userSkillListPage.helpers({
  countSkills: function(courseId) {
    return Skills.find({courseId: courseId}).count();
  },
  countRedSkills: function(courseId, userId) {
    return SkillStatus.find({courseId: courseId, studentId: userId, status: "Red"}).count();
  },
  countAmberSkills: function(courseId, userId) {
    return SkillStatus.find({courseId: courseId, studentId: userId, status: "Amber"}).count();
  },
  countGreenSkills: function(courseId, userId) {
    return SkillStatus.find({courseId: courseId, studentId: userId, $or: [{ready: true}, {status: "Green"}]}).count();
  },
  getTopicsForCourse: function(courseId) {
    var ts = Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}} );
    return ts;
  },
  getSkillsForTopic: function(topicId) {
    var ss = Skills.find({topicId: topicId, removed: {$ne: true}} );
    return ss;
  },
  countSkillsForTopic: function(topicId) {
    var ssc = Skills.find({topicId: topicId, removed: {$ne: true}} ).count();
    return ssc;
  },
  isSkillRed: function(skillId, userId) {
		var skillStatus = SkillStatus.findOne({studentId: userId, skillId: skillId});
		return skillStatus?skillStatus.status=="Red":false;
	},
  isSkillAmber: function(skillId, userId) {
		var skillStatus = SkillStatus.findOne({studentId: userId, skillId: skillId});
		return skillStatus?skillStatus.status=="Amber":false;
	},
  isSkillGreen: function(skillId, userId) {
		var skillStatus = SkillStatus.findOne({studentId: userId, skillId: skillId});
    // skillStatus.ready implies skillStatus.status=="Green"
    return skillStatus?skillStatus.ready:false;
	},
});

Template.userSkillListPage.events({
	'click .set-red-button': function(ev, template) {
    var userId = Template.currentData().user._id;
		var skillId = this._id;
		console.log(".set-red-button clicked. userId: "+userId+", skillId: "+skillId);
		Meteor.call('setUserSkillStatusRed', userId, skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-amber-button': function(ev, template) {
    var userId = Template.currentData().user._id;
		var skillId = this._id;
		console.log(".set-amber-button clicked. userId: "+userId+", skillId: "+skillId);
		Meteor.call('setUserSkillStatusAmber', userId, skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .set-green-button': function(ev, template) {
    var userId = Template.currentData().user._id;
		var skillId = this._id;
		console.log(".set-green-button clicked. userId: "+userId+", skillId: "+skillId);
		Meteor.call('setUserSkillStatusGreen', userId, skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-button': function(ev, template) {
    var userId = Template.currentData().user._id;
		var skillId = this._id;
		console.log(".clear-button clicked. userId: "+userId+", skillId: "+skillId);
		Meteor.call('setUserSkillStatusCleared', userId, skillId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
