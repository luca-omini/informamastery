Template.userLabFeedbackPage.helpers({
  getStudentRegistrationForCourseIdAndUserId: function(courseId, userId) {
    return StudentRegistrations.findOne({courseId: courseId, studentId: userId});
  },
  getField: function(object, fieldName) {
    //console.log("getField", object, fieldName);
    if (object) {
      return object[fieldName];
    } else {
      return undefined;
    }
  },
  qualityIs: function(object, value) {
    //console.log("qualityIs", object, value);
    if (object) {
      return object.collaborationQuality==value;
    } else {
      return undefined;
    }
  },
});

function setLabFeedback(lab, labFeedback, key, value) {
  if ((!labFeedback && value!="") || (labFeedback && labFeedback[key]!=value)) {
    console.log("value for '"+key+"' changed, new value is '"+value+"', need to save");
    Meteor.call("setLabFeedback", lab._id, key, value, function(error, result) {
      if (error) {
        throwError(error.reason);
      } else {
        console.log("saved.");
      }
    });
  }
}

Template.userLabFeedbackPage.events({
  'click .collaborationWorkedWell': function(e, t) {
    console.log("Template.userLabFeedbackPage.events click .collaborationWorkedWell");
    setLabFeedback(this.lab, this.labFeedback, "collaborationQuality", "collaborationWorkedWell");
  },
  'click .collaborationWorkedMostlyFine': function(e, t) {
    console.log("Template.userLabFeedbackPage.events click .collaborationWorkedMostlyFine");
    setLabFeedback(this.lab, this.labFeedback, "collaborationQuality", "collaborationWorkedMostlyFine");
  },
  'click .collaborationDidNotWorkWell': function(e, t) {
    console.log("Template.userLabFeedbackPage.events click .collaborationDidNotWorkWell");
    setLabFeedback(this.lab, this.labFeedback, "collaborationQuality", "collaborationDidNotWorkWell");
  },
  'blur .teamworkPositive': function(e, t) {
		console.log("Template.userLabFeedbackPage.events blur .teamworkPositive");
    setLabFeedback(this.lab, this.labFeedback, "teamworkPositive", e.target.value);
  },
  'blur .teamworkNegative': function(e, t) {
		console.log("Template.userLabFeedbackPage.events blur .teamworkNegative");
    setLabFeedback(this.lab, this.labFeedback, "teamworkNegative", e.target.value);
  },
  'blur .teamworkSelfImprovement': function(e, t) {
		console.log("Template.userLabFeedbackPage.events blur .teamworkSelfImprovement");
    setLabFeedback(this.lab, this.labFeedback, "teamworkSelfImprovement", e.target.value);
  },
  'blur .teamworkGradeCompensation': function(e, t) {
		console.log("Template.userLabFeedbackPage.events blur .teamworkGradeCompensation");
    setLabFeedback(this.lab, this.labFeedback, "teamworkGradeCompensation", e.target.value);
  },
  'blur .labPositive': function(e, t) {
		console.log("Template.userLabFeedbackPage.events blur .labPositive");
    setLabFeedback(this.lab, this.labFeedback, "labPositive", e.target.value);
  },
  'blur .labNegative': function(e, t) {
		console.log("Template.userLabFeedbackPage.events blur .labNegative");
    setLabFeedback(this.lab, this.labFeedback, "labNegative", e.target.value);
  },
});
