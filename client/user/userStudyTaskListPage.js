Template.userStudyTaskListPage.helpers({
  countStudyTasks: function(courseId) {
    return StudyTasks.find({courseId: courseId}).count();
  },
  countXStudyTasks: function(courseId, userId) {
    return StudyTasks.find({courseId: courseId}).count(); //TODO
  },
  getTopicsForCourse: function(courseId) {
    var ts = Topics.find({courseId: courseId, removed: {$ne: true}}, {sort: {masteryRequiredForGrade: 1}} );
    return ts;
  },
  getStudyTasksForTopic: function(topicId) {
    var ss = StudyTasks.find({topicId: topicId, removed: {$ne: true}} );
    return ss;
  },
  countStudyTasksForTopic: function(topicId) {
    var ssc = StudyTasks.find({topicId: topicId, removed: {$ne: true}} ).count();
    return ssc;
  },
});
