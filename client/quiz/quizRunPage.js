Template.quizRunPage.helpers({
	getStudentUsers: function(courseId) {
		var studentIds = StudentRegistrations.find({courseId: courseId, withdrawn: false}).map(function(studentRegistration) {return studentRegistration.studentId;});
		return Meteor.users.find({_id: {$in: studentIds}}, {sort: {"profile.firstName": 1}});
	},
	countStudents: function(courseId) {
		return StudentRegistrations.find({courseId: courseId, withdrawn: false}).count();
	},
	countFreshAttempts: function(quizId, quizStartDate) {
		return QuizAttempts.find({quizId: quizId, quizStartDate: quizStartDate, fresh: true}).count();
	},
	countOngoingAttempts: function(quizId, quizStartDate) {
		return QuizAttempts.find({quizId: quizId, quizStartDate: quizStartDate, ongoing: true}).count();
	},
	countFinishedAttempts: function(quizId, quizStartDate) {
		return QuizAttempts.find({quizId: quizId, quizStartDate: quizStartDate, finished: true}).count();
	},
});
Template.quizRunPage.events({
	'click .stop-quiz-button': function(ev, template) {
		var courseId = this.course._id;
		var quizId = this.quiz._id;
		Meteor.call('stopQuiz', quizId, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("quizPage", {courseId: courseId, quizId: quizId});
			}
		});
	},
});
