Template.quizRunStudentCard.helpers({
	gravatarUrl: function(user) {
		return Gravatar.imageUrl(user.emails[0].address, {size: 200, secure: window.location.protocol==="https:"});
	},
  getQuizAttempt: function(userId, quizId, quizStartDate) {
    console.log("getQuizAttempt("+userId+", "+quizId+", "+quizStartDate+"):");
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, quizStartDate: quizStartDate, studentId: userId});
    console.log(quizAttempt);
    return quizAttempt;
  },
});
Template.quizRunStudentCard.events({
	'click .add-to-quiz-button': function(ev, template) {
		var courseId = this.courseId;
		var quizId = this.quiz._id;
    var studentId = this.user._id;
		Meteor.call('createQuizAttempt', courseId, quizId, studentId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
