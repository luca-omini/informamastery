Template.quizEditPageUsePracticeProblem.helpers({
  isPracticeProblemInQuiz: function() {
    //console.log("isPracticeProblemInQuiz()");
    var practiceProblem = Template.currentData();
    //console.log("practiceProblem: ", practiceProblem);
    // HACK:
    // We assume we sit inside a reactiveTable.
    // We assume our context is a PracticeProblem.
    // We have to skip over a whole bunch of reactive-table-specific parent contexts
    // to find the context surrounding the reactive table,
    // which is the context that contains the course and quiz keys,
    // the context of the quizEditPage template.
    /*
    console.log("context:", Template.currentData());
    console.log("parentContext(0)==context:", Template.parentData(0));
    console.log("parentContext(1):", Template.parentData(1));
    console.log("parentContext(2):", Template.parentData(2));
    console.log("parentContext(3):", Template.parentData(3));
    console.log("parentContext(4):", Template.parentData(4));
    console.log("parentContext(5):", Template.parentData(5));
    console.log("parentContext(6):", Template.parentData(6));
    */
    var quiz = Template.parentData(6).quiz;
    //console.log("quiz: ", quiz);
    return _.contains(quiz.practiceProblemIds, practiceProblem._id);
  },
});


Template.quizEditPage.events({
	'click .set-use-button': function(event, template) {
		event.preventDefault();
		console.log("click .set-use-button");
    /*
  	console.log("context:", Template.currentData());
    console.log("parentContext(0)==context:", Template.parentData(0));
    console.log("parentContext(1):", Template.parentData(1));
    console.log("parentContext(2):", Template.parentData(2));
    console.log("parentContext(3):", Template.parentData(3));
    console.log("parentContext(4):", Template.parentData(4));
    console.log("parentContext(5):", Template.parentData(5));
    console.log("parentContext(6):", Template.parentData(6));
    */
    // this seems to point to the practice problem (but template.currentData() doesn't!)
		var practiceProblem = this;
		console.log("practiceProblem: ", practiceProblem);
    //HACK: find quiz in current context (weird!)
		var quiz = Template.currentData().quiz;
    console.log("quiz: ", quiz);
		Meteor.call("addQuizPracticeProblem", quiz._id, practiceProblem._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .clear-use-button': function(event, template) {
		event.preventDefault();
		console.log("click .clear-use-button");
    /*
  	console.log("context:", Template.currentData());
    console.log("parentContext(0)==context:", Template.parentData(0));
    console.log("parentContext(1):", Template.parentData(1));
    console.log("parentContext(2):", Template.parentData(2));
    console.log("parentContext(3):", Template.parentData(3));
    console.log("parentContext(4):", Template.parentData(4));
    console.log("parentContext(5):", Template.parentData(5));
    console.log("parentContext(6):", Template.parentData(6));
    */
    // this seems to point to the practice problem (but template.currentData() doesn't!)
		var practiceProblem = this;
		console.log("practiceProblem: ", practiceProblem);
    //HACK: find quiz in current context (weird!)
		var quiz = Template.currentData().quiz;
    console.log("quiz: ", quiz);
		Meteor.call("removeQuizPracticeProblem", quiz._id, practiceProblem._id, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});
