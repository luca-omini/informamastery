Template.quizTakeProblem.events({
  'click .choice-toggle-button': function (event) {
    console.log(event);
    console.log(event.currentTarget);
    if ($(event.currentTarget).hasClass("active")) {
      $(event.currentTarget).removeClass("active");
      $(event.currentTarget).html("<i class='minus icon'></i> Wrong");
    } else {
      $(event.currentTarget).addClass("active");
      $(event.currentTarget).html("<i class='checkmark icon'></i> Correct");
    }
  },
});
