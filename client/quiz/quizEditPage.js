var pickadateSubmitFormat = "mmmm d, yyyy";
var momentParseFormat = "MMMM D, YYYY";
var pickadateDisplayFormat = "dddd mmmm d, yyyy"


Template.quizEditPage.rendered = function() {
	var self = this;
	console.log("Template.quizEditPage.rendered", self);

  $('.datepicker').pickadate({
    formatSubmit: pickadateSubmitFormat,
    format: pickadateDisplayFormat,
    onSet: function(thingSet) {
      //console.log('Set stuff:', thingSet);
    },
  });

	this.$('.add-topic-dropdown').dropdown({
		onChange: function(value, text, selectedItem) {
			//console.log("changed. value="+value+", text="+text+", selectedItem="+selectedItem);
			var quizId = self.data.quiz._id;
			var topicId = value;
			//console.log("this:", this);
			Meteor.call('addQuizTopic', quizId, topicId, function(error, result) {
				$('.add-topic-dropdown').dropdown("restore defaults");
				// for some reason the above attaches the default class, making the text grey
				$('.add-topic-dropdown span.text.default').removeClass("default");
				if (error) {
					throwError(error.reason);
				}
			});
		},
	});
};


Template.quizEditPage.events({
	'submit .update-form': function(event, template) {
		var courseId = this.course._id;
		var quizId = this.quiz._id;
		var title = event.target.title.value;
		var description = event.target.description.value;
    var quizMoment = moment(event.target.date_submit.value, momentParseFormat);
    var quizDate = quizMoment.toDate();
    var duration = event.target.duration.value;

		Meteor.call('updateQuiz', quizId, title, description, quizDate, duration, function(error, result) {
			if (error) {
				throwError(error.reason);
			} else {
				Router.go("quizPage", {courseId: courseId, quizId: quizId});
			}
		});
		// Prevent default form submit
		return false;
	},
	'click .remove-topic-button': function(event, template) {
		event.preventDefault();
		console.log("click .remove-topic-button");
		var topicId = this._id;
		//console.log("this: ", this);
		//console.log("Template.instance(): ", Template.instance());
		//console.log("Template.currentData(): ", Template.currentData());
		//console.log("Template.parentData(): ", Template.parentData());
		//console.log("Template.parentData(2): ", Template.parentData(2));
		var quizId = Template.currentData().quiz._id;
		Meteor.call("removeQuizTopic", quizId, topicId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});


Template.quizEditPage.helpers({
	getTopics: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
		return Topics.find({_id: {$in: topicIds}});
	},
	getCourseTopics: function(courseId) {
		return Topics.find({courseId: courseId, removed: {$ne: true}});
	},
  getSkills: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
    return Skills.find({topicId: {$in: topicIds}}, {sort: {topicId: 1}});
  },
  getPracticeProblems: function(topicIds) {
    if (!topicIds) {
      topicIds = [];
    }
    return PracticeProblems.find({topicId: {$in: topicIds}, archived: false}, {sort: {topicId: 1}});
  },
	isSkillCoveredInQuiz: function(skillId, quiz) {
		var practiceProblemIds = quiz.practiceProblemIds;
		if (!practiceProblemIds) {
			practiceProblemIds = [];
		}
		//console.log("practiceProblemIds: ", practiceProblemIds);
		var practiceProblems = PracticeProblems.find({_id: {$in: practiceProblemIds}});
		var skillIds = _.flatten(practiceProblems.map(function(practiceProblem) {return practiceProblem.skillIds;}));
		//console.log("skillIds: ", skillIds);
		return _.contains(skillIds, skillId);
	},
  makeSettings: function() {
    return {
      rowsPerPage: 50,
      class: "ui compact table",
      fields: [
        {
          key: "publishedDate",
          label: "Published",
          sort: "descending",
          tmpl: Template.practiceProblemPublishedDateIfPublished,
          headerClass: "collapsing",
          cellClass: "left aligned",
          fn: function(value, object) {return object.publishedDate?object.publishedDate:new Date(0);},
        },
        {
          key: "userId",
          label: "Author",
          tmpl: Template.practiceProblemAuthorCell,
        },
        {
          key: "topicAndTitle",
          label: "Topic & Title",
          cellClass: "left aligned",
          tmpl: Template.practiceProblemTopicAndTitle,
          fn: function(value, object) {return object.title;},
        },
        {
          key: "solutions",
          label: "Sol.",
          headerClass: "collapsing",
          cellClass: "right aligned",
          fn: function(value, object) {return _countPracticeProblemSolutions(object._id)},
          tmpl: Template.practiceProblemSolutionCount,
        },
        {
          key: "difficulty",
          label: "Difficulty",
          headerClass: "collapsing",
          cellClass: "right aligned",
          fn: function (value, object) {return _computeAverageDifficultyRating(object._id)},
          tmpl: Template.practiceProblemDifficultyRating,
        },
        {
          key: "quality",
          label: "Quality",
          headerClass: "collapsing",
          cellClass: "right aligned",
          fn: function (value, object) {return _computeAverageQualityRating(object._id)},
          tmpl: Template.practiceProblemQualityRating,
        },
        {
          key: "use",
          label: "Use",
          headerClass: "collapsing",
          cellClass: "right aligned",
          tmpl: Template.quizEditPageUsePracticeProblem,
        },
      ],
    };
  },
});
