Template.quizTakePage.helpers({
  largeGravatarUrl: function(user) {
      var email = Meteor.user().emails[0].address;
      return Gravatar.imageUrl(email, {size: 400, secure: window.location.protocol==="https:"});
  },
  haveFreshQuizAttempt: function(quizId, quizStartDate) {
    console.log("haveFreshQuizAttempt("+quizId+", "+quizStartDate+"):");
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, quizStartDate: quizStartDate, studentId: Meteor.userId(), fresh: true});
    console.log(quizAttempt);
    return quizAttempt;
  },
  haveOngoingQuizAttempt: function(quizId, quizStartDate) {
    console.log("haveOngoingQuizAttempt("+quizId+", "+quizStartDate+"):");
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, quizStartDate: quizStartDate, studentId: Meteor.userId(), ongoing: true});
    console.log(quizAttempt);
    return quizAttempt;
  },
  haveFinishedQuizAttempt: function(quizId, quizStartDate) {
    console.log("haveFinishedQuizAttempt("+quizId+", "+quizStartDate+"):");
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, quizStartDate: quizStartDate, studentId: Meteor.userId(), finished: true});
    console.log(quizAttempt);
    return quizAttempt;
  },
  getQuizAttempt: function(quizId, quizStartDate) {
    console.log("getFinishedQuizAttempt("+quizId+", "+quizStartDate+"):");
    var quizAttempt = QuizAttempts.findOne({quizId: quizId, quizStartDate: quizStartDate, studentId: Meteor.userId()});
    console.log(quizAttempt);
    return quizAttempt;
  },
  shuffle: function(array) {
    return shuffle(array);
  },
  length: function(array) {
    return array.length;
  },
  getSecondsRemaining: function() {
    return Session.get("secondsRemaining");
  },
  getMinutesAndSecondsRemaining: function() {
    var seconds = Session.get("secondsRemaining");
    return Math.floor(seconds/60) + ":" + ((seconds%60)>9?(""+(seconds%60)):("0"+(seconds%60)));
  },
});
Template.quizTakePage.events({
	'click .begin-quiz-attempt-button': function(ev, template) {
		var quizId = this.quiz._id;
    var quizEndMoment = moment(new Date()).add(this.quiz.duration, 'minutes'); //TODO
    console.log("quizEndMoment:", quizEndMoment);
    var timer = Meteor.setInterval(function() {
      var now = moment(new Date());
      var secondsRemaining = quizEndMoment.diff(now, 'seconds');
      console.log("TICK: secondsRemaining:", secondsRemaining);
      Session.set("secondsRemaining", secondsRemaining);
      if (secondsRemaining<=0) {
        console.log("time is up!");
        var solutions = extractSolutions();
        Meteor.clearInterval(timer);
        Session.set("secondsRemaining", undefined);
        Meteor.call('finishQuizAttempt', quizId, solutions, function(error, result) {
    			if (error) {
    				throwError(error.reason);
    			}
    		});
      }
    }, 1000);
    this.timer = timer;
		Meteor.call('beginQuizAttempt', quizId, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
  'click .finish-quiz-attempt-button': function(ev, template) {
		var quizId = this.quiz._id;
    var solutions = extractSolutions();
    Meteor.clearInterval(this.timer);
    Session.set("secondsRemaining", undefined);
		Meteor.call('finishQuizAttempt', quizId, solutions, function(error, result) {
			if (error) {
				throwError(error.reason);
			}
		});
	},
});

function extractSolutions() {
  var solutions = [];
  $(".problem-container").each(function(index, element) {
    var selectedChoices = {};
    $(element).find(".choice-container").each(function(index, element) {
      var label = $(element).find(".choice-toggle-button").attr("name");
      var isCorrect = $(element).find(".choice-toggle-button").hasClass("active");
      selectedChoices[label] = isCorrect;
    });
    solutions.push({
      practiceProblemId: $(element).attr("name"),
      choices: selectedChoices,
    });
  });
  console.log(solutions);
  return solutions;
}

// randomly permute array elements
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
